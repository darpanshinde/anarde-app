<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance', function (Blueprint $table) {
            $table->id();

            $table->BigInteger('user_id');
            $table->BigInteger('employee_id');
            $table->date('date')->nullable() ;
            $table->string('weekday')->nullable() ;

            $table->boolean('clock_in')->default(0); 
            $table->string('clock_in_time')->nullable();
            $table->string('clock_in_latitude')->nullable();
            $table->string('clock_in_longitude')->nullable();
            $table->string('clock_in_image_path')->nullable();

            $table->boolean('clock_out')->default(0);
            $table->string('clock_out_time')->nullable();
            $table->string('clock_out_latitude')->nullable();
            $table->string('clock_out_longitude')->nullable();
            $table->string('clock_out_image_path')->nullable();

            $table->boolean('late_marked')->default(0);
            $table->boolean('half_day')->default(0);

            $table->integer('attendance_remark_code_id')->nullable() ; // If the attendance is marked (F - full day), (H - Half Day) and Absent
            $table->string('attendance_remark_code_val')->nullable() ;

            $table->BigInteger('created_by')->nullable();
            $table->BigInteger('updated_by')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance');
    }
}
