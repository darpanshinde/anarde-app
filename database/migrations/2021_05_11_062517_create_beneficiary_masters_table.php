<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeneficiaryMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beneficiary_masters', function (Blueprint $table) {
            $table->id();

            $table->BigInteger('project_id');
            $table->string('name');
            $table->string('mobile_number');
            $table->text('address');
            $table->text('photo_name'); //stored to recognise the extension of the file
            $table->boolean('active')->default(1);

            $table->BigInteger('created_by')->nullable();
            $table->BigInteger('updated_by')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beneficiary_masters');
    }
}
