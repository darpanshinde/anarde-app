<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_applications', function (Blueprint $table) {
            $table->id();
            $table->BigInteger('employee_id');
            $table->string('name');
            $table->string('department_centre_id');
            $table->string('location_centre_id');
            $table->string('mobile_number');
            $table->date('from_date');
            $table->date('to_date');
            $table->integer('total_days');
            $table->text('reason_of_leave');
            
            $table->boolean('approval_level_1')->nullable(); // 1 - means approved | 0 - means denied | null - means pending 
            $table->BigInteger('approved_level_1_by')->nullable();
            $table->text('approved_level_1_feedback')->nullable();

            $table->boolean('approval_level_2')->nullable(); // 1 - means approved | 0 - means denied | null - means pending 
            $table->BigInteger('approved_level_2_by')->nullable();
            $table->text('approved_level_2_feedback')->nullable();

            $table->BigInteger('created_by')->nullable();
            $table->BigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_applications');
    }
}
