<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationCentreMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_centre_masters', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('centre_id')->nullable();
            $table->boolean('active')->default(1);

            $table->BigInteger('created_by')->nullable();
            $table->BigInteger('updated_by')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_centre_masters');
    }
}
