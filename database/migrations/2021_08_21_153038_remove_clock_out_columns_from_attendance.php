<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveClockOutColumnsFromAttendance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendance', function (Blueprint $table) {
            $table->dropColumn('clock_out');
            $table->dropColumn('clock_out_time');
            $table->dropColumn('clock_out_latitude');
            $table->dropColumn('clock_out_longitude');
            $table->dropColumn('clock_out_image_path');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attendance', function (Blueprint $table) {
            $table->boolean('clock_out')->default(0);
            $table->string('clock_out_time')->nullable();
            $table->string('clock_out_latitude')->nullable();
            $table->string('clock_out_longitude')->nullable();
            $table->string('clock_out_image_path')->nullable();
        });
    }
}
