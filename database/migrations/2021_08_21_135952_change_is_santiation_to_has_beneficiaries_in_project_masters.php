<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeIsSantiationToHasBeneficiariesInProjectMasters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_masters', function (Blueprint $table) {
            $table->renameColumn('is_sanitation', 'has_beneficiaries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_masters', function (Blueprint $table) {
            $table->renameColumn('has_beneficiaries', 'is_sanitation');
        });
    }
}
