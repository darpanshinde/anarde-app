<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_masters', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('mobile_number');
            $table->string('employee_uid');

            $table->string('email_id');
            $table->string('password');
            $table->rememberToken();

            $table->integer("manager_employee_id");
            $table->integer('location_centre_id');
            $table->integer('department_centre_id');
            $table->integer('role_id');
            $table->boolean('active')->default(1);
            $table->BigInteger('created_by')->nullable();
            $table->BigInteger('updated_by')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_masters');
    }
}
