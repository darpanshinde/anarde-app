<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfficeHoursMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('office_hours_masters', function (Blueprint $table) {
            $table->id();

            $table->string("start_of_day");
            $table->string("end_of_day");
            $table->string("late_cutoff_time");
            $table->string("half_day_cutoff_time");

            $table->BigInteger('created_by')->nullable();
            $table->BigInteger('updated_by')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('office_hours_masters');
    }
}
