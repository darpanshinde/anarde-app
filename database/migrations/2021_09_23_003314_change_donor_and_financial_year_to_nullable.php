<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDonorAndFinancialYearToNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_masters', function (Blueprint $table) {
            $table->string('donor')->nullable()->change();
            $table->string('financial_year')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_masters', function (Blueprint $table) {
            $table->string('donor')->change();
            $table->string('financial_year')->change();
        });
    }
}
