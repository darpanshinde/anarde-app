<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_files', function (Blueprint $table) {
            $table->id();

            $table->BigInteger('content_manager_id');
            $table->BigInteger('project_id');
            $table->string('content_type');
            $table->string('content_period')->nullable();
            $table->date('date');
            $table->string('file_path');
            $table->string('file_name');
            $table->text('file_caption')->nullable();
            $table->boolean('approval_status')->nullable(); // 1 - Approved, 0 - Rejected, NULL - Denied
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_files');
    }
}
