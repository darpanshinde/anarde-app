<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutdoorDutiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outdoor_duties', function (Blueprint $table) {
            $table->id();
            $table->BigInteger('employee_id');
            $table->string('name');
            $table->string('department_centre_id');
            $table->string('mobile_number');
            $table->date('date');
            $table->string('place');
            $table->BigInteger('project_id');
            $table->text('purpose_of_visit');
            
            $table->string('od_duration');
            $table->string('start_time')->nullable();
            $table->string('end_time')->nullable();
            
            $table->boolean('approval_level_1')->nullable(); // 1 - means approved | 0 - means denied | null - means pending 
            $table->BigInteger('approved_level_1_by')->nullable();
            $table->text('approved_level_1_feedback')->nullable();

            $table->boolean('approval_level_2')->nullable(); // 1 - means approved | 0 - means denied | null - means pending 
            $table->BigInteger('approved_level_2_by')->nullable();
            $table->text('approved_level_2_feedback')->nullable();

            $table->BigInteger('created_by')->nullable();
            $table->BigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outdoor_duties');
    }
}
