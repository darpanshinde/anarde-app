<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Commands\AttendanceReminderController;

class ClockInReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clock_in:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a reminder email to all users who havent clocked in after 15 minutes of clock in time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $clock_in = new AttendanceReminderController();
        $clock_in->clock_in_reminder();
    }
}
