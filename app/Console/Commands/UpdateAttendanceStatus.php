<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Commands\UpdateAttendanceStatusController;
class UpdateAttendanceStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update_attendance:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the status of Attendance for each employee';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $update_status = new UpdateAttendanceStatusController();
        $update_status->index();
    }
}
