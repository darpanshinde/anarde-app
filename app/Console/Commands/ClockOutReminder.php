<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ClockOutReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clock_out:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a reminder email to all users who havent clocked out after 15 minutes of clock out time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $clock_out = new AttendanceReminderController();
        $clock_out->clock_out_reminder();
    }
}
