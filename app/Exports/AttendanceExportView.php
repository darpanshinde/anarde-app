<?php
namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class AttendanceExportView implements FromView
{
	// use Exportable;
    
    public function __construct($attendances, $employees, $from_date, $to_date)
    {
        $this->attendances = $attendances;
        $this->employees = $employees;
        $this->from_date = $from_date;
        $this->to_date = $to_date;
    }

    public function view(): View
    {
        return view('exports.attendance_report', [
            'attendances' => $this->attendances,
            'employees' => $this->employees,
            'from_date' => $this->from_date,
            'to_date' => $this->to_date,
        ]);
    }
}