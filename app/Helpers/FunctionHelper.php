<?php

function date_ymd($date)
{
    return date("Y-m-d", strtotime($date));
}

function date_dmy($date)
{
    return date("d-m-Y", strtotime($date));
}

function getWeekday($date) {
    return date('l', strtotime( $date));
}

function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}

?>