<?php

namespace App\Model\Attendance;

use Illuminate\Database\Eloquent\Model;
use App\Model\Masters\EmployeeMaster;

class MarkAttendance extends Model
{
	protected $table = 'attendance';

    protected $fillable = ['id', 'user_id', 'employee_id', 'date', 'weekday', 'clock_in', 'clock_in_time', 'clock_in_latitude', 'clock_in_longitude', 'clock_in_image_path', 'clock_out', 'clock_out_time', 'clock_out_latitude', 'clock_out_longitude', 'clock_out_image_path', 'late_marked', 'half_day', 'attendance_remark_code_id', 'attendance_remark_code_val', 'created_by', 'updated_by', 'created_at', 'updated_at'];

    public function employee(){
    	return $this->belongsTo(EmployeeMaster::class,'user_id', 'id');
    }
}
