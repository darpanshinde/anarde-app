<?php

namespace App\Model\ContentManager;

use Illuminate\Database\Eloquent\Model;
use App\Model\Masters\ProjectMaster;
use App\Model\Masters\EmployeeMaster;

class ContentFiles extends Model
{
    protected $fillable = ['id', 'content_manager_id', 'project_id', 'content_type', 'content_period', 'date', 'file_path', 'file_name', 'file_caption', 'created_at', 'updated_at'];

    public function project(){
    	return $this->belongsTo(ProjectMaster::class,'project_id', 'id');
    }

    public function created_by(){
    	return $this->belongsTo(EmployeeMaster::class,'created_by', 'id');
    }
    public function updated_by(){
    	return $this->belongsTo(EmployeeMaster::class,'created_by', 'id');
    }
}
