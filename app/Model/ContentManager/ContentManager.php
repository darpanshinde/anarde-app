<?php

namespace App\Model\ContentManager;

use Illuminate\Database\Eloquent\Model;

class ContentManager extends Model
{
    protected $table = "content_manager";

    protected $fillable = ['id','project_id','created_by','updated_by','created_at','updated_at'];
}
