<?php

namespace App\Model\Masters;

use Illuminate\Database\Eloquent\Model;

class LocationCentreMaster extends Model
{
    protected $fillable = ['id','name','centre_id','active','created_by','updated_by','created_at','updated_at'];
}
