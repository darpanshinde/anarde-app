<?php

namespace App\Model\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Model\Masters\ProjectMaster;

class BeneficiaryMaster extends Model
{
    protected $fillable = ['id','project_id','name','mobile_number','address','file_name', 'photo_name' ,'active','created_by','updated_by','created_at','updated_at'];

    public function project(){
    	return $this->belongsTo(ProjectMaster::class,'project_id', 'id');
    }
}
