<?php

namespace App\Model\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Model\Masters\LocationCentreMaster;
use App\Model\Masters\DepartmentCentreMaster;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Model\Masters\Role;
class EmployeeMaster extends Authenticatable
{
    use Notifiable;
    use HasRoles;
	
	protected $guarded = ['id'];

    public function getAuthPassword()
    {
     return $this->password;
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    protected $fillable = ['id','name','mobile_number','employee_uid','email_id','password','manager_employee_id','location_centre_id','department_centre_id','role_id', 'bypass_late_remarks', 'active', 'created_at','updated_at'];

    public function location_centre(){
    	return $this->belongsTo(LocationCentreMaster::class,'location_centre_id', 'id');
    }

    public function manager(){
        return $this->belongsTo(EmployeeMaster::class, 'manager_employee_id' ,'id');
    }

    public function department_centre(){
    	return $this->belongsTo(DepartmentCentreMaster::class,'department_centre_id', 'id');
    }


}
