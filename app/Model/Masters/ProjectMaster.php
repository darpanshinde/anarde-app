<?php

namespace App\Model\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Model\Masters\ProjectEmployeeMapping;
use App\Model\ContentManager\ContentFiles;

class ProjectMaster extends Model
{
    protected $fillable = ['id','name','project_description','state_id','donor','location_centre_id','financial_year','has_beneficiaries','active','created_by','updated_by','created_at','updated_at'];

    public function location_centre(){
    	return $this->belongsTo(LocationCentreMaster::class,'location_centre_id', 'id');
    }

    public function employee_mappings(){
    	return $this->hasMany(ProjectEmployeeMapping::class,'project_id');
    }
    public function before_content(){
    	return $this->hasMany(ContentFiles::class,'project_id')->where('content_period','before');
    }
    public function after_content(){
    	return $this->hasMany(ContentFiles::class,'project_id')->where('content_period','after');
    }
    public function work_in_progress_content(){
    	return $this->hasMany(ContentFiles::class,'project_id')->where('content_period','work_in_progress');
    }
    public function other_content(){
    	return $this->hasMany(ContentFiles::class,'project_id')->where('content_period','other');
    }
}
