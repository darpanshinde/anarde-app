<?php

namespace App\Model\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Model\Masters\EmployeeMaster;
class ProjectEmployeeMapping extends Model
{
    protected $fillable = ['id', 'project_id', 'employee_id', 'created_at', 'updated_at'];

    public function employee(){
    	return $this->belongsTo(EmployeeMaster::class,'employee_id');
    }
}
