<?php

namespace App\Model\Masters;

use Illuminate\Database\Eloquent\Model;

class HolidayMaster extends Model
{
    protected $fillable = ['id','name','date','created_by','updated_by','created_at','updated_at'];
}
