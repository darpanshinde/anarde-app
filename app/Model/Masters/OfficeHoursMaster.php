<?php

namespace App\Model\Masters;

use Illuminate\Database\Eloquent\Model;

class OfficeHoursMaster extends Model
{
    protected $fillable = ['id','start_of_day','end_of_day','late_cutoff_time','half_day_cutoff_time','created_by','updated_by','created_at','updated_at'];
}
