<?php

namespace App\Model\Masters;

use Illuminate\Database\Eloquent\Model;

class DepartmentCentreMaster extends Model
{
    protected $fillable = ['id',	'name',	'active',	'created_by',	'updated_by',	'created_at',	'updated_at'];
}
