<?php

namespace App\Model\LeaveApplication;

use Illuminate\Database\Eloquent\Model;
use App\Model\Masters\LocationCentreMaster;
use App\Model\Masters\DepartmentCentreMaster;
use App\Model\Masters\EmployeeMaster;

class LeaveApplication extends Model
{
    protected $fillable = ['id','employee_id','name','department_centre_id','location_centre_id','mobile_number','from_date','to_date', 'total_days','reason_of_leave','approval_level_1','approved_level_1_by','approved_level_1_feedback','approval_level_2','approved_level_2_by','approved_level_2_feedback','created_by','updated_by','created_at','updated_at'];

    public function location_centre(){
    	return $this->belongsTo(LocationCentreMaster::class,'location_centre_id', 'id');
    }

    public function department_centre(){
    	return $this->belongsTo(DepartmentCentreMaster::class,'department_centre_id', 'id');
    }

    public function employee(){
    	return $this->belongsTo(EmployeeMaster::class,'employee_id', 'id');
    }

    public function approvedLevel1By(){
        return $this->belongsTo(EmployeeMaster::class,'approved_level_1_by', 'id');
    }

    public function approvedLevel2By(){
        return $this->belongsTo(EmployeeMaster::class,'approved_level_2_by', 'id');
    }
}
