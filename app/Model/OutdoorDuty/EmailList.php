<?php

namespace App\Model\OutdoorDuty;

use Illuminate\Database\Eloquent\Model;
use App\Model\Masters\EmployeeMaster;

class EmailList extends Model
{
    protected $fillable = ['id','email','created_by','updated_by','created_at','updated_at'];

    public function createdBy(){
    	return $this->belongsTo(EmployeeMaster::class,'created_by', 'id');
    }
}
