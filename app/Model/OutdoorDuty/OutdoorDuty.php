<?php

namespace App\Model\OutdoorDuty;

use Illuminate\Database\Eloquent\Model;
use App\Model\Masters\DepartmentCentreMaster;
use App\Model\Masters\ProjectMaster;
use App\Model\Masters\EmployeeMaster;
use App\Model\OutdoorDuty\OutdoorDutyProjects;

class OutdoorDuty extends Model
{
    protected $fillable = ['id','employee_id','name','department_centre_id','mobile_number','date','place','purpose_of_visit','od_duration','start_time','end_time','approval_level_1','approved_level_1_by', 'approved_level_1_feedback','approval_level_2','approved_level_2_by', 'approved_level_2_feedback','created_by','updated_by','created_at','updated_at'];

    public function department_centre(){
    	return $this->belongsTo(DepartmentCentreMaster::class,'department_centre_id', 'id');
    }

    public function od_projects(){
    	return $this->hasMany(OutdoorDutyProjects::class,'outdoor_duty_id', 'id');
    }

    public function createdBy(){
    	return $this->belongsTo(EmployeeMaster::class,'created_by', 'id');
    }
    public function employee(){
    	return $this->belongsTo(EmployeeMaster::class,'employee_id', 'id');
    }

    public function approvedLevel1By(){
        return $this->belongsTo(EmployeeMaster::class,'approved_level_1_by', 'id');
    }

    public function approvedLevel2By(){
        return $this->belongsTo(EmployeeMaster::class,'approved_level_2_by', 'id');
    }
}
