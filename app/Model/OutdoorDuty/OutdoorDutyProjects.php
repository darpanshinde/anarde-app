<?php

namespace App\Model\OutdoorDuty;

use Illuminate\Database\Eloquent\Model;
use App\Model\Masters\ProjectMaster;

class OutdoorDutyProjects extends Model
{
    protected $fillable = ['id','outdoor_duty_id','project_id','created_at','updated_at'];

    public function project(){
    	return $this->belongsTo(ProjectMaster::class,'project_id', 'id');
    }
}
