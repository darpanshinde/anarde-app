<?php

namespace App\Http\Controllers\OutdoorDuty;

use App\Model\OutdoorDuty\OutdoorDuty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as Requested;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use App\Model\Masters\EmployeeMaster;
use App\User;
use App\Model\Masters\DepartmentCentreMaster;
use App\Model\Masters\ProjectMaster;
use App\Model\OutdoorDuty\EmailList;
use App\Jobs\OutdoorDutyMailJob;
use App\Model\OutdoorDuty\OutdoorDutyProjects;

class OutdoorDutyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $outdoor_duties = [];
        // return $request->all();
         if (count((array)$request->all()) == 0) {
            \Session::forget('data');
            $outdoor_duties = OutdoorDuty::orderBy('outdoor_duties.id', 'asc');

            if( !\Auth::user()->hasRole('Super Admin') ){
                    $outdoor_duties = $outdoor_duties->leftJoin('employee_masters as emp', function($join){
                        $join->on('emp.id', 'outdoor_duties.employee_id');
                    })
                    ->where(function($query){
                        $query->where('emp.id', \Auth::user()->id)
                                ->orWhere('emp.manager_employee_id', \Auth::user()->id);
                    });
            }
        }
        else {
            if(isset($request->search) && count($request->search) > 0)
            {
                \Session::forget('data');
            }
            \Session::push('data', $request->search);
             $outdoor_duties = OutdoorDuty::orderBy('outdoor_duties.id', 'asc');

             if( !\Auth::user()->hasRole('Super Admin') ){
                    $outdoor_duties = $outdoor_duties->leftJoin('employee_masters as emp', function($join){
                        $join->on('emp.id', 'outdoor_duties.employee_id');
                    })
                    ->where(function($query){
                        $query->where('emp.id', \Auth::user()->id)
                                ->orWhere('emp.manager_employee_id', \Auth::user()->id);
                    });
             }
            if (count((array)\Session::get('data')[0]) > 0) {
                                
                $query = OutdoorDuty::orderBy('outdoor_duties.id', 'asc');

                if( !\Auth::user()->hasRole('Super Admin') ){
                    $query = $query->leftJoin('employee_masters as emp', function($join){
                        $join->on('emp.id', 'outdoor_duties.employee_id');
                    })
                    ->where(function($query){
                        $query->where('emp.id', \Auth::user()->id)
                                ->orWhere('emp.manager_employee_id', \Auth::user()->id);
                    });
                }
                
                foreach (\Session::get('data')[0] as $outdoor_duty)
                {
                    
                    if( $outdoor_duty['column'] == 'name' ){
                        $query->where('name', 'LIKE', '%'. $outdoor_duty['search_data'] .'%');
                    }
                    if( $outdoor_duty['column'] == 'department' ){
                        $query->WhereHas('department_centre', function ($query1) use ($outdoor_duty) {
                            $query1->where('name', 'LIKE', '%' . $outdoor_duty['search_data'] . '%');
                        });
                    }
                    if( $outdoor_duty['column'] == 'date' ){
                        $query->where('date', 'LIKE', '%'. $outdoor_duty['search_data'] .'%');
                    }
                    if( $outdoor_duty['column'] == 'od_duration' ){
                        $query->where('od_duration', 'LIKE', '%'. $outdoor_duty['search_data'] .'%');
                    }
                    if( $outdoor_duty['column'] == 'place' ){
                        $query->where('place', 'LIKE', '%'. $outdoor_duty['search_data'] .'%');
                    }
                    
                    if( $outdoor_duty['column'] == 'project' ){
                        $query->WhereHas('project', function ($query1) use ($outdoor_duty) {
                            $query1->where('name', 'LIKE', '%' . $outdoor_duty['search_data'] . '%');
                        });
                    }
                    if( $outdoor_duty['column'] == 'purpose_of_visit' ){
                        $query->where('purpose_of_visit', 'LIKE', '%'. $outdoor_duty['search_data'] .'%');
                    }
                    


                    if( $outdoor_duty['column'] == 'approval_level_1_status' ){
                        if( strtolower($outdoor_duty['search_data']) == "approved" )
                            $query->where('approval_level_1', 1);
                        elseif( strtolower($outdoor_duty['search_data']) == "denied" )
                            $query->where('approval_level_1', 0);
                        elseif( strtolower($outdoor_duty['search_data']) == "pending" )
                            $query->where('approval_level_1', null);
                    }

                    if( $outdoor_duty['column'] == 'approval_level_2_status' ){
                        if( $outdoor_duty['search_data'] == "approved" )
                            $query->where('approval_level_2', 1);
                        elseif( $outdoor_duty['search_data'] == "denied" )
                            $query->where('approval_level_2', 0);
                        elseif( $outdoor_duty['search_data'] == "pending" )
                            $query->where('approval_level_2', null);
                    }
                    if( $outdoor_duty['column'] == 'status' ){
                        if( $outdoor_duty['search_data'] == "active" )
                            $query->where('active', 1);
                        elseif( $outdoor_duty['search_data'] == "inactive" )
                            $query->where('active', 0);
                    }

                    
                }
                $outdoor_duties = $query;
                
            }
        }

        $outdoor_duties = $outdoor_duties->paginate(10);
        $columns = ['name','department', 'date', 'od_duration', 'place', 'project', 'purpose_of_visit' ,  'approval_level_1_status','approval_level_2_status', 'status'];
        return view('outdoor_duty.index', compact('outdoor_duties','columns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = EmployeeMaster::pluck('name','id');
        $current_user = \Auth::user();
        $projects = ProjectMaster::pluck('name', 'id');
        return view('outdoor_duty.create', compact('employees', 'current_user','projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        //Validation to check if the required fields to store the data are available or not
        $validatedData = $request->validate([ 
            'name' => 'required', 
            'department_centre_id' => 'required',
            'mobile_number' => 'required', 
            'date' => 'required | after_or_equal:'. date("d-m-Y").'', 
            'place' => 'required', 
            'project_id' => 'required', 
            'purpose_of_visit' => 'required', 
            'od_duration' => 'required', 
        ]);

        //Validate if OD Duration is based on hours
        if( $data['od_duration'] == "custom_time" ){

            if( $data['start_time'] == "" ){
                $error = Collection::make( ["Start Time needed for OD Duration : Custom Time "] );
                return redirect()->back()->with('errors', $error)->withInput( $request->input() );
            }

            if( $data['end_time'] == "" ){
                $error = Collection::make( ["End Time needed for OD Duration : Custom Time "] );
                return redirect()->back()->with('errors', $error)->withInput( $request->input() );
            }

        }

        $data['date'] = date_ymd($data['date']);
        $data['created_by'] = \Auth::user()->id;

        \DB::beginTransaction();
            $outdoor_duty = new OutdoorDuty();
            $outdoor_duty = $outdoor_duty->create($data);

            foreach ($request->project_id as $key => $value) {
                if( !is_null($value) ){
                    $od_projects = new OutdoorDutyProjects();
                    $od_projects->outdoor_duty_id = $outdoor_duty->id;
                    $od_projects->project_id = $value;
                    $od_projects->save();
                }
            }

            $email_list = EmailList::pluck('email')->toArray();

            if( !is_null($outdoor_duty->employee->manager) )
                array_push($email_list, $outdoor_duty->employee->manager->email_id);

            $email_list = array_unique($email_list);

            foreach ($email_list as $key => $email) {
                
                $title = "Creation of Outdoor Duty for Employee ".$request->name."#".$request->employee_id;

                $body = "Outdoor Duty has been created for Employee:".$request->name." with Emp ID: ".$request->employee_id." for the date of ".$request->date."
                    Purpose of Visit: ".$request->purpose_of_visit." 
                    Place: ".$request->place." 
                    OD Duration: ".$request->od_duration."
                    " ;

                $data=[
                    "email"=>$email,
                    "title"=>$title,
                    "body"=>$body
                ];
                dispatch(new OutdoorDutyMailJob($data));
            }

        \DB::commit();
        
        session()->flash('status',"Outdoor Duty Successfully Created");
        return redirect()->route('outdoor-duty.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\OutdoorDuty\OutdoorDuty  $outdoorDuty
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $outdoor_duty = OutdoorDuty::find($id);
        return view('outdoor_duty.show', compact('outdoor_duty'));
    }

    public function approval_process($id, Request $request){
        $outdoor_duty = OutdoorDuty::find($id);
        $approval_level = $request->approval_level;

        $data['approval_status'] = $request->approval_status;
        $data['updated_by'] = \Auth::user()->id;

        if( $approval_level == 1 ){

            $data['approved_level_1_feedback'] = $request->approval_feedback;
            $data['approved_level_1_by'] = \Auth::user()->id;


            if( $data['approval_status'] == "reject" && $data['approved_level_1_feedback'] == "" ){
                return ['error' => "Approval Feedback Compulsory When Rejected"];
            }

            if( $data['approval_status'] == "approve" )
                $data['approval_level_1'] = 1;
            else if( $data['approval_status'] == "reject")
                $data['approval_level_1'] = 0;

            $email = $outdoor_duty->employee->email_id;
            $title = "Approval Level 1 Status of Outdoor Duty #".$id." for Employee ".$request->name."#".$request->employee_id;

            $body = "Outdoor Duty has created for Employee:".$request->name." with Emp ID: ".$request->employee_id." for the date of ".$request->date."
                Purpose of Visit: ".$request->purpose_of_visit." 
                Place: ".$request->place." 
                OD Duration: ".$request->od_duration."
                Approval Level 1 STATUS: ".$data['approval_status']."
                Approval Level 1 Feedback: ".$data['approved_level_1_feedback']."
                " ;

            $data=[
                "email"=>$email,
                "title"=>$title,
                "body"=>$body
            ];
            dispatch(new OutdoorDutyMailJob($data));

        }
        else if( $approval_level == 2){
            $data['approved_level_2_feedback'] = $request->approval_feedback;
            $data['approved_level_2_by'] = \Auth::user()->id;


            if( $data['approval_status'] == "reject" && $data['approved_level_2_feedback'] == "" ){
                return ['error' => "Approval Feedback Compulsory When Rejected"];
            }

            if( $data['approval_status'] == "approve" )
                $data['approval_level_2'] = 1;
            else if( $data['approval_status'] == "reject")
                $data['approval_level_2'] = 0;

            $email = $outdoor_duty->employee->email_id;
            $title = "Approval Level 2 Status of Outdoor Duty #".$id." for Employee ".$request->name."#".$request->employee_id;

            $body = "Outdoor Duty has created for Employee:".$request->name." with Emp ID: ".$request->employee_id." for the date of ".$request->date."
                Purpose of Visit: ".$request->purpose_of_visit." 
                Place: ".$request->place." 
                OD Duration: ".$request->od_duration."
                Approval Level 2 STATUS: ".$data['approval_status']."
                Approval Level 2 Feedback: ".$data['approved_level_2_feedback']."
                " ;

            $data=[
                "email"=>$email,
                "title"=>$title,
                "body"=>$body
            ];
            dispatch(new OutdoorDutyMailJob($data));
        }

        $outdoor_duty->update($data);

        session()->flash('status',"Outdoor Duty Successfully Updated");
        return "success";

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\OutdoorDuty\OutdoorDuty  $outdoorDuty
     * @return \Illuminate\Http\Response
     */
    public function edit(OutdoorDuty $outdoorDuty)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\OutdoorDuty\OutdoorDuty  $outdoorDuty
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OutdoorDuty $outdoorDuty)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\OutdoorDuty\OutdoorDuty  $outdoorDuty
     * @return \Illuminate\Http\Response
     */
    public function destroy(OutdoorDuty $outdoorDuty)
    {
        //
    }
}
