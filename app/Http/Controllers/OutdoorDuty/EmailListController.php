<?php

namespace App\Http\Controllers\OutdoorDuty;

use App\Model\OutdoorDuty\EmailList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class EmailListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $email_list =EmailList::orderBy('id', 'asc');
        if( !isset($request->search) ){
            $email_list = EmailList::orderBy('id', 'asc');
        }
        else{
            $email_list = EmailList::where('email', $request->search);
        }

        $email_list = $email_list->paginate(10);
        return view('outdoor_duty.email_list_index', compact('email_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data =  $request->all();

        $created_by = \Auth::user()->id;

        $error = [];
        $error_string = "";
        \DB::beginTransaction();
            foreach ($data['email'] as $key => $email) {
                if( EmailList::where('email', $email)->exists() ){
                    $error[$key] = "\\n\u2022".$email;
                    continue;
                }

                $new_email = new EmailList();
                $new_email->email = $email;
                $new_email->created_by = $created_by;
                $new_email->save();
            }

        \DB::commit();

        if( !is_null($error) ){
            $error_string = " except the following emails which were already present: ";

            foreach ($error as $key => $value) {
                $error_string = $error_string." ".$value;
            }
        }

        session()->flash('status',"Emails successfully added to the list".$error_string);
        return redirect()->route('email-list.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\OutdoorDuty\EmailList  $emailList
     * @return \Illuminate\Http\Response
     */
    public function show(EmailList $emailList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\OutdoorDuty\EmailList  $emailList
     * @return \Illuminate\Http\Response
     */
    public function edit(EmailList $emailList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\OutdoorDuty\EmailList  $emailList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmailList $emailList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\OutdoorDuty\EmailList  $emailList
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id = null ){

        if( !is_null($id) ){
            EmailList::find($id)->delete();

            session()->flash('status',"Email ID deleted successfully");
            return "success";
        }
        elseif( isset($request->email) ){
            foreach ($request->email as $key => $email_id) {
                \DB::beginTransaction();
                    EmailList::find($email_id)->delete();
                \DB::commit();
            }

            session()->flash('status',"Email ID deleted successfully");
            return redirect()->route('email-list.index');

        }
        else{
            return ["error" => "Something is Wrong"];
        }

       
    }
    public function bulk_delete(Request $request){
        return $request->email_list;
        foreach ($request->email_list as $key => $email_id) {
            \DB::beginTransaction();
                EmailList::find($email_id)->delete();
            \DB::commit();
        }
        session()->flash('status',"Email IDs deleted successfully");
        return "success";
    }
    public function destroy(Request $request, $id)
    {
        
    }
}
