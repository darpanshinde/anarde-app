<?php

namespace App\Http\Controllers\Attendance;

use App\Model\Attendance\MarkAttendance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as Requested;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use App\Model\Masters\EmployeeMaster;
use App\Model\Masters\OfficeHoursMaster;
use App\User;
use App\Model\LeaveApplication\LeaveApplication;
use App\Model\Masters\HolidayMaster;
class MarkAttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $clock_in_details = null;

        if( MarkAttendance::where('user_id', \Auth::user()->id)->where('date', date_ymd(Carbon::now()))->where('clock_in', 1)->exists() )
            $clock_in_details = MarkAttendance::where('user_id', \Auth::user()->id)->where('date', date_ymd(Carbon::now()))->where('clock_in', 1)->first();

        return view('attendance.mark_attendance.index', compact('clock_in_details'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();

        //If Attendance Directory doesn't exist then create one
        $data['attendance_directory'] =  public_path('\uploads\Attendances\\');
        if(!\File::isDirectory($data['attendance_directory'])){
            //make the directory because it doesn't exists
            \File::makeDirectory($data['attendance_directory']);
        }

        $data['user_directory'] = public_path('\uploads\Attendances\\'.\Auth::user()->id."_".\Auth::user()->name);
        $data['directoryPath'] = public_path('\uploads\Attendances\\'.\Auth::user()->id."_".\Auth::user()->name.'/'.\Auth::user()->id."_".\Auth::user()->name."_".date_dmy(Carbon::now()));

        if(!\File::isDirectory($data['user_directory'])){
            //make the directory because it doesn't exists
            \File::makeDirectory($data['user_directory']);
        }

        //check if the directory exists
        if(!\File::isDirectory($data['directoryPath'])){
            //make the directory because it doesn't exists
            \File::makeDirectory($data['directoryPath']);
        }

        $data['user_id'] = \Auth::user()->id;

        if($request->attendance_details == "clock_in")
            return $this->clock_in($data);
        elseif( $request->attendance_details == "clock_out" );
            return $this->clock_out($data);

    }

    public function clock_in($data){
        
        $datetime = Carbon::now();

        $file = Requested::file('image');
        $filename = \Auth::user()->id."_".date_dmy($datetime)."_clock_in".".jpeg";

        \Storage::disk('google_attendance')->put($filename, file_get_contents($file));

         // Get the uploaded file to store it's Google Drive File ID
         $dir = '/';
         $recursive = false; // Get subdirectories also?
         $contents = collect(\Storage::disk('google_attendance')->listContents($dir, $recursive));

         $gdrive_uploaded_file = $contents
             ->where('type', '=', 'file')
             ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
             ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
             ->first(); // there can be duplicate file names!

        \DB::beginTransaction();

            $attendance = new MarkAttendance();
            $attendance->user_id = $data['user_id'];
            $attendance->employee_id = \Auth::user()->employee_uid; //===============HERE EMPLOYEE USER ID IS NEEDED(WHEN AUTH COMES FROM EMPLOYEE)============
            $attendance->date = date_ymd($datetime);
            $attendance->weekday = date('l');
            $attendance->clock_in = 1;
            $attendance->clock_in_time = $datetime->format('H:i');
            $attendance->clock_in_latitude = $data['current_latitude'];
            $attendance->clock_in_longitude = $data['current_longitude'];
            $attendance->clock_in_image_path = $gdrive_uploaded_file['path'];

            //Check if the employee is late
            $start_of_day = date("H:i", strtotime(OfficeHoursMaster::first()->start_of_day));
            $late_cutoff_mins = OfficeHoursMaster::first()->late_cutoff_time;
            $clock_in_time = date("H:i", strtotime($datetime->format('H:i')));

            $late_cutoff_timestamp = date( "H:i", strtotime( "$start_of_day +$late_cutoff_mins minutes" ) ); 

            if( $clock_in_time > $late_cutoff_timestamp )
                $attendance->late_marked = 1;

            $attendance->created_by = $data['user_id'];
            $attendance->save();
        \DB::commit();

        return "success";
    }

    public function clock_out($data){

        $datetime = Carbon::now();

        $file = Requested::file('image');
        $filename = \Auth::user()->id."_".date_dmy($datetime)."_clock_out".".jpeg";


        if( MarkAttendance::where('user_id', $data['user_id'])->where('date', date_ymd($datetime))->where('clock_in', 1)->exists() ){
            
            if( !MarkAttendance::where('user_id', $data['user_id'])->where('date', date_ymd($datetime))->where('clock_out', 1)->exists() ){

                \Storage::disk('google_attendance')->put($filename, file_get_contents($file));

                // Get the uploaded file to store it's Google Drive File ID
                $dir = '/';
                $recursive = false; // Get subdirectories also?
                $contents = collect(\Storage::disk('google_attendance')->listContents($dir, $recursive));

                $gdrive_uploaded_file = $contents
                    ->where('type', '=', 'file')
                    ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
                    ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
                    ->first(); // there can be duplicate file names!


                \DB::beginTransaction();

            

                    $attendance = MarkAttendance::where('user_id', $data['user_id'])->where('date', date_ymd($datetime))->where('clock_in', 1)->first();
                    $attendance->clock_out = 1;
                    $attendance->clock_out_time = $datetime->format('H:i');
                    $attendance->clock_out_latitude = $data['current_latitude'];
                    $attendance->clock_out_longitude = $data['current_longitude'];
                    $attendance->clock_out_image_path = $gdrive_uploaded_file['path'];

                    //Check if Employee went by Half Day
                    $start_of_day = date("H:i", strtotime(OfficeHoursMaster::first()->start_of_day));
                    $clock_in_time = date("H:i", strtotime("10:30"));

                    //Check if employee clocked in before start of day or not
                        // If Employee clocks in earlier than start of day then Half day minutes will be added to start of day
                        // If Employee clocks in later than start of day then half day minutes will be added to clock in time-0
                    if( $start_of_day > $clock_in_time )
                        $day_start_time = $start_of_day;
                    else
                        $day_start_time = $clock_in_time;

                    $half_day_cutoff_mins = OfficeHoursMaster::first()->half_day_cutoff_time;
                    $half_day_cutoff_timestamp = date( "H:i", strtotime( "$day_start_time +$half_day_cutoff_mins minutes" ) );
                    $clock_out_time =  date("H:i", strtotime($datetime->format('H:i')));
                    
                    if( $clock_out_time < $half_day_cutoff_timestamp ){
                        $attendance->half_day = 1;
                        $attendance->attendance_remark_code_id = 1120; //============NEEDS TO BE MADE DYNAMIC=======
                        $attendance->attendance_remark_code_val = "H";
                    }
                    else{
                        $attendance->attendance_remark_code_val = "F";
                    }
                    
                    $attendance->updated_by = $data['user_id'];
                    $attendance->update();
                \DB::commit();
            }
            else{
                return "clocked_out";
            }
        }

        return "success";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Attendance\MarkAttendance  $markAttendance
     * @return \Illuminate\Http\Response
     */
    public function show_attendance_index(Request $request){

        $employees = EmployeeMaster::pluck('name', 'id');
        $clock_in_details = null;
        $clock_out_details = null;
        return view('attendance.mark_attendance.show_attendance_index', compact('employees', 'clock_in_details', 'clock_out_details'));
    }

    public function find_attendance(Request $request){
        $user_id = $request->employee;
        $date = date_ymd($request->date);

        $employees = EmployeeMaster::pluck('name', 'id');
        
        $clock_in_details = MarkAttendance::where(\DB::Raw("1"), "1");
        $clock_out_details = MarkAttendance::where(\DB::Raw("1"), "1");

        if( is_null($user_id) ){
            $clock_in_details->where('user_id', \Auth::user()->id);
            $clock_out_details->where('user_id', \Auth::user()->id);
        }
        else{
            $clock_in_details->where('user_id', $user_id);
            $clock_out_details->where('user_id', $user_id);
        }

        $clock_in_details = $clock_in_details->where('date', $date)->orderBy('id', 'asc')->first();

        if( !is_null($clock_in_details) )
            $clock_out_details = $clock_out_details->where('date', $date)->where('id', '!=', $clock_in_details->id)->orderBy('id', 'desc')->first();

        if( is_null($clock_in_details) ){

            $day = date('l', strtotime($date));
            if( strtolower($day) != "saturday" || strtolower("sunday") ){
                   
                   if( LeaveApplication::where('employee_id', $user_id)->where(function($query) use ($date){
                                        $query->where('from_date', '<=',$date)->where('to_date', '>=', $date);
                                       })->where('approval_level_2', 1)->exists()
                    ){

                        $error = Collection::make( ["Approved Holiday: Attendance not Available"] );
                        return redirect()->back()->with('errors', $error)->withInput( $request->input() );

                   }
                   else if( HolidayMaster::where('date', $date)->exists() ){
                        $error = Collection::make( [" Holiday: Attendance not Available"] );
                        return redirect()->back()->with('errors', $error)->withInput( $request->input() );
                   }
                   else{

                        $error = Collection::make( [" You were ABSENT: Attendance not Available"] );
                        return redirect()->back()->with('errors', $error)->withInput( $request->input() );

                   }

            }
            else{
                $error = Collection::make( ["Weekend Holiday: Attendance not Available"] );
                return redirect()->back()->with('errors', $error)->withInput( $request->input() );
            }

        } 

        $total_time = null;
        if( !is_null($clock_out_details) ){
            $total_time = abs(strtotime($clock_in_details->clock_in_time ) -  strtotime($clock_out_details->clock_in_time)) / 60;
            $total_time = convertToHoursMins($total_time);
        }
        $clock_in_details->total_time = $total_time;
        
        return view('attendance.mark_attendance.show_attendance_index', compact('employees', 'clock_in_details', 'clock_out_details'));
    }

    public function show($id, Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Attendance\MarkAttendance  $markAttendance
     * @return \Illuminate\Http\Response
     */
    public function edit(MarkAttendance $markAttendance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Attendance\MarkAttendance  $markAttendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MarkAttendance $markAttendance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Attendance\MarkAttendance  $markAttendance
     * @return \Illuminate\Http\Response
     */
    public function destroy(MarkAttendance $markAttendance)
    {
        //
    }

    public function ip_details(Request $request)
    {
        // $ip = '103.239.147.187'; //For static IP address get
        
        $ip = $request->ip;
        $data = \Location::get($ip);

        dd($data);                
        return view('details',compact('data'));
    }
}
