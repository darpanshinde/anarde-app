<?php

namespace App\Http\Controllers\Commands;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Masters\OfficeHoursMaster;
use App\Model\Masters\EmployeeMaster;
use App\Model\Attendance\MarkAttendance;
use App\Model\Masters\State;
class UpdateAttendanceStatusController extends Controller
{
    public function index(){

    	$start_of_day = OfficeHoursMaster::first()->start_of_day;
    	$end_of_day = OfficeHoursMaster::first()->end_of_day;
    	
    	$current_date = date('Y-m-d');
    	$yesterdays_date = date('Y-m-d', strtotime('-1 day', strtotime($current_date)));
    	
    	$late_cutoff = OfficeHoursMaster::first()->late_cutoff_time;
		$late_cutoff_in_hrs_and_mins = date("H:i", strtotime('+'.$late_cutoff.' minutes', strtotime($start_of_day)));
    	
    	$half_day_cutoff = OfficeHoursMaster::first()->half_day_cutoff_time;
		$half_cutoff_in_hrs_and_mins = date("H:i", strtotime('+'.$half_day_cutoff.' minutes', strtotime($start_of_day)));

    	$late_remark = 0;
    	$half_day_remark = 0;
    	$full_day_remark = 0;
		
		$user_ids = MarkAttendance::where('date', $yesterdays_date)->distinct()->pluck('user_id');



		\DB::beginTransaction();
			foreach ($user_ids as $key => $user_id) {
				$employee = EmployeeMaster::find($user_id);

				$clock_in_details = MarkAttendance::where('user_id', $user_id)->where('date', $yesterdays_date)->orderBy('id','asc')->first();
				$clock_out_details = MarkAttendance::where('user_id', $user_id)->where('id', '!=', $clock_in_details->id)->where('date', $yesterdays_date)->orderBy('id','desc')->first();

				if( $clock_in_details->clock_in_time > $late_cutoff_in_hrs_and_mins ){
					$clock_in_details->update(['late_marked' => 1]);
				}
				
				if( !is_null($clock_out_details) ){
					if( $clock_out_details->clock_in_time < $half_cutoff_in_hrs_and_mins ){
						$clock_out_details->update(['half_day' => 1, 'attendance_remark_code_val' => 'half_day']);
					}
					else{
						$clock_out_details->update(['attendance_remark_code_val' => 'full_day']);
					}
				}
			}
		\DB::commit();

    }
}
