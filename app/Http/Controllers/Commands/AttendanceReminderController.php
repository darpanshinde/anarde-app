<?php

namespace App\Http\Controllers\Commands;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Masters\EmployeeMaster;
use Illuminate\Support\Carbon;
use App\Model\Attendance\MarkAttendance;
use App\Jobs\AttendanceReminderMailJob;
use App\Model\Masters\OfficeHoursMaster;

class AttendanceReminderController extends Controller
{
    public function clock_in_reminder(){
    	$current_time = Carbon::now();
    	$todays_date = date_ymd($current_time->toDateTimeString());
        
        $current_hour_minutes = date('H:i');
        $start_time = Carbon::parse(OfficeHoursMaster::find(1)->start_of_day);

        $start_time->addMinutes(15); //Reminder cutoff post start time
        $reminder_cutoff = $start_time->format('H:i');
        
        \Log::info("This is the log entry of cron job");
        if( $current_time == $reminder_cutoff ){
                
            	$employee_ids = MarkAttendance::where('date', $todays_date)->where('clock_in', 1)->pluck('employee_id');
            	$employee_details = EmployeeMaster::Select('name', 'employee_uid', 'email_id')->whereNotIn('id', $employee_ids)->groupBy('email_id')->get();

            	$title = "Attendance Clock In Reminder for ".date_dmy($todays_date);	

            	foreach ($employee_details as $key => $employee) {

            		$body = "Attendance (Clock In) for the date: ".date_dmy($todays_date)." for Employee: ".$employee->name." with EMP UID: ".$employee->employee_uid." hasn't been recorded yet. Please clock in to avoid being marked Half Day/ Absent";

        			$data=[
        	            "email"=>$employee->email_id,
        	            "title"=>$title,
        	            "body"=>$body
        	        ];
        	        dispatch(new AttendanceReminderMailJob($data));
            	}

            	return "Emails Sent";

        }
    }

    public function clock_out_reminder(){
        $current_time = Carbon::now();
        $todays_date = date_ymd($current_time->toDateTimeString());

        $current_hour_minutes = date('H:i');
        $end_time = Carbon::parse(OfficeHoursMaster::find(1)->end_of_day);

        $end_time->addMinutes(15); //Reminder cutoff post start time
        $reminder_cutoff = $end_time->format('H:i');

        if( $current_time == $reminder_cutoff ){

            $employee_ids = MarkAttendance::where('date', $todays_date)->where('clock_out', 1)->pluck('employee_id');
            $employee_details = EmployeeMaster::Select('name', 'employee_uid', 'email_id')->whereNotIn('id', $employee_ids)->groupBy('email_id')->get();

            $title = "Attendance Clock Out Reminder for ".date_dmy($todays_date); 

            foreach ($employee_details as $key => $employee) {

                $body = "Attendance (Clock Out) for the date: ".date_dmy($todays_date)." for Employee: ".$employee->name." with EMP UID: ".$employee->employee_uid." hasn't been recorded yet. Please clock out to avoid being marked Half Day/ Absent";

                $data=[
                    "email"=>$employee->email_id,
                    "title"=>$title,
                    "body"=>$body
                ];
                dispatch(new AttendanceReminderMailJob($data));
            }

            return "Emails Sent";
        }
    }
}
