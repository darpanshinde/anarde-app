<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Attendance\MarkAttendance;
use App\Exports\AttendanceExportView;
use App\Exports\AttendanceHorizontalExportView;
use App\Model\Masters\EmployeeMaster;

class AttendanceReportController extends Controller
{
    public function index(Request $request){
    	$employees = EmployeeMaster::pluck('name', 'id');
        $attendance = null;
        return view('reports.attendance.index', compact('employees', 'attendance'));
    }

    public function export_attendance(Request $request){
        $employee_id = $request->employee_id;
    	$from_date = date_ymd($request->from_date);
    	$to_date = date_ymd($request->to_date);

        $employees = EmployeeMaster::where('active',1);
        $attendance = EmployeeMaster::select('name', 'at.user_id', 'at.date', 'clock_in.clock_in_time', 
                        \DB::raw('CASE WHEN clock_in.id = clock_out.id THEN NULL ELSE clock_out.clock_in_time END as clock_out_time'),
                        'clock_out.attendance_remark_code_val')
                        ->orderBy('employee_masters.id')->where('active', 1)
                        ->leftJoin('attendance as at', 'employee_masters.id', '=', 'at.user_id')
                        ->leftJoin('attendance as clock_in', function ($join){
                            $join->on('clock_in.id','=', \DB::raw('(select min(id) from attendance att where at.date = att.date and att.user_id = at.user_id)'));
                        })
                        ->leftJoin('attendance as clock_out', function ($join){
                            $join->on('clock_out.id','=', \DB::raw('(select max(id) from attendance att where at.date = att.date and att.user_id = at.user_id)'));
                        })
                        ->groupBy('at.date');

        if( !\Auth::user()->hasRole('Super Admin') ){

            $attendance->where('at.user_id', \Auth::user()->id);
            $employees->where('id', \Auth::user()->id);

        }
        else
        {
            if( !is_null($employee_id) ){

                $attendance->where('at.user_id', $employee_id);
                $employees->where('id', $employee_id);
            }
        }

        $employees = $employees->get();

        // DB::table( DB::raw("({$sub->toSql()}) as sub") )
        // $distinct_users = \DB::select( \DB::raw('select distinct user_id from ('.$attendance->toSql().') as attendance_rows') );

        // return view('exports.attendance_report', compact('attendances', 'employees', 'from_date', 'to_date'));
    	
        $export = new AttendanceExportView( $attendance->get(), $employees, $from_date, $to_date );
    	return \Excel::download($export, ' Attendance_export.xlsx');
        return 1;
    }

    public function export_attendance_horizontal(Request $request){

        $clock_in_time = MarkAttendance::select(\DB::raw("min(attendance.id) as ID"),'attendance.user_id','employee_masters.name','employee_masters.email_id', 'attendance.date', 'clock_in_time', 'clock_in_time as description')
                        ->leftJoin('employee_masters', function($join){
                            $join->on('employee_masters.id','=','attendance.user_id');
                        })->groupBy('attendance.date')->orderBy('date')->orderBy('user_id');

        $clock_out_time = MarkAttendance::select(\DB::raw("max(attendance.id) as ID"),'attendance.user_id','employee_masters.name','employee_masters.email_id', 'attendance.date', 'clock_in_time', 'clock_out_time as description')
                        ->leftJoin('employee_masters', function($join){
                            $join->on('employee_masters.id','=','attendance.user_id');
                        })->groupBy('attendance.date')->orderBy('date')->orderBy('user_id');

        $status = MarkAttendance::select(\DB::raw("max(attendance.id) as ID"),'attendance.user_id','employee_masters.name','employee_masters.email_id', 'attendance.date', 'attendance_remark_code_val', 'status as description')
                        ->leftJoin('employee_masters', function($join){
                            $join->on('employee_masters.id','=','attendance.user_id');
                        })->groupBy('attendance.date')->orderBy('date')->orderBy('user_id');

        // $pivot = \DB::table(\DB::raw($clock_in_time))->union(\DB::raw($clock_out_time))->union('status');

        $attendance = \DB::select("select * from (
    select * from (
        select min(at.id) as ID, at.user_id, emp.name, emp.email_id, at.date, clock_in_time, 'clock_in_time' descrip from attendance at
            Left Join employee_masters emp on emp.id = at.user_id
            GROUP BY date ORDER BY date, ID
        ) as clock_in_time
    union all
    select * from (
        select max(at.id) as ID, at.user_id, emp.name, emp.email_id, at.date, clock_in_time, 'clock_out_time' descrip from attendance at
            Left Join employee_masters emp on emp.id = at.user_id
            GROUP BY date ORDER BY date, ID
        ) as clock_out_time
    union all
    select * from (
        select max(at.id) as ID, at.user_id, emp.name, emp.email_id, at.date, attendance_remark_code_val, 'attendance_status' descrip from attendance at
            Left Join employee_masters emp on emp.id = at.user_id
            GROUP BY date ORDER BY date, ID
        ) as status
    ) as pivot
ORDER BY date;");

        $export = new AttendanceHorizontalExportView( $attendance);
        return \Excel::download($export, ' Attendance_export.xlsx');
    }
}
