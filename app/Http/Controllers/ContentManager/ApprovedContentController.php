<?php

namespace App\Http\Controllers\ContentManager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Masters\ProjectMaster;
use App\Model\ContentManager\ContentManager;
use App\Model\ContentManager\ContentFiles;

class ApprovedContentController extends Controller
{
    public function index(Request $request){
    	$content_files = [];
        // return $request->all();
         if (count((array)$request->all()) == 0) {
            \Session::forget('data');
            $content_files = ContentFiles::orderBy('id', 'asc')->where('approval_status',1);
        }
        else {
            if(isset($request->search) && count($request->search) > 0)
            {
                \Session::forget('data');
            }
            \Session::push('data', $request->search);
             $content_files = ContentFiles::orderBy('id', 'asc')->where('approval_status',1);
            if (count((array)\Session::get('data')[0]) > 0) {
                                
                $query = ContentFiles::orderBy('id', 'asc')->where('approval_status',1);
                
                foreach (\Session::get('data')[0] as $content_file)
                {
                    
                    if( $content_file['column'] == 'project_name' ){
                    	$query->whereHas('project', function($query1) use ($content_file){
                        	$query1->where('name', 'LIKE', '%'. $content_file['search_data'] .'%');
                    	});
                    }

                    if( $content_file['column'] == 'donor' ){
                    	$query->whereHas('project', function($query1) use ($content_file){
                        	$query1->where('donor', 'LIKE', '%'. $content_file['search_data'] .'%');
                    	});
                    }

                    if( $content_file['column'] == 'photo_period' ){
                    	$query->where('photo_period', 'LIKE', '%'. $content_file['search_data'] .'%');
                    }
                    if( $content_file['column'] == 'date' ){
                    	$query->where('date', date_ymd($content_file['search_data']) );
                    }
                    if( $content_file['column'] == 'uploaded_by' ){
                    	$query->whereHas('created_by', function($query1) use ($content_file){
                        	$query1->where('name', 'LIKE', '%'. $content_file['search_data'] .'%');
                    	});
                    }

                    if( $content_file['column'] == 'file_caption' ){
                    	$query->where('file_caption', 'LIKE', '%'. $content_file['search_data'] .'%');
                    }
                    if( $content_file['column'] == 'content_type' ){
                    	$query->where('content_type', 'LIKE', '%'. $content_file['search_data'] .'%');
                    }
                }
                $content_files = $query;
                
            }
        }

        $content_files = $content_files->paginate(10);
    	$columns = ['project_name', 'photo_period', 'donor', 'date', 'uploaded_by', 'file_caption', 'content_type'];
    	return view('content_manager.approved_content.index', compact('columns', 'content_files'));
    }
}
