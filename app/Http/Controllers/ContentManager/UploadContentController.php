<?php

namespace App\Http\Controllers\ContentManager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Model\Masters\ProjectMaster;
use App\Model\Masters\ProjectEmployeeMapping;
use App\Model\ContentManager\ContentManager;
use App\Model\ContentManager\ContentFiles;
use Illuminate\Http\File;
use App\Providers\GoogleDriveServiceProvider;
class UploadContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $projects = [];
        // return $request->all();
         if (count((array)$request->all()) == 0) {
            \Session::forget('data');
            $projects = ProjectMaster::orderBy('id', 'asc');

            if( !\Auth::user()->hasRole('Super Admin') ){
                $employee_mappings = ProjectEmployeeMapping::where('employee_id', \Auth::user()->id)->pluck('project_id');
                $projects = $projects->whereIn('id', $employee_mappings);
            }
        }
        else {
            if(isset($request->search) && count($request->search) > 0)
            {
                \Session::forget('data');
            }
            \Session::push('data', $request->search);
             $projects = ProjectMaster::orderBy('id', 'asc');

             if( !\Auth::user()->hasRole('Super Admin') ){
                 $employee_mappings = ProjectEmployeeMapping::where('employee_id', \Auth::user()->id)->pluck('project_id');
                 $projects = $projects->whereIn('id', $employee_mappings);
             }
            if (count((array)\Session::get('data')[0]) > 0) {
                                
                $query = ProjectMaster::orderBy('id', 'asc');

                if( !\Auth::user()->hasRole('Super Admin') ){
                    $employee_mappings = ProjectEmployeeMapping::where('employee_id', \Auth::user()->id)->pluck('project_id');
                    $query = $query->whereIn('id', $employee_mappings);
                }
                
                foreach (\Session::get('data')[0] as $project)
                {
                    
                    if( $project['column'] == 'project_name' ){
                        $query->where('name', 'LIKE', '%'. $project['search_data'] .'%');
                    }
                }
                $projects = $query;
                
            }
        }

        $projects = $projects->paginate(10);
        $columns = ['project_name'];
        return view('content_manager.upload_content.index', compact('columns', 'projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if( isset( $request->id ) ){
            $id = $request->id;
            $project = ProjectMaster::find($id);
            return view('content_manager.upload_content.create', compact('project'));
        }
        else{
            $error = Collection::make( ["Something Went Wrong "] );
            return redirect()->back()->with('errors', $error)->withInput( $request->input() );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $project_id = $request->project_id;
        $date = date_ymd($request->date);
        $created_by = \Auth::user()->id;
        $content_type = $request->content_type;
        $content_period = $request->content_period;
        $file_captions = $request->content_caption;
        \DB::beginTransaction();

            $content_manager = new ContentManager();
            $content_manager->project_id = $project_id;
            $content_manager->date = $date;
            $content_manager->created_by = $created_by;
            $content_manager->save();

            foreach ( $request->content as $key => $value) {
                if(isset($request->content))
                {

                    $content_file = new ContentFiles();
                    $content_file->content_manager_id = $content_manager->id;
                    $content_file->project_id = $project_id;
                    $content_file->content_type = $content_type;
                    $content_file->content_period = $content_period;
                    $content_file->date = $date;
                    

                    $file = $request->content[$key];
                    $file_name = explode(".", $file->getClientOriginalName());
                    $filename = $file_name[0];
                    $ext = $file_name[ sizeof($file_name)-1 ];
                    $file_path = '/uploads/content_manager';
                    $file_name = str_replace(' ', '_', strtolower($filename))."_".$content_manager->id. '.' . $ext;

                   \Storage::disk('google')->put($file_name, file_get_contents($file));

                    // Get the uploaded file to store it's Google Drive File ID
                    $dir = '/';
                    $recursive = false; // Get subdirectories also?
                    $contents = collect(\Storage::disk('google')->listContents($dir, $recursive));

                    $gdrive_uploaded_file = $contents
                        ->where('type', '=', 'file')
                        ->where('filename', '=', pathinfo($file_name, PATHINFO_FILENAME))
                        ->where('extension', '=', pathinfo($file_name, PATHINFO_EXTENSION))
                        ->first(); // there can be duplicate file names!

                    $content_file->file_path = $gdrive_uploaded_file['path'];
                    $content_file->file_name = $file_name;
                    $content_file->file_caption = $file_captions[$key];
                    $content_file->save();
                }
            };
        \DB::commit();
        session()->flash('status',"Files Sent for Approval Successfully");
        return redirect()->route('upload-content.index');
    }

    public function get_access_token(){
        $client = new \GuzzleHttp\Client();
        $request = $client->post('https://oauth2.googleapis.com/token?client_id=374216423712-atmenfd24jvr29vo9mefannt0ab0p068.apps.googleusercontent.com&client_secret=Cz5IvTepJ2LUneVVn4hla5Ea&refresh_token=1//04ibvSySu4ykYCgYIARAAGAQSNwF-L9IrsTfebYE787HQjk1SXc2lyH8j9oTUi89zz_8Ik1CJS4ujtuMsbLg032dMrsmotck5ieg&grant_type=refresh_token');
        
        $response = $request->getBody();
        $response_json = json_decode($response, true);
        $access_token = $response_json['access_token'];

        putenv ("gdrive_access_token=$access_token");
        \Artisan::call('config:cache');
        dd( env('gdrive_access_token') );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
