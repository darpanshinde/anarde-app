<?php

namespace App\Http\Controllers\ContentManager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Masters\ProjectMaster;
use App\Model\ContentManager\ContentFiles;
use Illuminate\Support\Facades\File; 
class PendingApprovalContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $projects = [];
        // return $request->all();
         if (count((array)$request->all()) == 0) {
            \Session::forget('data');
            $projects = ProjectMaster::orderBy('id', 'asc');
        }
        else {
            if(isset($request->search) && count($request->search) > 0)
            {
                \Session::forget('data');
            }
            \Session::push('data', $request->search);
             $projects = ProjectMaster::orderBy('id', 'asc');
            if (count((array)\Session::get('data')[0]) > 0) {
                                
                $query = ProjectMaster::orderBy('id', 'asc');
                
                foreach (\Session::get('data')[0] as $project)
                {
                    if( $project['column'] == 'project_name' ){
                        $query->where('name', 'LIKE', '%'. $project['search_data'] .'%');
                    }
                }
                $projects = $query;
                
            }
        }

        $projects = $projects->paginate(10);
        $columns = ['project_name'];
        return view('content_manager.pending_approval_content.index', compact('columns', 'projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $project = ProjectMaster::find($id);

        if( isset($request->content_type) ){

            \Session::forget('content_type');
            \Session::forget('content_period');

            \Session::push('content_type', $request->content_type);
            \Session::push('content_period', $request->content_period);
        }

        $page =1;
        if( isset($request->page) ){
            $page = $request->page;
        }

        $content_type = \Session::get('content_type')[0];
        $content_period = \Session::get('content_period')[0];
        $content_files = ContentFiles::where('project_id', $id)->where('content_type', $content_type)->where('content_period', $content_period)->whereNull('approval_status')->paginate(3, // per page (may be get it from request)
                                ['*'], // columns to select from table (default *, means all fields)
                                'page', // page name that holds the page number in the query string
                                $page // current page, default 1
                                );

        return view('content_manager.pending_approval_content.edit', compact('project', 'content_files'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $project = ProjectMaster::find($id);

        \DB::beginTransaction();
            foreach ($request->content_file as $id => $data) {

                $content_file = ContentFiles::find($id);
                $content_file->file_caption = $data['file_caption'];
                $content_file->date = date_ymd($data['date']);
                
                if( !is_null($data['approval_status']) ){
                    $content_file->approval_status = $data['approval_status'];
                    if( $data['approval_status'] == 0){
                        \Storage::disk('google')->delete($content_file->file_path);
                    }
                }

                if(array_key_exists('content', $data))
                {
                    $file = $data['content'];
                    $file_name = explode(".", $file->getClientOriginalName());
                    $filename = $file_name[0];
                    $ext = $file_name[ sizeof($file_name)-1 ];
                    $file_path = '\uploads\content_manager';
                    $file_name = str_replace(' ', '_', strtolower($filename))."_".$content_file->content_manager_id. '.' . $ext;
                    
                    // Delete previous File
                        \Storage::disk('google')->delete($content_file->file_path);

                    \Storage::disk('google')->put($file_name, file_get_contents($file));

                    // Get the uploaded file to store it's Google Drive File ID
                    $dir = '/';
                    $recursive = false; // Get subdirectories also?
                    $contents = collect(\Storage::disk('google')->listContents($dir, $recursive));

                    $gdrive_uploaded_file = $contents
                        ->where('type', '=', 'file')
                        ->where('filename', '=', pathinfo($file_name, PATHINFO_FILENAME))
                        ->where('extension', '=', pathinfo($file_name, PATHINFO_EXTENSION))
                        ->first(); // there can be duplicate file names!

                    $content_file->file_path = $gdrive_uploaded_file['path'];
                    $content_file->file_name = $file_name;
                }

                $content_file->update();    
        }
        \DB::commit();
        session()->flash('status',"Files Updated Successfully");
        return redirect()->route('pending-approval-content.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
