<?php

namespace App\Http\Controllers\Masters;

use App\Model\Masters\OfficeHoursMaster;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
class OfficeHoursMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $office_hours = OfficeHoursMaster::get();
        $count = OfficeHoursMaster::count(); // to check if a row already exists or not in the table. Only one row is allowed.
        return view('masters.office_hours_master.index', compact('office_hours', 'count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masters.office_hours_master.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([ 
            'start_of_day' => 'required', 
            'end_of_day' => 'required', 
            'late_cutoff_time' => 'required', 
            'half_day_cutoff_time' => 'required', 
        ]);

        $data = $request->all();
        $data['created_by'] = \Auth::user()->id;
        OfficeHoursMaster::create($data);

        session()->flash('status',"Office Hours Successfully Created");
        return redirect()->route('office-hours-master.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Masters\OfficeHoursMaster  $officeHoursMaster
     * @return \Illuminate\Http\Response
     */
    public function show(OfficeHoursMaster $officeHoursMaster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Masters\OfficeHoursMaster  $officeHoursMaster
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $office_hours = OfficeHoursMaster::find($id);
        return view('masters.office_hours_master.edit', compact('office_hours'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Masters\OfficeHoursMaster  $officeHoursMaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([ 
            'start_of_day' => 'required', 
            'end_of_day' => 'required', 
            'late_cutoff_time' => 'required', 
            'half_day_cutoff_time' => 'required', 
        ]);

        $data = $request->all();
        $data['updated_by'] = \Auth::user()->id;
        OfficeHoursMaster::find($id)->update($data);

        session()->flash('status',"Office Hours Successfully Updated");
        return redirect()->route('office-hours-master.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Masters\OfficeHoursMaster  $officeHoursMaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(OfficeHoursMaster $officeHoursMaster)
    {
        //
    }
}
