<?php

namespace App\Http\Controllers\Masters;

use App\Model\Masters\LocationCentreMaster;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
class LocationCentreMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $location_centres = [];
        // return $request->all();
         if (count((array)$request->all()) == 0) {
            \Session::forget('data');
            $location_centres = LocationCentreMaster::orderBy('id', 'asc');
        }
        else {
            if(isset($request->search) && count($request->search) > 0)
            {
                \Session::forget('data');
            }
            \Session::push('data', $request->search);
             $location_centres = LocationCentreMaster::orderBy('id', 'asc');
            if (count((array)\Session::get('data')[0]) > 0) {
                                
                $query = LocationCentreMaster::orderBy('id', 'asc');
                
                foreach (\Session::get('data')[0] as $location_centre)
                {
                    
                    if( $location_centre['column'] == 'name' ){
                        $query->where('name', 'LIKE', '%'. $location_centre['search_data'] .'%');
                    }

                    if( $location_centre['column'] == 'centre_id' ){
                        $query->where('centre_id',$location_centre['search_data']);
                    }

                    
                }
                $location_centres = $query;
                
            }
        }

        $location_centres = $location_centres->paginate(10);
        $columns = ['name', 'centre_id'];
        return view('masters.location_centre_master.index', compact('columns','location_centres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masters.location_centre_master.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //Validation to check if the required fields to store the data are available or not
        $validatedData = $request->validate([ 
            'name' => 'required|unique:location_centre_masters', //Name of the location centre should be unique in the table
            'active' => 'required',
        ]);

        $data= $request->all();
        $data['created_by'] = \Auth::user()->id;

        LocationCentreMaster::create($data);

        session()->flash('status',"Location Centre Successfully Created");
        return redirect()->route('location-centre-master.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\LocationCentreMaster  $locationCentreMaster
     * @return \Illuminate\Http\Response
     */
    public function show(LocationCentreMaster $locationCentreMaster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\LocationCentreMaster  $locationCentreMaster
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $location_centre= LocationCentreMaster::find($id);
        return view('masters.location_centre_master.edit', compact('location_centre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\LocationCentreMaster  $locationCentreMaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = $request->validate([
            'name' => ['required', Rule::unique('location_centre_masters')->ignore($id)],
            'active' => 'required',
        ]);
        
        $data = $request->all();
        $data['updated_by'] = \Auth::user()->id;
        
        LocationCentreMaster::find($id)->update($data);

        session()->flash('status',"Location Centre Successfully Updated");

        return redirect()->route('location-centre-master.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\LocationCentreMaster  $locationCentreMaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(LocationCentreMaster $locationCentreMaster)
    {
        //
    }
}
