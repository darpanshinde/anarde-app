<?php

namespace App\Http\Controllers\Masters;

use App\Model\Masters\ProjectMaster;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Model\Masters\LocationCentreMaster;
use App\Model\Masters\EmployeeMaster;
use App\Model\Masters\ProjectEmployeeMapping;
use App\Model\Masters\DepartmentCentreMaster;
use App\Model\Masters\State;
class ProjectMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $projects = [];
        // return $request->all();
         if (count((array)$request->all()) == 0) {
            \Session::forget('data');
            $projects = ProjectMaster::orderBy('id', 'asc');
        }
        else {
            if(isset($request->search) && count($request->search) > 0)
            {
                \Session::forget('data');
            }
            \Session::push('data', $request->search);
             $projects = ProjectMaster::orderBy('id', 'asc');
            if (count((array)\Session::get('data')[0]) > 0) {
                                
                $query = ProjectMaster::orderBy('id', 'asc');
                
                foreach (\Session::get('data')[0] as $project)
                {
                    
                    if( $project['column'] == 'name' ){
                        $query->where('name', 'LIKE', '%'. $project['search_data'] .'%');
                    }
                    if( $project['column'] == 'project_description' ){
                        $query->where('project_description', 'LIKE', '%'. $project['search_data'] .'%');
                    }
                    if( $project['column'] == 'state_id' ){
                        $query->where('state_id', $project['search_data']);
                    }
                    if( $project['column'] == 'donor' ){
                        $query->where('donor', 'LIKE', '%'. $project['search_data'] .'%');
                    }
                    if( $project['column'] == 'financial_year' ){
                        $query->where('financial_year', $project['search_data']);
                    }
                    if( $project['column'] == 'email_id' ){
                        $query->where('email_id', $project['search_data']);
                    }
                    if( $project['column'] == 'manager_name' ){
                        $query->where('name', 'LIKE', '%'. $project['search_data'] .'%');
                    }
                    if( $project['column'] == 'location_centre' ){
                        $query->WhereHas('location_centre', function ($query1) use ($project) {
                            $query1->where('name', 'LIKE', '%' . $project['search_data'] . '%');
                        });
                    }
                    if( $project['column'] == 'employees_assigned' ){
                        $query->WhereHas('employee_mappings', function ($query1) use ($project) {
                                $query1->WhereHas('employee', function ($query2) use ($project) {
                                    $query2->where('name', 'LIKE', '%' . $project['search_data'] . '%');
                                });
                        });
                    }


                    if( $project['column'] == 'sanitation_project' ){
                        if( $project['search_data'] == "yes" )
                            $query->where('has_beneficiaries', 1);
                        elseif( $project['search_data'] == "no" )
                            $query->where('has_beneficiaries', 0);
                    }
                    if( $project['column'] == 'status' ){
                        if( $project['search_data'] == "active" )
                            $query->where('active', 1);
                        elseif( $project['search_data'] == "inactive" )
                            $query->where('active', 0);
                    }

                    
                }
                $projects = $query;
                
            }
        }

        $projects = $projects->paginate(10);
        $columns = ['name','project_description', 'state_id', 'donor', 'location_centre', 'financial_year', 'employees_assigned' ,  'sanitation_project', 'status'];
        return view('masters.project_master.index',compact('columns','projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $location_centres = LocationCentreMaster::pluck('name', 'id');
        $department_centres = DepartmentCentreMaster::pluck('name', 'id');
        $employees = EmployeeMaster::pluck('name', 'id');
        $states= State::pluck('name','id');
        return view('masters.project_master.create', compact('location_centres', 'employees', 'states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validation to check if the required fields to store the data are available or not
        $validatedData = $request->validate([ 
            'name' => 'required', 
            'state_id' => 'required',
            'location_centre_id' => 'required',
            'has_beneficiaries' => 'required',
        ]);

        //store the data in an array
        $data = $request->all();
        $data['created_by'] = \Auth::user()->id; //Add the user who is performing the store acction

        //Encompass under Begin transaction so that if error occurs at any step then everything is reverted back
        \DB::BeginTransaction();

            $project = ProjectMaster::create($data);

            //Store all the mapped employees in project_employee_mapping table
            foreach ($data['employee_ids'] as $key => $employee_id) {
                $employee_mapping['project_id'] = $project->id;
                $employee_mapping['employee_id'] = $employee_id;

                ProjectEmployeeMapping::create($employee_mapping);
            }

        \DB::commit();

        session()->flash('status',"Project Successfully Created");
        return redirect()->route('project-master.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Masters\ProjectMaster  $projectMaster
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectMaster $projectMaster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Masters\ProjectMaster  $projectMaster
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = ProjectMaster::find($id);
        $location_centres = LocationCentreMaster::pluck('name', 'id');
        $department_centres = DepartmentCentreMaster::pluck('name', 'id');
        
        $employees = EmployeeMaster::pluck('name', 'id');
        $selected_employees = ProjectEmployeeMapping::where('project_id', $id)->pluck('employee_id')->toArray();
        $states= State::pluck('name','id');
        return view('masters.project_master.edit', compact('location_centres', 'employees', 'states', 'project', 'selected_employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Masters\ProjectMaster  $projectMaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $validatedData = $request->validate([ 
            'name' => 'required', 
            'state_id' => 'required',
            'donor' => 'required',
            'location_centre_id' => 'required',
            'financial_year' => 'required',
            'has_beneficiaries' => 'required',
        ]);

        //store the data in an array
        $data = $request->all();
        $data['updated_by'] = \Auth::user()->id; //Add the user who is performing the update acction

        //Encompass under Begin transaction so that if error occurs at any step then everything is reverted back
        \DB::BeginTransaction();

            $project = ProjectMaster::find($id)->update([$data]);

            //Delete all the previously mapped employees
            ProjectEmployeeMapping::where('project_id', $id)->delete();

            //Store all the mapped employees in project_employee_mapping table
            foreach ($data['employee_ids'] as $key => $employee_id) {
                $employee_mapping['project_id'] = $id;
                $employee_mapping['employee_id'] = $employee_id;

                ProjectEmployeeMapping::create($employee_mapping);
            }

        \DB::commit();

        session()->flash('status',"Project Successfully Updated");
        return redirect()->route('project-master.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Masters\ProjectMaster  $projectMaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectMaster $projectMaster)
    {
        //
    }
}
