<?php

namespace App\Http\Controllers\Masters;

use App\Model\Masters\DepartmentCentreMaster;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class DepartmentCentreMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $department_centres = [];
        // return $request->all();
         if (count((array)$request->all()) == 0) {
            \Session::forget('data');
            $department_centres = DepartmentCentreMaster::orderBy('id', 'asc');
        }
        else {
            if(isset($request->search) && count($request->search) > 0)
            {
                \Session::forget('data');
            }
            \Session::push('data', $request->search);
             $department_centres = DepartmentCentreMaster::orderBy('id', 'asc');
            if (count((array)\Session::get('data')[0]) > 0) {
                                
                $query = DepartmentCentreMaster::orderBy('id', 'asc');
                
                foreach (\Session::get('data')[0] as $department_centre)
                {
                    
                    if( $department_centre['column'] == 'name' ){
                        $query->where('name', 'LIKE', '%'. $department_centre['search_data'] .'%');
                    }

                    if( $department_centre['column'] == 'status' ){
                        if( $department_centre['search_data'] == "active" )
                            $query->where('active', 1);
                        elseif( $department_centre['search_data'] == "inactive" )
                            $query->where('active', 0);
                    }

                    
                }
                $department_centres = $query;
                
            }
        }

        $department_centres = $department_centres->paginate(10);
        $columns = ['name', 'status'];
        return view('masters.department_centre_master.index', compact('columns', 'department_centres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masters.department_centre_master.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validation to check if the required fields to store the data are available or not
        $validatedData = $request->validate([ 
            'name' => 'required|unique:department_centre_masters', //Name of the Department centre should be unique in the table
            'active' => 'required',
        ]);

        $data= $request->all();
        $data['created_by'] = \Auth::user()->id;

        DepartmentCentreMaster::create($data);

        session()->flash('status',"Department Centre Successfully Created");
        return redirect()->route('department-centre-master.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Masters\DepartmentCentreMaster  $departmentCentreMaster
     * @return \Illuminate\Http\Response
     */
    public function show(DepartmentCentreMaster $departmentCentreMaster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Masters\DepartmentCentreMaster  $departmentCentreMaster
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $department_centre= DepartmentCentreMaster::find($id);
        return view('masters.department_centre_master.edit', compact('department_centre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Masters\DepartmentCentreMaster  $departmentCentreMaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => ['required', Rule::unique('department_centre_masters')->ignore($id)],
            'active' => 'required',
        ]);
        
        $data = $request->all();
        $data['updated_by'] = \Auth::user()->id;
        
        DepartmentCentreMaster::find($id)->update($data);

        session()->flash('status',"Department Centre Successfully Updated");

        return redirect()->route('department-centre-master.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Masters\DepartmentCentreMaster  $departmentCentreMaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(DepartmentCentreMaster $departmentCentreMaster)
    {
        //
    }
}
