<?php

namespace App\Http\Controllers\Masters;

use App\Model\Masters\BeneficiaryMaster;
use Illuminate\Http\Request;
use App\Model\Masters\ProjectMaster;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
class BeneficiaryMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $beneficiaries = [];
        // return $request->all();
         if (count((array)$request->all()) == 0) {
            \Session::forget('data');
            $beneficiaries = BeneficiaryMaster::orderBy('id', 'asc');
        }
        else {
            if(isset($request->search) && count($request->search) > 0)
            {
                \Session::forget('data');
            }
            \Session::push('data', $request->search);
             $beneficiaries = BeneficiaryMaster::orderBy('id', 'asc');
            if (count((array)\Session::get('data')[0]) > 0) {
                                
                $query = BeneficiaryMaster::orderBy('id', 'asc');
                
                foreach (\Session::get('data')[0] as $beneficiary)
                {
                    
                    if( $beneficiary['column'] == 'name' ){
                        $query->where('name', 'LIKE', '%'. $beneficiary['search_data'] .'%');
                    }
                    if( $beneficiary['column'] == 'mobile_number' ){
                        $query->where('mobile_number', $beneficiary['search_data']);
                    }
                    if( $beneficiary['column'] == 'address' ){
                        $query->where('name', 'LIKE', '%'. $beneficiary['search_data'] .'%');
                    }
                    if( $beneficiary['column'] == 'photo_name' ){
                        $query->where('name', 'LIKE', '%'. $beneficiary['search_data'] .'%');
                    }
                    if( $beneficiary['column'] == 'project' ){
                        $query->WhereHas('project', function ($query1) use ($beneficiary) {
                            $query1->where('name', 'LIKE', '%' . $beneficiary['search_data'] . '%');
                        });
                    }

                    if( $beneficiary['column'] == 'status' ){
                        if( $beneficiary['search_data'] == "active" )
                            $query->where('active', 1);
                        elseif( $beneficiary['search_data'] == "inactive" )
                            $query->where('active', 0);
                    }

                    
                }
                $beneficiaries = $query;
                
            }
        }

        $beneficiaries = $beneficiaries->paginate(10);
        $columns = ['name','project','mobile_number','address','photo_name' ,'status',];
        return view('masters.beneficiary_master.index',compact('columns','beneficiaries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $project = ProjectMaster::find($request->project_id);

        if( !isset($request->project_id)){

            session()->flash('status',"Project Not Available");
            return redirect()->route('project-master.index');
        }
        if( $project == NULL ){

            session()->flash('status',"Project Not Found");
            return redirect()->route('project-master.index');
        }


        return view('masters.beneficiary_master.create', compact('project'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->except('_token');
        $data['photo_name'] = $request->photo->getClientOriginalName();

        \DB::beginTransaction();
        $beneficiary = BeneficiaryMaster::create($data);


        if(isset($request->photo))
        {
            $file = $request->photo;
            $file_name = explode(".", $file->getClientOriginalName());
            $filename = $file_name[0];
            $ext = $file_name[ sizeof($file_name)-1 ];
            $data['file_path'] = '/uploads/beneficiaries';
            $data['beneficiary_file_name'] = str_replace(' ', '_', strtolower($request->name))."_".$beneficiary->id. '.' . $ext;

            $destinationPath = 'uploads/beneficiaries';

            if ($file->move($destinationPath, $data['beneficiary_file_name'])) {
                
            }
        }
        \DB::commit();

        session()->flash('status',"Beneficiary Added");
        return redirect()->route('beneficiary-master.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Masters\BeneficiaryMaster  $beneficiaryMaster
     * @return \Illuminate\Http\Response
     */
    public function show(BeneficiaryMaster $beneficiaryMaster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Masters\BeneficiaryMaster  $beneficiaryMaster
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $beneficiary = BeneficiaryMaster::find($id);
        return view('masters.beneficiary_master.edit', compact('beneficiary'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Masters\BeneficiaryMaster  $beneficiaryMaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token');
        $data['photo_name'] = $request->photo->getClientOriginalName();

        \DB::beginTransaction();
        $beneficiary = BeneficiaryMaster::find($id);
        $update = $beneficiary->update($data);

        if(isset($request->photo))
        {
            $file = $request->photo;
            $file_name = explode(".", $file->getClientOriginalName());
            $filename = $file_name[0];
            $ext = $file_name[ sizeof($file_name)-1 ];
            $data['file_path'] = '/uploads/beneficiaries';
            $data['beneficiary_file_name'] = str_replace(' ', '_', strtolower($beneficiary->name))."_".$id. '.' . $ext;

            $destinationPath = 'uploads/beneficiaries';

            if ($file->move($destinationPath, $data['beneficiary_file_name'])) {
                
            }
        }
        \DB::commit();

        session()->flash('status',"Beneficiary Updated Successfully");
        return redirect()->route('beneficiary-master.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Masters\BeneficiaryMaster  $beneficiaryMaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(BeneficiaryMaster $beneficiaryMaster)
    {
        //
    }
}
