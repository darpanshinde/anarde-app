<?php

namespace App\Http\Controllers\Masters;

use App\Model\Masters\EmployeeMaster;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Model\Masters\LocationCentreMaster;
use App\Model\Masters\DepartmentCentreMaster;
use App\Model\Masters\Role;
class EmployeeMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $employees = [];
        // return $request->all();
         if (count((array)$request->all()) == 0) {
            \Session::forget('data');
            $employees = EmployeeMaster::orderBy('id', 'asc');
        }
        else {
            if(isset($request->search) && count($request->search) > 0)
            {
                \Session::forget('data');
            }
            \Session::push('data', $request->search);
             $employees = EmployeeMaster::orderBy('id', 'asc');
            if (count((array)\Session::get('data')[0]) > 0) {
                                
                $query = EmployeeMaster::orderBy('id', 'asc');
                
                foreach (\Session::get('data')[0] as $employee)
                {
                    
                    if( $employee['column'] == 'name' ){
                        $query->where('name', 'LIKE', '%'. $employee['search_data'] .'%');
                    }
                    if( $employee['column'] == 'mobile_number' ){
                        $query->where('mobile_number', $employee['search_data']);
                    }
                    if( $employee['column'] == 'employee_uid' ){
                        $query->where('employee_uid', $employee['search_data']);
                    }
                    if( $employee['column'] == 'email_id' ){
                        $query->where('email_id', $employee['search_data']);
                    }
                    if( $employee['column'] == 'manager_name' ){
                        $query->where('name', 'LIKE', '%'. $employee['search_data'] .'%');
                    }
                    if( $employee['column'] == 'location_centre' ){
                        $query->WhereHas('location_centre', function ($query1) use ($employee) {
                            $query1->where('name', 'LIKE', '%' . $employee['search_data'] . '%');
                        });
                    }
                    if( $employee['column'] == 'department_centre' ){
                        $query->WhereHas('department_centre', function ($query1) use ($employee) {
                            $query1->where('name', 'LIKE', '%' . $employee['search_data'] . '%');
                        });
                    }


                    if( $employee['column'] == 'status' ){
                        if( $employee['search_data'] == "active" )
                            $query->where('active', 1);
                        elseif( $employee['search_data'] == "inactive" )
                            $query->where('active', 0);
                    }

                    
                }
                $employees = $query;
                
            }
        }

        $employees = $employees->paginate(10);
        $columns = ['name','mobile_number', 'employee_uid', 'email_id', 'manager_name', 'location_centre', 'department_centre', 'role', 'status'];
        return view('masters.employee_master.index', compact('columns', 'employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $location_centres = LocationCentreMaster::pluck('name', 'id');
        $department_centres = DepartmentCentreMaster::pluck('name', 'id');
        $roles = Role::pluck('name','id');
        $manager_id = Role::where('name','%like%','Manager')->first();
        $managers = EmployeeMaster::where('role_id', $manager_id)->pluck('name','id');
        return view('masters.employee_master.create', compact('location_centres', 'department_centres', 'roles', 'managers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mobile_number = $request->mobile_number;
        
        //Validate if mobile number is numeric
        if( !is_numeric($mobile_number)){

            $error = Collection::make( ["Mobile Number is invalid "] );
            return redirect()->back()->with('errors', $error)->withInput( $request->input() );
        }

        //Validate if mobile number is numeric
        if( strlen($mobile_number) != 10 ){
            $error = Collection::make( ["Mobile Number needs to be of 10 digits "] );
            return redirect()->back()->with('errors', $error)->withInput( $request->input() );
        }
        //Validation to check if the required fields to store the data are available or not
        $validatedData = $request->validate([ 
            'name' => 'required', 
            'mobile_number' => 'required|unique:employee_masters', //Mobile Number should be unique in the table
            'active' => 'required',
            'email_id' => 'required',
            'password' => 'required',
            'location_centre_id' => 'required',
            'department_centre_id' => 'required',
            'role_id' => 'required',
        ]);

        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $data['created_by'] = \Auth::user()->id;
        $employee = EmployeeMaster::Create($data);

        $assigned_role = Role::find("$request->role_id");
        $employee->assignRole($assigned_role->name);

        session()->flash('status',"Employee Successfully Created");
        return redirect()->route('employee-master.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Masters\EmployeeMaster  $employeeMaster
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeMaster $employeeMaster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Masters\EmployeeMaster  $employeeMaster
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $employee = EmployeeMaster::find($id);
        $location_centres = LocationCentreMaster::pluck('name', 'id');
        $department_centres = DepartmentCentreMaster::pluck('name', 'id');
        $roles = Role::pluck('name','id');
        $manager_id = Role::where('name','like','%Manager%')->first()->id;
        $managers = EmployeeMaster::where('role_id', $manager_id)->pluck('name','id');

        return view('masters.employee_master.edit', compact('employee', 'location_centres', 'department_centres', 'roles', 'managers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Masters\EmployeeMaster  $employeeMaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mobile_number = $request->mobile_number;
        
        //Validate if mobile number is numeric
        if( !is_numeric($mobile_number)){

            $error = Collection::make( ["Mobile Number is invalid "] );
            return redirect()->back()->with('errors', $error)->withInput( $request->input() );
        }

        //Validate if mobile number is numeric
        if( strlen($mobile_number) != 10 ){
            $error = Collection::make( ["Mobile Number needs to be of 10 digits "] );
            return redirect()->back()->with('errors', $error)->withInput( $request->input() );
        }
        //Validation to check if the required fields to store the data are available or not
        $validatedData = $request->validate([ 
            'name' => 'required', //Name of the Department centre should be unique in the table
            'mobile_number' => ['required', Rule::unique('employee_masters')->ignore($id)], //Mobile Number should be unique in the table
            'active' => 'required',
            'email_id' => 'required',
            'location_centre_id' => 'required',
            'department_centre_id' => 'required',
            'role_id' => 'required',
            'bypass_late_remarks' => 'required',
        ]);

        $data = $request->all(); //collect the employee data in array
        $data['updated_by'] = \Auth::user()->id; //add the user who is performing the update action
        
        if( $data['password'] ==  "" ){
            unset($data['password']);
        }
        else{
            $data['password'] = bcrypt($data['password']);
        }
        

        EmployeeMaster::find($id)->update($data); //update the data in database


        \DB::table('model_has_roles')->where('model_id',$id)->delete();

        $employee = EmployeeMaster::find($id);
        $assigned_role = Role::find("$request->role_id");
        $employee->assignRole($assigned_role->name);

        session()->flash('status',"Employee Successfully Updated");
        return redirect()->route('employee-master.index');
    }

    public function get_employee_details($id){
        $employee = EmployeeMaster::find($id);
        $employee->department_name = $employee->department_centre->name;
        $employee->location_centre_name = $employee->location_centre->name;
        return ['success' => $employee];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Masters\EmployeeMaster  $employeeMaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeMaster $employeeMaster)
    {
        //
    }
}
