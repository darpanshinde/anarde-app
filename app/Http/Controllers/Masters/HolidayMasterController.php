<?php

namespace App\Http\Controllers\Masters;

use App\Model\Masters\HolidayMaster;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;

class HolidayMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $holidays = [];
        // return $request->all();
         if (count((array)$request->all()) == 0) {
            \Session::forget('data');
            $holidays = HolidayMaster::orderBy('id', 'asc');
        }
        else {
            if(isset($request->search) && count($request->search) > 0)
            {
                \Session::forget('data');
            }
            \Session::push('data', $request->search);
             $holidays = HolidayMaster::orderBy('id', 'asc');
            if (count((array)\Session::get('data')[0]) > 0) {
                                
                $query = HolidayMaster::orderBy('id', 'asc');
                
                foreach (\Session::get('data')[0] as $holiday)
                {
                    
                    if( $holiday['column'] == 'name' ){
                        $query->where('name', 'LIKE', '%'. $holiday['search_data'] .'%');
                    }

                    if( $holiday['column'] == 'date' ){
                        $query->where('date', date_ymd($holiday['search_data']) );
                    }

                    
                }
                $holidays = $query;
                
            }
        }

        $holidays = $holidays->paginate(10);
        $columns = ['name', 'date'];
        return view('masters.holiday_master.index', compact('columns', 'holidays'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masters.holiday_master.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        //Validation to check if the required fields to store the data are available or not
        $validatedData = $request->validate([ 
            'name' => 'required', 
            'date' => 'required', 
        ]);

        $data['date'] = date_ymd($data['date']);
        $data['created_by'] = \Auth::user()->id;
        HolidayMaster::create($data);

        session()->flash('status',"Holiday Successfully Created");
        return redirect()->route('holiday-master.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Masters\HolidayMaster  $holidayMaster
     * @return \Illuminate\Http\Response
     */
    public function show(HolidayMaster $holidayMaster)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Masters\HolidayMaster  $holidayMaster
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $holiday = HolidayMaster::find($id);
        return view('masters.holiday_master.edit', compact('holiday'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Masters\HolidayMaster  $holidayMaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        //Validation to check if the required fields to store the data are available or not
        $validatedData = $request->validate([ 
            'name' => 'required', 
            'date' => 'required', 
        ]);

        $data['date'] = date_ymd($data['date']);
        $data['updated_by'] = \Auth::user()->id;
        HolidayMaster::find($id)->update($data);

        session()->flash('status',"Holiday Successfully updated");
        return redirect()->route('holiday-master.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Masters\HolidayMaster  $holidayMaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(HolidayMaster $holidayMaster)
    {
        //
    }
}
