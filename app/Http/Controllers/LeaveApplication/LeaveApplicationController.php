<?php

namespace App\Http\Controllers\LeaveApplication;

use App\Model\LeaveApplication\LeaveApplication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as Requested;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use App\Model\Masters\EmployeeMaster;
use App\User;
use App\Model\Masters\DepartmentCentreMaster;
use App\Model\Masters\ProjectMaster;
use App\Model\Masters\HolidayMaster;
use App\Jobs\OutdoorDutyMailJob;

class LeaveApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $leave_applications = [];
        // return $request->all();
         if (count((array)$request->all()) == 0) {
            \Session::forget('data');
            $leave_applications = LeaveApplication::orderBy('leave_applications.id', 'asc');

                //Unless the user is super admin, they will see only theirs or if they're managers then the entries of people under them.
                if( !\Auth::user()->hasRole('Super Admin') ){

                $leave_applications =   $leave_applications->leftJoin('employee_masters as emp', function($join){
                                            $join->on('emp.id','=','employee_id');
                                        })
                                        ->where(function($query){
                                            $query->where('emp.id', \Auth::user()->id)
                                                    ->orWhere('emp.manager_employee_id', \Auth::user()->id);
                                        });
                }
        }
        else {
            if(isset($request->search) && count($request->search) > 0)
            {
                \Session::forget('data');
            }
            \Session::push('data', $request->search);
             $leave_applications = LeaveApplication::orderBy('leave_applications.id', 'asc');

                //Unless the user is super admin, they will see only theirs or if they're managers then the entries of people under them.
                if( !\Auth::user()->hasRole('Super Admin') ){

                $leave_applications =   $leave_applications->leftJoin('employee_masters as emp', function($join){
                                            $join->on('emp.id','=','employee_id');
                                        })
                                        ->where(function($query){
                                            $query->where('emp.id', \Auth::user()->id)
                                                    ->orWhere('emp.manager_employee_id', \Auth::user()->id);
                                        });
                }
            if (count((array)\Session::get('data')[0]) > 0) {
                                
                $query = LeaveApplication::orderBy('leave_applications.id', 'asc');

                    //Unless the user is super admin, they will see only theirs or if they're managers then the entries of people under them.
                    if( !\Auth::user()->hasRole('Super Admin') ){

                    $query =    $query->leftJoin('employee_masters as emp', function($join){
                                                $join->on('emp.id','=','employee_id');
                                            })
                                            ->where(function($query){
                                                $query->where('emp.id', \Auth::user()->id)
                                                        ->orWhere('emp.manager_employee_id', \Auth::user()->id);
                                            });
                    }
                
                foreach (\Session::get('data')[0] as $leave_application)
                {
                    
                    if( $leave_application['column'] == 'name' ){
                        $query->where('name', 'LIKE', '%'. $leave_application['search_data'] .'%');
                    }
                    if( $leave_application['column'] == 'department' ){
                        $query->WhereHas('department_centre', function ($query1) use ($leave_application) {
                            $query1->where('name', 'LIKE', '%' . $leave_application['search_data'] . '%');
                        });
                    }
                    if( $leave_application['column'] == 'location_centre' ){
                        $query->WhereHas('location_centre', function ($query1) use ($leave_application) {
                            $query1->where('name', 'LIKE', '%' . $leave_application['search_data'] . '%');
                        });
                    }
                    if( $leave_application['column'] == 'from_date' ){
                        $query->where('from_date', 'LIKE', '%'. date_ymd($leave_application['search_data']) .'%');
                    }
                    if( $leave_application['column'] == 'to_date' ){
                        $query->where('to_date', 'LIKE', '%'. date_ymd($leave_application['search_data']) .'%');
                    }
                    if( $leave_application['column'] == 'mobile_number' ){
                        $query->where('mobile_number', $leave_application['search_data'] );
                    }
                    if( $leave_application['column'] == 'reason_of_leave' ){
                        $query->where('reason_of_leave', 'LIKE', '%'. $leave_application['search_data'] .'%');
                    }


                    if( $leave_application['column'] == 'approval_level_1_status' ){
                        if( strtolower($leave_application['search_data']) == "approved" )
                            $query->where('approval_level_1', 1);
                        elseif( strtolower($leave_application['search_data']) == "denied" )
                            $query->where('approval_level_1', 0);
                        elseif( strtolower($leave_application['search_data']) == "pending" )
                            $query->where('approval_level_1', null);
                    }

                    if( $leave_application['column'] == 'approval_level_2_status' ){
                        if( $leave_application['search_data'] == "approved" )
                            $query->where('approval_level_2', 1);
                        elseif( $leave_application['search_data'] == "denied" )
                            $query->where('approval_level_2', 0);
                        elseif( $leave_application['search_data'] == "pending" )
                            $query->where('approval_level_2', null);
                    }
                    if( $leave_application['column'] == 'status' ){
                        if( $leave_application['search_data'] == "active" )
                            $query->where('active', 1);
                        elseif( $leave_application['search_data'] == "inactive" )
                            $query->where('active', 0);
                    }

                    
                }
                $leave_applications = $query;
                
            }
        }

        $leave_applications = $leave_applications->paginate(10);
        $columns = ['name','department', 'location_centre', 'from_date', 'to_date', 'mobile_number', 'reason_of_leave' ,  'approval_level_1_status','approval_level_2_status', 'status'];
        return view('leave_application.index', compact('leave_applications','columns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = EmployeeMaster::pluck('name','id');
        $current_user = \Auth::user();
        $holidays = HolidayMaster::pluck('date')->toArray();
        return view('leave_application.create', compact('employees', 'current_user','holidays'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();
        //Validation to check if the required fields to store the data are available or not
        $validatedData = $request->validate([ 
            'name' => 'required', 
            'department_centre_id' => 'required',
            'location_centre_id' => 'required', 
            'from_date' => 'required', 
            'to_date' => 'required', 
            'mobile_number' => 'required', 
            'reason_of_leave' => 'required', 
        ]);
        $data['from_date'] = date_ymd($data['from_date']);
        $data['to_date'] = date_ymd($data['to_date']);
        $data['created_by'] = \Auth::user()->id;

        //Validate if From Date is not bigger than To Date
        if( $data['from_date'] > $data['to_date'] ){
                $error = Collection::make( ["From Date Cannot be Greater Than To Date"] );
                return redirect()->back()->with('errors', $error)->withInput( $request->input() );
        }

        

        \DB::beginTransaction();
            $leave_application = new LeaveApplication();
            $leave_application = $leave_application->create($data);

            if( !is_null( $leave_application->employee->manager ) ){
                $email = $leave_application->employee->manager->email_id;
                $title = "Leave Application created for Employee ".$request->name."#".$request->employee_id;

                $body = "Leave Application has been created for Employee:".$request->name." with Emp ID: ".$request->employee_id." from the date of ".$request->from_date." to the date of ".$request->to_date."
                    Reason of Leave: ".$request->reason_of_leave." 
                    Total No. Of Days: ".$request->total_no_of_days."
                    " ;

                $data=[
                    "email"=>$email,
                    "title"=>$title,
                    "body"=>$body
                ];
                dispatch(new OutdoorDutyMailJob($data));
            }
        \DB::commit();

        session()->flash('status',"Leave Application Successfully Created");
        return redirect()->route('leave-application.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\LeaveApplication\LeaveApplication  $leaveApplication
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $leave_application = LeaveApplication::find($id);
        return view('leave_application.show', compact('leave_application'));
    }

    public function approval_process($id, Request $request){
        $leave_application = LeaveApplication::find($id);
        $approval_level = $request->approval_level;

        $data['approval_status'] = $request->approval_status;
        $data['updated_by'] = \Auth::user()->id;

        if( $approval_level == 1 ){

            $data['approved_level_1_feedback'] = $request->approval_feedback;
            $data['approved_level_1_by'] = \Auth::user()->id;


            if( $data['approval_status'] == "reject" && $data['approved_level_1_feedback'] == "" ){
                return ['error' => "Approval Feedback Compulsory When Rejected"];
            }

            if( $data['approval_status'] == "approve" )
                $data['approval_level_1'] = 1;
            else if( $data['approval_status'] == "reject")
                $data['approval_level_1'] = 0;

            if( !is_null( $leave_application->employee->manager ) ){

                $email = $leave_application->employee->manager->email_id;
                $title = "Leave Application created for Employee ".$request->name."#".$request->employee_id;

                $body = "Leave Application has been created for Employee:".$request->name." with Emp ID: ".$request->employee_id." from the date of ".$request->from_date." to the date of ".$request->to_date."
                    Reason of Leave: ".$request->reason_of_leave." 
                    Total No. Of Days: ".$request->total_no_of_days."
                    Approval Level 1 STATUS: ".$data['approval_status']."
                    Approval Level 1 Feedback: ".$data['approved_level_1_feedback']."
                    " ;

                $data=[
                    "email"=>$email,
                    "title"=>$title,
                    "body"=>$body
                ];
                dispatch(new OutdoorDutyMailJob($data));
            }
        }
        else if( $approval_level == 2){
            $data['approved_level_2_feedback'] = $request->approval_feedback;
            $data['approved_level_2_by'] = \Auth::user()->id;


            if( $data['approval_status'] == "reject" && $data['approved_level_2_feedback'] == "" ){
                return ['error' => "Approval Feedback Compulsory When Rejected"];
            }

            if( $data['approval_status'] == "approve" )
                $data['approval_level_2'] = 1;
            else if( $data['approval_status'] == "reject")
                $data['approval_level_2'] = 0;

            if( !is_null( $leave_application->employee->manager ) ){
                $email = $leave_application->employee->manager->email_id;
                $title = "Leave Application created for Employee ".$request->name."#".$request->employee_id;

                $body = "Leave Application has been created for Employee:".$request->name." with Emp ID: ".$request->employee_id." from the date of ".$request->from_date." to the date of ".$request->to_date."
                    Reason of Leave: ".$request->reason_of_leave." 
                    Total No. Of Days: ".$request->total_no_of_days."
                    Approval Level 2 STATUS: ".$data['approval_status']."
                    Approval Level 2 Feedback: ".$data['approved_level_2_feedback']."
                    " ;

                $data=[
                    "email"=>$email,
                    "title"=>$title,
                    "body"=>$body
                ];
                dispatch(new OutdoorDutyMailJob($data));
            }
        }

        $leave_application->update($data);

        session()->flash('status',"Leave Application Successfully Updated");
        return "success";

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\LeaveApplication\LeaveApplication  $leaveApplication
     * @return \Illuminate\Http\Response
     */
    public function edit(LeaveApplication $leaveApplication)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\LeaveApplication\LeaveApplication  $leaveApplication
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LeaveApplication $leaveApplication)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\LeaveApplication\LeaveApplication  $leaveApplication
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeaveApplication $leaveApplication)
    {
        //
    }
}
