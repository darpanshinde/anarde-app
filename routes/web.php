<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Auth::check()) {
        return redirect()->route('home');
    }
    return view('auth.login');
});

Auth::routes();

Route::get('test', function() {
    Storage::disk('google')->put('test.txt', 'Hello World');
});

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
	Route::get('table-list', function () {
		return view('pages.table_list');
	})->name('table');

	Route::get('typography', function () {
		return view('pages.typography');
	})->name('typography');

	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');

	Route::get('map', function () {
		return view('pages.map');
	})->name('map');

	Route::get('notifications', function () {
		return view('pages.notifications');
	})->name('notifications');

	Route::get('rtl-support', function () {
		return view('pages.language');
	})->name('language');

	Route::get('upgrade', function () {
		return view('pages.upgrade');
	})->name('upgrade');

	//Master
    Route::group(['namespace' => 'Masters', 'prefix' => 'master'], function() {
       // Route::get('master/location-centre-master', 'LocationCentreMaster');
        
        Route::resource('location-centre-master', 'LocationCentreMasterController');
        
        Route::resource('department-centre-master', 'DepartmentCentreMasterController');
      	
      	Route::get('get-employee-details/{id}', 'EmployeeMasterController@get_employee_details')->name('getEmployeeDetails');
        Route::resource('employee-master', 'EmployeeMasterController');

        Route::resource('project-master', 'ProjectMasterController');

        Route::resource('beneficiary-master', 'BeneficiaryMasterController');

        Route::resource('holiday-master', 'HolidayMasterController');

        Route::resource('office-hours-master', 'OfficeHoursMasterController');

        Route::resource('roles','RoleMasterController');


    });

    //Attendance
    Route::group(['namespace' => 'Attendance', 'prefix' => 'attendance'], function() {
        
        Route::get('location-details', 'MarkAttendanceController@ip_details')->name('getLocation');
        Route::get('show-attendance', 'MarkAttendanceController@show_attendance_index')->name('showAttendance');
        Route::post('show-attendance', 'MarkAttendanceController@find_attendance')->name('findAttendance');
        Route::resource('mark-attendance', 'MarkAttendanceController');

    });

    //Outdoor Duty
    Route::group(['namespace' => 'OutdoorDuty'], function() {
        
        Route::put('outdoor-duty/email-list/delete/{id?}', 'EmailListController@delete')->name('delete_email');
        Route::resource('outdoor-duty/email-list', 'EmailListController', ['names' => 'email-list']);

        Route::put('outdoor-duty/approval-process/{id}', 'OutdoorDutyController@approval_process');
        Route::resource('outdoor-duty', 'OutdoorDutyController');

    });

    //leave-application
    Route::group(['namespace' => 'LeaveApplication'], function() {
        
        Route::put('leave-application/approval-process/{id}', 'LeaveApplicationController@approval_process');
        Route::resource('leave-application', 'LeaveApplicationController');

    });

    //Report
    Route::group(['namespace' => 'Report', 'prefix' => 'reports'], function() {
        
        Route::get('export-attendance', 'AttendanceReportController@export_attendance')->name('export-attendance');
        Route::get('export-attendance-horizontal', 'AttendanceReportController@export_attendance_horizontal')->name('export-attendance-horizontal');
        Route::get('attendance-report', 'AttendanceReportController@index')->name('attendance-report.index');

    });

        //Content Manager
        Route::group(['namespace' => 'ContentManager', 'prefix' => 'content-manager'], function() {
           // Route::get('master/location-centre-master', 'LocationCentreMaster');
            
            Route::get('get-access-token', 'UploadContentController@get_access_token');
            Route::resource('upload-content', 'UploadContentController');
            
            Route::resource('pending-approval-content', 'PendingApprovalContentController');

            Route::get('approved-content', 'ApprovedContentController@index')->name('approved-content.index');
        });
});

Route::get('get-google-drive-file', function() {
    $filename = 'banner_-_3_82.png';

    $dir = '/';
    $recursive = false; // Get subdirectories also?
    $contents = collect(Storage::disk('google')->listContents($dir, $recursive));

    $file = $contents
        ->where('type', '=', 'file')
        ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
        ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
        ->first(); // there can be duplicate file names!

    dd($file['path']);

    $rawData = Storage::disk('google')->get($file['path']);

    return response($rawData, 200)
        ->header('ContentType', $file['mimetype'])
        ->header('Content-Disposition', "attachment; filename='$filename'");
});


Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

});

Route::get('clock-in-reminder', 'Commands\AttendanceReminderController@clock_in_reminder');
Route::get('clock-out-reminder', 'Commands\AttendanceReminderController@clock_out_reminder');
Route::get('update-attendance-status', 'Commands\UpdateAttendanceStatusController@index');

