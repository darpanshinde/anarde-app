<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="#" class="simple-text logo-normal">
      <img src="{{ URL('/uploads/Login/logo.png') }}" style="width: 80%;">
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>

      <li class="nav-item {{ ($activePage == 'profile' || $activePage == 'user-management') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#MastersDropdown" aria-expanded="true">
          <i class="fas fa-brain"></i>
          <p> Masters
            <b class="caret"></b>
          </p>
        </a>
        @php
          $current_page = "";
          if( $activePage == "location-centre-master" || $activePage == "department-centre-master" || $activePage == "employee-master" || $activePage == "project-master" || $activePage == "beneficiary-master" || $activePage == "holiday-master" || $activePage == "office-hours-master"|| $activePage == "role")
            $current_page = $activePage
          
        @endphp
        <div class="collapse {{ $activePage == $current_page ? 'show' : '' }}" id="MastersDropdown">

          <ul class="nav">
            <li class="nav-item{{ $activePage == 'project-master' ? ' active' : ' inactive' }}">
              <a class="nav-link project_dropdown" data-toggle="collapse" href="#project_master_collapse" aria-expanded="true">
                <span class="sidebar-mini"> PM </span>
                <span class="sidebar-normal"> Project Master 
                  <b class="caret"></b>
                </span>

              </a>
              @php
                $current_page_is_project_master = "";
                  if( $activePage == "project-master" || $activePage == "beneficiary-master" )
                    $current_page_is_project_master = $activePage;
                
              @endphp
              <div class="collapse {{ $activePage == $current_page_is_project_master ? 'show' : '' }}" id="project_master_collapse">
                <ul class="nav">

                  <li class="nav-item{{ $activePage == 'project-master' ? ' active' : ' inactive' }}">
                    <a class="nav-link" href="{{ route('project-master.index') }}">
                      <span class="sidebar-mini"> AP </span>
                      <span class="sidebar-normal"> All Projects </span>
                    </a>
                  </li>

                  <li class="nav-item{{ $activePage == 'beneficiary-master' ? ' active' : ' inactive' }}">
                    <a class="nav-link" href="{{ route('beneficiary-master.index') }}">
                      <span class="sidebar-mini"> BM </span>
                      <span class="sidebar-normal"> Beneficiary Master </span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>


            @if( \Auth::user()->hasRole('Super Admin') )
              <li class="nav-item{{ $activePage == 'location-centre-master' ? ' active' : ' inactive' }}">
                <a class="nav-link" href="{{ route('location-centre-master.index') }}">
                  <span class="sidebar-mini"> LCM </span>
                  <span class="sidebar-normal"> Location Centre Master </span>
                </a>
              </li>
              <li class="nav-item{{ $activePage == 'department-centre-master' ? ' active' : ' inactive' }}">
                <a class="nav-link" href="{{ route('department-centre-master.index') }}">
                  <span class="sidebar-mini"> DCM </span>
                  <span class="sidebar-normal"> Department Centre Master </span>
                </a>
              </li>

              <li class="nav-item{{ $activePage == 'employee-master' ? ' active' : ' inactive' }}">
                <a class="nav-link" href="{{ route('employee-master.index') }}">
                  <span class="sidebar-mini"> EM </span>
                  <span class="sidebar-normal"> Employee Master </span>
                </a>
              </li>


               <li class="nav-item{{ $activePage == 'holiday-master' ? ' active' : ' inactive' }}">
                <a class="nav-link" href="{{ route('holiday-master.index') }}">
                  <span class="sidebar-mini"> HM </span>
                  <span class="sidebar-normal"> Holiday Master </span>
                </a>
              </li>

              <li class="nav-item{{ $activePage == 'office-hours-master' ? ' active' : ' inactive' }}">
                <a class="nav-link" href="{{ route('office-hours-master.index') }}">
                  <span class="sidebar-mini"> OHM </span>
                  <span class="sidebar-normal"> Office Hours Master </span>
                </a>
              </li>
              <li class="nav-item{{ $activePage == 'role' ? ' active' : ' inactive' }}">
                <a class="nav-link" href="{{ route('roles.index') }}">
                  <span class="sidebar-mini"> RM </span>
                  <span class="sidebar-normal"> Roles Master </span>
                </a>
              </li>
            @endif
          </ul>

        </div>
      </li>
      @php
        $current_page = "";
        if( $activePage == "mark-attendance" || $activePage == "show-attendance" )
          $current_page = $activePage
        
      @endphp
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#MarkAttendance" aria-expanded="true">
          <i class="fas fa-clipboard-list"></i>
          <p> Mark Attendance
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse {{ $activePage == $current_page ? 'show' : '' }}" id="MarkAttendance">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'mark-attendance' ? ' active' : ' inactive' }}">
              <a class="nav-link" href="{{ route('mark-attendance.index') }}">
                <span class="sidebar-mini"> MA </span>
                <span class="sidebar-normal"> Mark Attendance </span>
              </a>
            </li>

            <li class="nav-item{{ $activePage == 'show-attendance' ? ' active' : ' inactive' }}">
              <a class="nav-link" href="{{ route('showAttendance') }}">
                <span class="sidebar-mini"> SA </span>
                <span class="sidebar-normal"> Show Attendance </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#OutdoorDuty" aria-expanded="true">
          <i class="fas fa-suitcase-rolling"></i>
          <p> Outdoor Duty
            <b class="caret"></b>
          </p>
        </a>
        @php
          $current_page = "";
          if( $activePage == "outdoor-duty" || $activePage == "email-list" )
            $current_page = $activePage

        @endphp
        <div class="collapse {{ $activePage == $current_page ? 'show' : '' }}" id="OutdoorDuty">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'outdoor-duty' ? ' active' : ' inactive' }}">
              <a class="nav-link" href="{{ route('outdoor-duty.index') }}">
                <span class="sidebar-mini"> OD </span>
                <span class="sidebar-normal"> Outdoor Duty </span>
              </a>
            </li>
            @can('create-outdoor-email-list')
              <li class="nav-item{{ $activePage == 'email-list' ? ' active' : ' inactive' }}">
                <a class="nav-link" href="{{ route('email-list.index') }}">
                  <span class="sidebar-mini"> EL </span>
                  <span class="sidebar-normal"> Email List </span>
                </a>
              </li>
            @endcan
        </div>
      </li>
      <li class="nav-item{{ $activePage == 'leave-application' ? ' active' : ' inactive' }}">
        <a class="nav-link" href="{{ route('leave-application.index') }}">
          <i class="fas fa-sign-out-alt"></i>
          <span class="sidebar-mini"> LA </span>
          <span class="sidebar-normal"> Leave Application </span>
        </a>
      </li>
      @php
        $current_page = "";
        if( $activePage == "attendance-report")
          $current_page = $activePage
        
      @endphp
      
        <li class="nav-item">
          <a class="nav-link" data-toggle="collapse" href="#Reports" aria-expanded="true">
            <i class="fas fa-chart-line"></i>
            <p> Reports
              <b class="caret"></b>
            </p>
          </a>
          <div class="collapse {{ $activePage == $current_page ? 'show' : '' }}" id="Reports">
            <ul class="nav">
              <li class="nav-item{{ $activePage == 'attendance-report' ? ' active' : ' inactive' }}">
                <a class="nav-link" href="{{ route('attendance-report.index') }}">
                  <span class="sidebar-mini"> AR </span>
                  <span class="sidebar-normal"> Attendance Report </span>
                </a>
              </li>
            </ul>
          </div>
        </li>

        @php
          $current_page = "";
          if( $activePage == "upload-content" || $activePage == "pending-approval-content" )
            $current_page = $activePage
          
        @endphp
        <li class="nav-item">
          <a class="nav-link" data-toggle="collapse" href="#ContentManager" aria-expanded="true">
            <i class="fas fa-images"></i>
            <p> Content Manager
              <b class="caret"></b>
            </p>
          </a>
          <div class="collapse {{ $activePage == $current_page ? 'show' : '' }}" id="ContentManager">
            <ul class="nav">
              <li class="nav-item{{ $activePage == 'upload-content' ? ' active' : ' inactive' }}">
                <a class="nav-link" href="{{ route('upload-content.index') }}">
                  <span class="sidebar-mini"> UC </span>
                  <span class="sidebar-normal"> Upload Content </span>
                </a>
              </li>
              <li class="nav-item{{ $activePage == 'pending-approval-content' ? ' active' : ' inactive' }}">
                <a class="nav-link" href="{{ route('pending-approval-content.index') }}">
                  <span class="sidebar-mini"> PAC </span>
                  <span class="sidebar-normal"> Pending Approval Content </span>
                </a>
              </li>
              <li class="nav-item{{ $activePage == 'approved-content' ? ' active' : ' inactive' }}">
                <a class="nav-link" href="{{ route('approved-content.index') }}">
                  <span class="sidebar-mini"> AC </span>
                  <span class="sidebar-normal"> Approved Content </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <li>
            <div class="mobile_show" aria-labelledby="mobile_show navbarDropdownProfile" >
              <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">{{ __('Log out') }}</a>
            </div>
        </li>
  </div>
</div>