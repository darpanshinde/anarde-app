<footer class="footer">
  <div class="container-fluid">
    <nav class="float-left">
      <ul>
        <li>
          <a href="https://www.google.com">
              {{ __('Anarde App') }}
          </a>
        </li>
        <li>
          <a href="https://google.com/presentation">
              {{ __('About Us') }}
          </a>
        </li>
        <li>
          <a href="http://blog.google.com">
              {{ __('Blog') }}
          </a>
        </li>
        <li>
          <a href="https://www.google.com/license">
              {{ __('Licenses') }}
          </a>
        </li>
      </ul>
    </nav>
    <div class="copyright float-right">
      &copy;
      <script>
        document.write(new Date().getFullYear())
      </script>, made with <i class="material-icons">favorite</i> by
      <a href="https://www.google.com" target="_blank">Anarde App</a> 
    </div>
  </div>
</footer>