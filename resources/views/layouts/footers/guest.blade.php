<footer class="footer">
    <div class="container">
        <div class="copyright" style="text-align: center;">
        &copy;
        <script>
            document.write(new Date().getFullYear())
        </script>, made with <i class="material-icons">favorite</i> by
        <a href="https://www.google.com" target="_blank" style="background-color: orange; color: #fff;">Anarde Foundation</a> 
        </div>
    </div>
</footer>