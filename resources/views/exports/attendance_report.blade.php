<table class="table table-striped table-bordered table-hove" id="party_parent">
    <thead>
    <tr>
        <th> Name </th>
        <th>Date</th>
        <th>Day</th>
        <th>Time In</th>
        <th>Time out</th>
        <th>Total Time</th>
        <th>Remark</th>
        <th>Location Centre</th>
        <th>Department</th>
      </tr>
    </thead>
    <tbody>
      @php
        $from_date_fixed = $from_date;
      @endphp

      @foreach($attendances->unique('user_id') as $attendance)
        
        @php
          $from_date = $from_date_fixed ;
        @endphp

        @while($from_date <= $to_date)
          @php
            $flag = 0;  // When attendance doesn't exist 

            if( !is_null($attendances->where('user_id', $attendance->user_id)->where('date', $from_date)->first())  ){
              $attendance = $attendances->where('user_id', $attendance->user_id)->where('date', $from_date)->first();
              $flag = 1;
            }
          @endphp
            <tr>
              <td> {{ $attendance->name }} </td>

              <td> {{date_dmy($from_date)}} </td>
              
              <!-- <td> {{ date('w', strtotime($from_date)) }} </td> -->
              <td> {{getWeekday($from_date)}} </td>
              
              <td>
                @if( $flag == 0 )
                  -
                @else
                  {{ $attendance->clock_in_time }} 
                @endif
              </td>
              
              <td>
                @if( $flag == 0 )
                  -
                @else

                  @if( !is_null($attendance->clock_out_time) )
                    {{ $attendance->clock_out_time }} 
                  @else
                    <p style="color:red; font-weight:500;">-</p>
                  @endif
                @endif
              </td>
              
              <td>
                @if( $flag == 0 )
                  -
                @else
                  @php
                    $total_time = "-";
                    if( !is_null($attendance->clock_out_time) ){
                        $total_time = abs(strtotime($attendance->clock_in_time ) -  strtotime($attendance->clock_out_time)) / 60;
                        $total_time = convertToHoursMins($total_time);
                    }
                    $attendance->total_time = $total_time;

                  @endphp
                  {{ $total_time }}
                @endif
              </td>
              
              <td>
                @if( $flag == 0 )
                  A
                @else
                  @if( !is_null($attendance->attendance_remark_code_val) )
                    {{ $attendance->attendance_remark_code_val }} 
                  @else
                    <p style="color:red; font-weight:500;">-</p>
                  @endif
                @endif
              </td>
              
              <td> 
                  
                  @if( !is_null($attendance->location_centre)  )
                    {{ $attendance->location_centre->name }}
                  @else
                    <p style="color:red; font-weight:500;">-</p> 
                  @endif
              </td>
              
              <td>
                
                @if( !is_null($attendance->department_centre)  )
                  {{ $attendance->department_centre->name }}
                @else
                  <p style="color:red; font-weight:500;">-</p> 
                @endif
              </td>

            </tr>

          @php( $from_date = date('Y-m-d', strtotime( $from_date . " +1 days")) )

        @endwhile
      <tr></tr>
      @endforeach
    </tbody>
</table>
