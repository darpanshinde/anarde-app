@extends('layouts.app', ['activePage' => 'leave-application', 'titlePage' => __('Leave Application')])

@section('title') Create Leave Application @endsection


@section('page-level-css')
    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datepicker-master/dist/datepicker.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-clockpicker/dist/bootstrap-clockpicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet" type="text/css" />

    <style type="text/css">
      td{
        padding: 10px;
      }

      input.form-control.upload-file {
          padding: 2px;
      }
    </style>
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Create Leave Application </h4>
      </div>
      <div class="card-body">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <h6> Errors </h6>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="POST" action="{{ route('leave-application.store') }}">
          @csrf

          @if( \Auth::user()->hasRole('Super Admin') )
            <div class="row">
              <div class="col-md-4">
                <label>Select Employee</label>
                <select class="selectpicker form-control employees_select" name="employees">
                  <option value="">Select Employee</option>
                  @foreach($employees as $key => $employee)
                    <option value="{{ $key }}">{{ $employee }}</option>
                  @endforeach
                </select>
              </div>
            </div>  
          @endif

          <div class="row">
            <div class="col-md-4">
              <input type="hidden" id="employee_id" name="employee_id" value="{{ \Auth::user()->id }}">

              <label> Employee Name</label>
              <input type="text" name="name" id="name" readonly class="form-control" value="{{ old('name', $current_user->name) }}" placeholder="Enter Name of Leave Application" required>
            </div>
            <div class="col-md-4">
              <label>Department Name</label>
              <input type="hidden" name="department_centre_id" id="department_id" value="{{ old('department_centre_id', $current_user->department_centre_id) }}">
              <input type="text" name="departments" id="departments" readonly class="form-control" value="{{ old('department', $current_user->department_centre->name) }}" placeholder="Enter department" readonly>
            </div>

            <div class="col-md-4">
              <label>Location Centre</label>
              <input type="hidden" name="location_centre_id" id="department_id" value="{{ old('location_centre_id', $current_user->location_centre_id) }}">
              <input type="text" name="location_centre" id="location_centre" readonly class="form-control" value="{{ old('location_centre', $current_user->location_centre->name) }}" placeholder="Enter Location Centre" readonly>
            </div>

          </div>

          <div class="row">

            <div class="col-md-3">
              <label>From Date</label>
              <input type="text" name="from_date" id="from_date" class="form-control datepicker" value="{{ old('from_date', '') }}" placeholder="Enter From Date" required>
            </div>

            <div class="col-md-3">
              <label>To Date</label>
              <input type="text" name="to_date" id="to_date" class="form-control datepicker" value="{{ old('to_date', '') }}" placeholder="Enter To Date" required>
            </div>

            <div class="col-md-3">
              <label> Total No. of Days</label>
              <input type="hidden" name="total_days" id="total_no_of_days" value="{{ old('total_no_of_days', '') }}">
              <input type="text" id="total_days" class="form-control" value="{{ old('total_days', '') }}" placeholder="0" disabled>
            </div>
            <div class="col-md-3">
              <label> Mobile Number (Contact No. During leave)</label>
              <input type="text" name="mobile number" id="mobile_number" class="form-control" value="{{ old('mobile_number', '') }}" placeholder="Enter Mobile Number" required>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <label>Reason of Leave</label>
              <textarea class="form-control" name="reason_of_leave" placeholder="Enter the reason of leave" required>{{ old('reason_of_leave', '') }}</textarea>
            </div>
          </div>
        </div>

          <div class="row">
            <div class="col-md-3 offset-4">
              <input type="submit" name="" class="btn" value="Submit" style="width: 100%; background-color: #fa9f19; font-weight: 600; color: #fff;">
            </div>
          </div>
          <hr>

          
        </form>

      </div>
    </div>

  </div>
</div>
<?php
    $js_data = array();
    $js_data['holidays'] = $holidays;
?>
@endsection

@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datepicker-master/dist/datepicker.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-clockpicker/dist/bootstrap-clockpicker.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-clockpicker/dist/jquery-clockpicker.js')}}" type="text/javascript"></script>

@endsection

@section('page-level-scripts-js')

  <script type="text/javascript">
    

    $(document).ready(function(){

        $("#from_date").datepicker({
            format      :   "dd-mm-yyyy",
            viewMode    :   "years", 
        });
        $("#to_date").datepicker({
            format      :   "dd-mm-yyyy",
            viewMode    :   "years", 
        });

        $('select.employees_select').change(function(){
          var employee_id = $(this).val();
          var url = "{{ URL('/master/get-employee-details') }}/"+employee_id;
          $.ajax({
              type: "GET",
              url: url,
              success: function (r) {
                  if( r.success != undefined || r.success != null ){
                    r = r.success;

                    $('#employee_id').val(r.id);
                    $('#name').val(r.name);
                    
                    $('#department_id').val(r.department_centre_id);
                    $('#departments').val(r.department_name);

                    $('#location_centre_id').val(r.location_centre_id);
                    $('#location_centre').val(r.location_centre_name);

                    $('#mobile_number').val(r.mobile_number);
                  }
                  else{
                    alert("Something Went Wrong");
                  }

              }
          });

        });

        $('.datepicker').change(function(){
          var from = $("#from_date").val().split("-");
          var from_date = new Date("20"+from[2], from[1] - 1, from[0]);
          var from_date_for_calculation = new Date("20"+from[2], from[1] - 1, from[0]);
          

          var to = $("#to_date").val().split("-");
          var to_date = new Date("20"+to[2], to[1] - 1, to[0]);

          if( $("#from_date").val() == "" || $("#to_date").val() == "" ){
            return false;
          }
          if( from_date > to_date ){
            alert('From Date cannot be greater than To Date');
            return false;
          }
          
          // var total_days = weekdaysBetween(from_date,to_date);
          // $('#total_days').val(total_days);

          //Calculate number of weekend days between from and to date
          var total_days = 0;
          while( from_date_for_calculation <= to_date){
            
            if( from_date_for_calculation.getDay() != 0 && from_date_for_calculation.getDay() != 6){  //excluding weekend days
              total_days++;
            }

            from_date_for_calculation.setDate(from_date_for_calculation.getDate()+1);
          }

          //Calculate the number of holidays between from and to date.
          var holidays = lang.holidays;

          index_holidays=0;
          number_of_holidays=0;
          while(index_holidays < holidays.length)
             {
                 holiday=new Date(holidays[index_holidays]);
                 if( holiday > from_date && holiday < to_date ){
                    number_of_holidays++;
                    console.log(holiday+" "+from_date+" "+to_date);

                 }
                 index_holidays++;
             }

          total_days = total_days - number_of_holidays;

          $('#total_days').val(total_days);
          $('#total_no_of_days').val(total_days);

        });

    });
  </script>
            
@endsection