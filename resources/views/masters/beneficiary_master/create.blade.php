@extends('layouts.app', ['activePage' => 'beneficiary-master', 'titlePage' => __('Beneficiary Master')])

@section('title') Create Beneficiary @endsection


@section('page-level-css')
    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet" type="text/css" />
    <style type="text/css">
      td{
        padding: 10px;
      }

      input.form-control.upload-file {
          padding: 2px;
      }
    </style>
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Create Beneficiary </h4>
      </div>
      <div class="card-body">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <h6> Errors </h6>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="POST" action="{{ route('beneficiary-master.store') }}" enctype="multipart/form-data">
          @csrf
          <div class="row">
            <div class="col-md-12">
              <label>Project Name :</label>
              <h5> {{ $project->name }} </h5>
              <input type="hidden" name="project_id" value="{{ $project->id }}">

            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-4">
              <label>Beneficiary Name</label>
              <input type="text" name="name" class="form-control" value="{{ old('name', '') }}" placeholder="Enter Name of Beneficiary">
            </div>


            <div class="col-md-4">
              <label>Mobile Number</label>
              <input type="number" maxlength="10" minlength="10" name="mobile_number" class="form-control" value="{{ old('mobile_number', '') }}" placeholder="Enter Mobile Number">
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <label> Address </label>
              <textarea class="form-control" name="address" placeholder="Enter Beneficiary Address">{{ old('address', '') }}</textarea>
            </div>

            <div class="col-md-4" style="padding: 15px;">
              <label>Status</label>
              <table>
                <tr>
                  <td>
                      <input type="radio" name="active" class="" value="1" 
                      @if( old('active', 1) == "1" )
                        checked="checked"
                      @endif
                      > Active
                  </td>
                  <td>
                      <input type="radio" name="active" class="" value="0"
                      @if( old('active', '') == "0" )
                        checked="checked"
                      @endif
                      > Inactive
                  </td>
                </tr>
              </table>
            </div>

            
          </div>

          <div class="row">
            <div class="col-md-6">
                <label>Photo </label>
                <input type="File" class="form-control upload-file" name="photo" placeholder="Enter Reward Link for campaign" accept=" .jpeg, .png, .jpg">
            </div>

            <div class="col-md-6">
                <div class="type2 ">
                    
                    <label style="display: block;"> File Preview: </label>

                        <iframe class="file_preview" src="" width="500" height="300"></iframe>
                        <img class="img_preview" src="" width="500" height="300">
                </div>
            </div>
          </div>

        </div>


          <div class="row">
            <div class="col-md-3 offset-4">
              <input type="submit" name="" class="btn" value="Submit" style="width: 100%; background-color: #fa9f19; font-weight: 600; color: #fff;">
            </div>
          </div>
          
        </form>

      </div>
    </div>

  </div>
</div>
@endsection

@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

@endsection

@section('page-level-scripts-js')
  <script src="{{ URL::asset('template/assets/pages/scripts/components-date-time-pickers.js')}}" type="text/javascript"></script>

  <script type="text/javascript">
    

    $(document).ready(function(){

      $('.img_preview').hide();

        $("#financial_year").datepicker({
            format      :   "yy",
            viewMode    :   "years", 
        });
        
        $('body').delegate('.upload-file', 'change', function () {
            readURL(this);
        })
        
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {

                    var file = input.files[0];
                    var fileType = file["type"];
                        console.log( file);
                    var validImageTypes = ["application/"];
                    if( fileType == "image/jpeg" || fileType == "image/jpg" || fileType == "image/png"){
                        $('.file_preview').hide();
                        $('.img_preview').show();
                        $('.img_preview').attr('src', e.target.result);
                        $('.img_preview').show();
                    }
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    });
  </script>
            
@endsection