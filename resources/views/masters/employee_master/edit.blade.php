@extends('layouts.app', ['activePage' => 'employee-master', 'titlePage' => __('Employee Master')])

@section('title') Edit Employee @endsection


@section('page-level-css')
    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

    <style type="text/css">
      td{
        padding: 10px;
      }

    </style>
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Edit Employee </h4>
      </div>
      <div class="card-body">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <h6> Errors </h6>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="POST" action="{{ route('employee-master.update', $employee->id) }}">
          @csrf
          @method('PATCH')
          <div class="row">
            <div class="col-md-4">
              <label>Employee Name</label>
              <input type="text" name="name" class="form-control" value="{{ old('name', $employee->name) }}" placeholder="Enter Name of Employee">
            </div>

            <div class="col-md-4">
              <label>Phone Number</label>
              <input type="number" maxlength="10" minlength="10" name="mobile_number" class="form-control" value="{{ old('mobile_number', $employee->mobile_number) }}" placeholder="Enter Phone Number of Employee">
            </div>

            <div class="col-md-4">
              <label>Employee UID</label>
              <input type="text" name="employee_uid" class="form-control" value="{{ old('employee_uid', $employee->employee_uid) }}" placeholder="Enter UID of Employee">
            </div>

            
          </div>

          <div class="row">
            <div class="col-md-4">
              <label>Email</label>
              <input type="email" name="email_id" class="form-control" value="{{ old('email_id', $employee->email_id) }}" placeholder="Enter Email of Employee">
            </div>

            <div class="col-md-4">
              <label>Password</label>
              <input type="password" name="password" class="form-control" value="{{ old('password', '') }}" placeholder="Enter Password of Employee">
            </div>
            <div class="col-md-4">
              <label>Manager</label>
              <select class="selectpicker form-control" name="manager_employee_id">
                <option value="">Select Manager</option>
                  @foreach($managers as $key => $manager)
                    <option value="{{$key}}"

                    @if($key == old('manager_employee_id', $employee->manager_employee_id))
                      selected="selected"
                    @endif 

                    > {{$manager}} </option>
                  @endforeach
              </select>
            </div>

            
          </div>

          <div class="row">
            <div class="col-md-4">
              <label>Location Centres</label>
              <select class="selectpicker form-control" name="location_centre_id" required>
                <option value="">Select Location Centre</option>
                @foreach($location_centres as $key => $location_centre)
                  <option value="{{$key}}"

                  @if($key == old('location_centre_id', $employee->location_centre_id))
                    selected="selected"
                  @endif 
                  > {{$location_centre}} </option>
                @endforeach
              </select>
            </div>

            <div class="col-md-4">
              <label>Department Centres</label>
              <select class="selectpicker form-control" name="department_centre_id" required>
                <option value="">Select Department Centre</option>
                @foreach($department_centres as $key => $department_centre)
                  <option value="{{$key}}"

                  @if($key == old('department_centre_id', $employee->department_centre_id))
                    selected="selected"
                  @endif 

                  > {{$department_centre}} </option>
                @endforeach
              </select>
            </div>
            
            <div class="col-md-4">
              <label>Role</label>
              <select class="selectpicker form-control" name="role_id" required>
                <option value="">Select Roles</option>
                @foreach($roles as $key => $role)
                  <option value="{{$key}}"

                  @if($key == old('role_id', $employee->role_id))
                    selected="selected"
                  @endif 

                  > {{$role}} </option>
                @endforeach
              </select>
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-4" >
              <label>Bypass Late Remark</label>  
              <select class="selectpicker form-control" name="bypass_late_remarks" required>
                <option value="0" 

                @if(0 == old('bypass_late_remarks', $employee->bypass_late_remarks))
                  selected="selected"
                @endif 

                >No</option>
                <option value="1" 

                @if(1 == old('bypass_late_remarks', $employee->bypass_late_remarks))
                  selected="selected"
                @endif

                >Yes</option>
              </select>
            </div>

            <div class="col-md-4" style="padding: 15px;">
              <label>Status</label>
              <table>
                <tr>
                  <td>
                      <input type="radio" name="active" class="" value="1" 
                      @if( old('active', $employee->active) == "1" )
                        checked="checked"
                      @endif
                      > Active
                  </td>
                  <td>
                      <input type="radio" name="active" class="" value="0"
                      @if( old('active', $employee->active) == "0" )
                        checked="checked"
                      @endif
                      > Inactive
                  </td>
                </tr>
              </table>
            </div>
          </div>

        </div>


          <div class="row">
            <div class="col-md-3 offset-4">
              <input type="submit" name="" class="btn" value="Submit" style="width: 100%; background-color: #fa9f19; font-weight: 600; color: #fff;">
            </div>
          </div>
          
        </form>

      </div>
    </div>

  </div>
</div>
@endsection

@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"
            type="text/javascript"></script>
@endsection