@extends('layouts.app', ['activePage' => 'holiday-master', 'titlePage' => __('Holiday Master')])

@section('title') Create Holiday @endsection


@section('page-level-css')
    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datepicker-master/dist/datepicker.css')}}" rel="stylesheet" type="text/css" />
    
    <style type="text/css">
      td{
        padding: 10px;
      }

      input.form-control.upload-file {
          padding: 2px;
      }
    </style>
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Create Holiday </h4>
      </div>
      <div class="card-body">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <h6> Errors </h6>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="POST" action="{{ route('holiday-master.store') }}">
          @csrf
          <div class="row">
            <div class="col-md-4">
              <label>Holiday Name</label>
              <input type="text" name="name" class="form-control" value="{{ old('name', '') }}" placeholder="Enter Name of Holiday">
            </div>


            <div class="col-md-4">
              <label>Holiday Date</label>
              <input type="text" name="date" id="date" class="form-control datepicker" value="{{ old('date', '') }}" placeholder="Enter Holiday Date">
            </div>
          </div>

        </div>


          <div class="row">
            <div class="col-md-3 offset-4">
              <input type="submit" name="" class="btn" value="Submit" style="width: 100%; background-color: #fa9f19; font-weight: 600; color: #fff;">
            </div>
          </div>
          
        </form>

      </div>
    </div>

  </div>
</div>
@endsection

@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datepicker-master/dist/datepicker.js')}}" type="text/javascript"></script>
    

@endsection

@section('page-level-scripts-js')
  <script src="{{ URL::asset('template/assets/pages/scripts/components-date-time-pickers.js')}}" type="text/javascript"></script>

  <script type="text/javascript">
    

    $(document).ready(function(){

        $("#date").datepicker({
            format      :   "dd-mm-yy",
            viewMode    :   "years", 
        });
    });
  </script>
            
@endsection