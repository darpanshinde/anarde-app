@extends('layouts.app', ['activePage' => 'project-master', 'titlePage' => __('project Master')])

@section('title')Project Master @endsection

@section('page-level-css')
    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

    <style type="text/css">
      .beneficiary-button {
          text-transform: none;
      }
    </style>
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Project Master </h4>
      </div>
      <div class="card-body">
        <div id="location_centre_Master">
          <div class="table-toolbar">
                      <div class="row">
                        <div class="col-md-12">
                          <label for="" class="caption font-green">Search Projects</label>
                          <lable class="pull-right">
                            <a class="btn fa fa-plus add-fields" href="{{ route('project-master.create') }}"> Create Project</a>
                          </lable>
                          
                        </div>
                      </div>
                        <hr>
                        
                        <form method="get" action="{{route('project-master.index')}}">
                          <div class="row">
                            @if( count((array)\Session::get('data')[0] ) > 0 )
                              @foreach( \Session::get('data')[0] as $input_key => $input)
                                <div class="col-md-12 search_fields">
                                  <div class="row">
                                    <div class=" col-md-4">
                                        <label>Column Name</label>
                                        <select name="search[{{$input_key}}][column]" class="column form-control selectpicker" data-live-search="true" id="column_names">
                                            <option value="">Select Search</option>
                                            @foreach($columns as $key => $column)
                                                <option value="{{$column}}"
                                                @if( $input[ "column" ] == $column )
                                                    selected="selected"
                                                @endif
                                                >{{ucwords(str_replace("_", " ", $column))}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class=" col-md-4">
                                        <label>Search Data</label>
                                        @if(isset($input['search_data']))
                                            @if($input[ "column" ] == "status" && is_array($input['search_data']))
                                                <input type="text" name="search[{{$input_key}}][search_data]" class="form-control search_data" value="{{implode(',',$input['search_data']) }}" placeholder="Enter data">
                                            @else
                                                <input type="text" name="search[{{$input_key}}][search_data]" class="form-control search_data" value="{{ $input['search_data'] }}" placeholder="Enter data">
                                            @endif
                                        @else
                                            <input type="text" name="search[{{$input_key}}][search_data]" class="form-control search_data" placeholder="Enter data" required>
                                        @endif
                                    </div>
                                    @if( $input_key != 0 )
                                        <div class="form-group col-md-4">
                                            <i class="fa fa-trash fa-lg btn  remove-fields "></i>
                                        </div>
                                    @else
                                        <div class="form-group col-md-4">
                                            <i class="fa fa-plus fa-lg btn hide add-fields "></i>
                                        </div>
                                    @endif
                                    
                                  </div>
                                </div>
                              @endforeach
                              <div class="additional_fields">
                                
                              </div>
                            @else
                              <div class="col-md-12 search_fields">
                                <div class="row">
                                  <div class=" col-md-4">
                                      <label>Column Name</label>
                                      <select name="search[0][column]" class="column form-control selectpicker" data-live-search="true" id="column_names">
                                          <option value="">Select Search</option>
                                          @foreach($columns as $key => $column)
                                              <option value="{{$column}}">{{ucwords(str_replace("_", " ", $column))}}</option>
                                          @endforeach
                                      </select>
                                  </div>
                                  <div class=" col-md-4">
                                      <label>Search Data</label>
                                      <input type="text" name="search[0][search_data]" class=" form-control search_data" placeholder="Enter data">
                                  </div>
                                  <div class=" col-md-4">
                                    <div class="icon_container">
                                      <i class="fa fa-plus fa-lg btn add-fields"></i>
                                    </div>
                                  </div>
                                  
                                </div>
                              </div>
                              <div class="additional_fields">
                                
                              </div>
                            @endif

                              <div class="col-md-12">
                                <div class="row">
                                  <div class=" col-md-2">
                                      <button type="submit" class=" btn green find_parent">Find</button>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </form>

                    </div>
        </div>
      </div>
    </div>

    <div class="card">
      <div class="card-body" style="overflow: auto;">

        <table class="table table-striped table-bordered table-hove">
          <tr>
            <th>Action</th>
            <th>Project Name</th>
            <th>Project Description</th>
            <th>State</th>
            <th>Donor</th>
            <th>Location Centre</th>
            <th> Financial Year</th>
            <th> Employees Assigned</th>
            <th> Has Beneficiaries</th>
            <th>Status</th>
            <th> Add Beneficiary </th>
          </tr>

          @foreach($projects as $project)
            <tr>
              <td> 
                <a href="{{ url('master/project-master/'.$project->id.'/edit') }}" class="fa fa-pencil"></a> 
                </td>
              <td>{{ $project->name }}</td>
              <td>{{ $project->project_description }}</td>
              <td>{{ $project->state_id }}</td>
              <td>{{ $project->donor }}</td>
              <td>{{ $project->location_centre->name }}</td>
              <td>{{ $project->financial_year }}</td>
              <td>
                <table>
                  @foreach( $project->employee_mappings as $key => $employees )
                  <tr>
                    <td>
                      {{$employees->employee->name}}
                    </td>
                  </tr>
                  
                  @endforeach
                </table>
                
              </td>
              <td>
                @if( $project->has_beneficiaries == 1 )
                  Yes
                @else
                  No
                @endif
              </td>
              <td>
                @if( $project->active == 1 )
                  Active
                @else
                  Inactive
                @endif
              </td>

              <td>
                @if( $project->has_beneficiaries == 1 )
                  <a href="{{ route('beneficiary-master.create', ['project_id' => $project->id]) }}" class="beneficiary-button btn btn-primary"><i class="fa fa-plus" ></i> Beneficiary</a>
                @endif
              </td>
            </tr>
          @endforeach
        </table>
        {{$projects->render()}}

      </div>
    </div>

  </div>
</div>
@endsection

@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"
            type="text/javascript"></script>
@endsection

@section('page-level-scripts-js')
  @if(Session::has('status'))
      <script>alert('{{Session::get('status')}}');</script>
  @endif

  <script type="text/javascript">
    
    $(document).ready(function(){

      $('.add-fields').click(function(){
        search_fields = $(this).parents('.search_fields');  
        new_row = $(search_fields).clone().eq(0);
        var time = new Date().getTime();

        $(new_row).find('.column').attr('name','search['+time+'][column]');
        $(new_row).find('.column').attr('id','search['+time+'][column]');
        $(new_row).find('.search_data').attr('name','search['+time+'][search_data]');
        $(new_row).find('.fa-plus').removeClass('fa-plus').addClass('fa-trash remove-fields');

        $(new_row).find('select').each(function(){
            var name = $(this).data('name');
            $(this).val('');
            $(this).siblings('div').remove();
            $(this).siblings('button').remove();
        });
        $(new_row).find('.selectpicker').selectpicker('refresh');
        new_row.find('.search_data').val('');

        $('.additional_fields').append(new_row);
        
      })

      $(document).on('click','.remove-fields', function(){
        $(this).parents('.search_fields').remove();
      });

    })

  </script>
@endsection