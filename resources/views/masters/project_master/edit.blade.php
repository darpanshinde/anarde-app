@extends('layouts.app', ['activePage' => 'project-master', 'titlePage' => __('Project Master')])

@section('title') Edit Project @endsection


@section('page-level-css')
    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet" type="text/css" />
    <style type="text/css">
      td{
        padding: 10px;
      }
      .bootstrap-select.btn-group .dropdown-menu li a {
          margin: 5px;
      }

      .bootstrap-select.btn-group .dropdown-menu li.selected a {
          background-color: purple;
          color: #ffffff;
      }

    </style>
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Edit Project </h4>
      </div>
      <div class="card-body">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <h6> Errors </h6>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="POST" action="{{ route('project-master.update', $project->id) }}">
          @csrf
          @method('PATCH')
          <div class="row">
            <div class="col-md-4">
              <label>Project Name</label>
              <input type="text" name="name" class="form-control" value="{{ old('name', $project->name) }}" placeholder="Enter Name of Project" required>
            </div>

            <div class="col-md-4">
              <label>State</label>
              <select class="selectpicker form-control" name="state_id" required>
                <option value="">Select State</option>
                  @foreach($states as $key => $state)
                    <option value="{{$key}}"

                    @if($key == old('state_id', $project->state_id))
                      selected="selected"
                    @endif 

                    > {{$state}} </option>
                  @endforeach
              </select>
            </div>

            <div class="col-md-4">
              <label>Donor</label>
              <input type="text" name="donor" class="form-control" value="{{ old('donor', $project->donor) }}" placeholder="Enter Donor">
            </div>

            
          </div>

          <div class="row">
            <div class="col-md-4">
                <label>Location Centres</label>
                <select class="selectpicker form-control" name="location_centre_id" required>
                  <option value="">Select Location Centre</option>
                  @foreach($location_centres as $key => $location_centre)
                    <option value="{{$key}}"

                    @if($key == old('location_centre_id', $project->location_centre_id) )
                      selected="selected"
                    @endif 
                    > {{$location_centre}} </option>
                  @endforeach
                </select>
            </div>

            <div class="col-md-4">
              <label>Financial Year</label>
              <input type="numeric" name="financial_year" id="financial_year" class="form-control datepicker" value="{{ old('financial_year', $project->financial_year) }}" placeholder="Enter Financial Year" data-provide="datepicker">
            </div>

            <div class="col-md-4" style="padding: 15px;">
              <label>Status</label>
              <table>
                <tr>
                  <td>
                      <input type="radio" name="active" class="" value="1" 
                      @if( old('active', 1) == "1" )
                        checked="checked"
                      @endif
                      > Active
                  </td>
                  <td>
                      <input type="radio" name="active" class="" value="0"
                      @if( old('active', '') == "0" )
                        checked="checked"
                      @endif
                      > Inactive
                  </td>
                </tr>
              </table>
            </div>

            
          </div>

          <div class="row">
            

            <div class="col-md-4">
              <label>Employee List</label>
              <select class="selectpicker form-control employee_select" name="employee_ids[]" multiple="true" required>
                <option value="" disabled>Select Employee</option>
                @foreach($employees as $key => $employee)
                  <option value="{{$key}}"

                  @if($key == old('employee_id') || in_array($key, $selected_employees))
                    selected="selected"
                  @endif 

                  > {{$employee}} </option>
                @endforeach
              </select>
            </div>
            
            <div class="col-md-4" style="padding: 15px;">
              <label>Has Beneficiaries</label>
              <table>
                <tr>
                  <td>
                      <input type="radio" name="has_beneficiaries" class="" value="1" 
                      @if( old('has_beneficiaries', $project->has_beneficiaries) == "1" )
                        checked="checked"
                      @endif
                      > Yes
                  </td>
                  <td>
                      <input type="radio" name="has_beneficiaries" class="" value="0"
                      @if( old('has_beneficiaries', $project->has_beneficiaries) == "0" )
                        checked="checked"
                      @endif
                      > No
                  </td>
                </tr>
              </table>
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-12">
              <label> Project Description</label>
              <textarea class="form-control" name="project_description" placeholder="Enter Project Details">{{ old('project_description', $project->project_description) }}</textarea>
            </div>
          </div>

          <div class="row">
            <div class="col-md-4">
              <h5>Employees Mapped</h5>
              <table class="table table-striped table-bordered table-hove employee_table">
                <th>Employee Name</th>
                <tr class="generated_row">
                  <td>None Selected</td>
                </tr>
              </table>
            </div>
          </div>

        </div>


          <div class="row">
            <div class="col-md-3 offset-4">
              <input type="submit" name="" class="btn" value="Submit" style="width: 100%; background-color: #fa9f19; font-weight: 600; color: #fff;">
            </div>
          </div>
          
        </form>

      </div>
    </div>

  </div>
</div>

<?php
    $js_data = array();
    $js_data['employees'] = $employees;
?>

@endsection

@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

@endsection

@section('page-level-scripts-js')
  <script src="{{ URL::asset('template/assets/pages/scripts/components-date-time-pickers.js')}}" type="text/javascript"></script>

  <script type="text/javascript">
    $(document).ready(function(){



        $("#financial_year").datepicker({
            format      :   "yy",
            viewMode    :   "years", 
        });

        $('select.employee_select').change(function(){
          var employees = $(this).val();

          $('.generated_row').remove();
          $.each(employees, function(key, employee_id){
            employee_name = $('select.employee_select option[value="'+employee_id+'"]').text();
            $('.employee_table').append('<tr class="generated_row"><td>'+employee_name+'</td></tr>');
          })
        });

        $('select.employee_select').trigger('change');
    });
  </script>
            
@endsection