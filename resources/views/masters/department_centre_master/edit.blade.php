@extends('layouts.app', ['activePage' => 'department-centre-master', 'titlePage' => __('Department Centre Master')])

@section('title') Update Department Centre @endsection

@section('page-level-css')
    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

    <style type="text/css">
      td{
        padding: 10px;
      }

    </style>
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Update Department Centre </h4>
      </div>
      <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <h6> Errors </h6>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="POST" action="{{ route('department-centre-master.update', $department_centre->id ) }}">
          @csrf
          @method('PATCH')
          <div class="row">
            <div class="col-md-4">
              <label>Department Centre Name</label>
              <input type="text" name="name" class="form-control" placeholder="Enter Name of Department Centre" value="{{old('name', $department_centre->name)}}" >
            </div>
          </div>

          <div class="row">
            <div class="col-md-4" style="padding: 15px;">
              <label>Status</label>
              <table>
                <tr>
                  <td>
                      <input type="radio" name="active" class="" value="1" 
                      @if( old('active', $department_centre->active) == 1 )
                        checked="checked"
                      @endif
                      > Active
                  </td>
                  <td>
                      <input type="radio" name="active" class="" value="0"
                      @if( old('active', $department_centre->active) == 0 )
                        checked="checked"
                      @endif
                      > Inactive
                  </td>
                </tr>
              </table>
            </div>
          </div>

          <div class="row">
            <div class="col-md-3 offset-4">
              <input type="submit" name="" class="btn" value="Submit" style="width: 100%; background-color: #fa9f19; font-weight: 600; color: #fff;">
            </div>
          </div>
          
        </form>

      </div>
    </div>

  </div>
</div>
@endsection

@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"
            type="text/javascript"></script>
@endsection