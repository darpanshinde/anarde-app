@extends('layouts.app', ['activePage' => 'roles', 'titlePage' => __('Create Roles')])

@section('title') Create Roles @endsection


@section('page-level-css')
    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

    <style type="text/css">
      td{
        padding: 10px;
      }

    </style>
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Create Roles </h4>
      </div>
      <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <h6> Errors </h6>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="POST" action="{{ route('roles.store') }}">
          @csrf
          <div class="row">
            <div class="col-md-4">
              <label>Roles Name</label>
              <input type="text" name="name" class="form-control" placeholder="Enter Name of Roles">
            </div>

          </div>
          <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                      <strong>Permission:</strong>
                      <br/>
                      <table class="table table-striped table-bordered table-hove">

                                              @php
                                                $i = 1;
                                              @endphp
                                              @foreach($permission as $value)

                                                @if( $i % 3 == 1 )
                                                <tr>
                                                @endif
                                                
                                                <td>
                                                  <label>
                                                      <input type="checkbox" name="permission[]" value="{{ $value->id }}"> {{ $value->name }} {{$i}}
                                                  </label>
                                                </td>

                                                @if( $i % 3 == 0 )
                                                </tr>
                                                @endif

                                                @php( $i++ )
                                              @endforeach
                                            </table>
                  </div>
              </div>
          </div>
          <div class="row">
            <div class="col-md-3 offset-4">
              <input type="submit" name="" class="btn" value="Submit" style="width: 100%; background-color: #fa9f19; font-weight: 600; color: #fff;">
            </div>
          </div>
          
        </form>

      </div>
    </div>

  </div>
</div>
@endsection

@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"
            type="text/javascript"></script>
@endsection