@extends('layouts.app', ['activePage' => 'office-hours-master', 'titlePage' => __('Office Hours Master')])

@section('title')Office Hours Master @endsection

@section('page-level-css')
    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

    <style type="text/css">
      .beneficiary-button {
          text-transform: none;
      }
    </style>
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Office Hours Master </h4>
      </div>
      <!-- Create button only visible if there is no record of Office Hours in the table -->
      @if( $count == 0 )
        <div class="card-body">
          <div id="location_centre_Master">
            <div class="table-toolbar">
              <div class="row">
                <div class="col-md-12">
                  <lable class="">
                    <a class="btn fa fa-plus add-fields" href="{{ route('office-hours-master.create') }}"> Create Office Hours</a>
                  </lable>
                  
                </div>
              </div>
                <hr>
            </div>
          </div>
        </div>
      @endif
    </div>

    <div class="card">
      <div class="card-body" style="overflow: auto;">

        <table class="table table-striped table-bordered table-hove">
          <tr>
            <th>Action</th>
            <th>Start of Day (SOD)</th>
            <th>End of Day (EOD) </th>
            <th>Late cutoff Time (mins) </th>
            <th> Half Day Cutoff Time (mins) </th>
          </tr>

          @foreach($office_hours as $office_hour)
            <tr>
              <td> 
                <a href="{{ url('master/office-hours-master/'.$office_hour->id.'/edit') }}" class="fa fa-pencil"></a> 
                </td>
              <td>{{ $office_hour->start_of_day }}</td>
              <td>{{ $office_hour->end_of_day }}</td>
              <td>{{ $office_hour->late_cutoff_time }}</td>
              <td>{{ $office_hour->half_day_cutoff_time }}</td>
            </tr>
          @endforeach
        </table>
      </div>
    </div>

  </div>
</div>
@endsection

@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"
            type="text/javascript"></script>
@endsection

@section('page-level-scripts-js')
  @if(Session::has('status'))
      <script>alert('{{Session::get('status')}}');</script>
  @endif
@endsection