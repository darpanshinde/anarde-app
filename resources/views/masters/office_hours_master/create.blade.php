@extends('layouts.app', ['activePage' => 'office-hours-master', 'titlePage' => __('Office Hours Master')])

@section('title') Create Office Hours @endsection


@section('page-level-css')
    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-clockpicker/dist/bootstrap-clockpicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet" type="text/css" />

    <style type="text/css">
      td{
        padding: 10px;
      }

      input.form-control.upload-file {
          padding: 2px;
      }
    </style>
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Create Office Hours </h4>
      </div>
      <div class="card-body">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <h6> Errors </h6>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="POST" action="{{ route('office-hours-master.store') }}">
          @csrf
          <div class="row">
            <div class="col-md-3">
              <label>Start of Day (SOD)</label>
              <input type="text" name="start_of_day" id="start_of_day" class="form-control" value="{{ old('start_of_day', '') }}" placeholder="Enter Start Of Day">
            </div>

            <div class="col-md-3">
              <label>End of Day (SOD)</label>
              <input type="text" name="end_of_day" id="end_of_day" class="form-control" value="{{ old('end_of_day', '') }}" placeholder="Enter End Of Day">
            </div>

            <div class="col-md-3">
              <label>Late Cutoff Time (mins)</label>
              <input type="text" name="late_cutoff_time" class="form-control" value="{{ old('late_cutoff_time', '') }}" placeholder="Enter Late Cutoff Time">
            </div>

            <div class="col-md-3">
              <label>Half Day Cutoff Time (mins) </label>
              <input type="text" name="half_day_cutoff_time" class="form-control" value="{{ old('half_day_cutoff_time', '') }}" placeholder="Enter Half Day Cutoff Time">
            </div>
        </div>


          <div class="row">
            <div class="col-md-3 offset-4">
              <input type="submit" name="" class="btn" value="Submit" style="width: 100%; background-color: #fa9f19; font-weight: 600; color: #fff;">
            </div>
          </div>
          
        </form>

      </div>
    </div>

  </div>
</div>
@endsection


@section('page-level-scripts-js')
  <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"
          type="text/javascript"></script>

  <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
  <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-clockpicker/dist/bootstrap-clockpicker.js')}}" type="text/javascript"></script>
  <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-clockpicker/dist/jquery-clockpicker.js')}}" type="text/javascript"></script>

  <script type="text/javascript">
    

    $(document).ready(function(){

      $('#start_of_day').clockpicker();
      $('#end_of_day').clockpicker();
    });
  </script>
            
@endsection