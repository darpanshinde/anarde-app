@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'email', 'title' => __('Material Dashboard')])

@section('content')
<div class="container" style="height: auto;">
  <div class="row align-items-center">
    <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
      <form class="form" method="POST" action="{{ route('password.email') }}">
        @csrf

        <div class="card card-login card-hidden mb-3">
          <div class="card-header card-header-primary text-center">
            <h4 class="card-title"><strong>{{ __('Forgot Password') }}</strong></h4>
          </div>
          <div class="card-body">
            @if (session('status'))
              <div class="row">
                <div class="col-sm-12">
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span>{{ session('status') }}</span>
                  </div>
                </div>
              </div>
            @endif
            <div class="bmd-form-group{{ $errors->has('mobile_number') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend" style="width: 100%;">
                  <span class="input-group-text">
                    <p>Mobile Number</p>
                  </span>
                </div>
                <input type="number" name="mobile_number" class="form-control" placeholder="{{ __('Mobile Number...') }}" value="{{ old('mobile_number') }}" required>
              </div>
              @if ($errors->has('mobile_number'))
                <div id="email-error" class="error text-danger pl-3" for="mobile_number" style="display: block;">
                  <strong>{{ $errors->first('mobile_number') }}</strong>
                </div>
              @endif
            </div>
          </div>
          <div class="card-footer justify-content-center">
            <button type="submit" class="btn btn-primary btn-link btn-lg">{{ __('Send Password Reset Link') }}</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
