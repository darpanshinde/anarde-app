@extends('layouts.app', ['activePage' => 'pending-approval-content', 'titlePage' => __('Pending Approval Content')])

@section('title') Pending Approval Content @endsection


@section('page-level-css')
    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datepicker-master/dist/datepicker.css')}}" rel="stylesheet" type="text/css" />
    <style type="text/css">
      input.content_type_radio {
          margin: 0 5px;
      }
      img.img_preview {
          object-fit: contain;
      }
      p.status_text {
          margin: 4% 0;
          text-align: center;
          color: #fff;
          font-weight: 600;
          border: 1px solid;
          padding: 4%;
          border-radius: 5px;
      }
      p.status_text.approve {
          color: #fff;
          background-color: #469e46;
      }
      p.status_text.disapprove {
          color: #fff;
          background-color: #d00000;
      }

    </style>
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Project : {{ $project->name }} </h4>
      </div>
      <div class="card-body">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <h6> Errors </h6>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="POST" action="{{ route('pending-approval-content.update', $project->id) }}" enctype="multipart/form-data">
          @csrf
          @method('PUT')
          <input type="hidden" name="project_id" value="{{  $project->id }}">

          <div class="row content_row">
            @foreach( $content_files as $key => $files )
              <div class="col-md-4 single_file_column">
                      
                  <label style="display: block;"> File Preview: </label>

                  @php
                    $file = $files->file_path."/".$files->file_name;
                    $real_file = "https://drive.google.com/uc?id=".$files->file_path;

                    \Log::info($real_file);
                  @endphp

                  @if( $files->content_type == "document" )
                    <iframe class="file_preview" src="{{ URL($file)  }}" width="500" height="300"></iframe>
                  @elseif( $files->content_type == "photo" )
                    <img class="img_preview" src="{{ URL($real_file) }}" width="500" height="300">
                  @endif
                  <div class="row">
                    <div class="col-md-4">
                      <label>Replace content</label>

                      @php
                        $accept_content = "";
                        if( $files->content_type == "photo" )
                          $accept_content = ".jpeg, .jpg, .png";
                        elseif($files->content_type == "document" )
                          $accept_content = ".pdf, .docx";

                      @endphp

                      <input type="file" class="upload-file" name="content_file[{{$files->id}}][content]" accept="{{ $accept_content }}">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <textarea class="form-control caption" name="content_file[{{$files->id}}][file_caption]" placeholder="Enter Caption">{{ $files->file_caption }}</textarea>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <input type="text" class="form-control date" id="date" name="content_file[{{$files->id}}][date]" placeholder="Enter Date" value="{{ date_dmy($files->date) }}">
                    </div>
                  </div>
                  <div class="row approval_row">
                      <input type="hidden" class="approval_status" name="content_file[{{$files->id}}][approval_status]" value="">
                    <div class="col-md-4 approval-column">
                      <a href="javascript:void(0)" class="btn status_button approve_button" style="width: 100%;"><i class="fa fa-check" style="color: green;"></i></a>
                    </div>
                    <div class="col-md-4 disapproval-column">
                      <a href="javascript:void(0)" class="btn status_button disapprove_button" style="width: 100%;"><i class="fa fa-trash" style="color: red;"></i></a>
                    </div>
                  </div>
              </div>
            @endforeach
            <div class="col-md-12">
              {{ $content_files->render() }}
            </div>
          </div>

        </div>


          @can('review-content')
            <div class="row">
              <div class="col-md-3 offset-4">
                <input type="submit" name="" class="btn" value="Submit" style="width: 100%; background-color: #fa9f19; font-weight: 600; color: #fff;">
              </div>
            </div>
          @endcan
          
        </form>

      </div>
    </div>

  </div>
</div>
@endsection

@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datepicker-master/dist/datepicker.js')}}" type="text/javascript"></script>
    

@endsection

@section('page-level-scripts-js')

  <script type="text/javascript">
    

    $(document).ready(function(){


        $(".date").datepicker({
            format      :   "dd-mm-yyyy",
            viewMode    :   "years", 
        });
        
        fadeOuttime = 1000;
        $('.approve_button').click(function(){
          $( $(this).parents('.approval_row').find('.approval_status') ).val(1);


          $(this).fadeOut(fadeOuttime, () => { 
              $(this).parents('.approval-column').append('<p class="status_text approve">Approved</p>').fadeIn(2000);
          });
          $(this).parents('.approval_row').find('.disapprove_button').fadeOut(fadeOuttime);
          
        })
        $('.disapprove_button').click(function(){
          $( $(this).parents('.approval_row').find('.approval_status') ).val(0);

          $this = this;
          $(this).fadeOut(fadeOuttime, () => { 
              $($this).parents('.approval_row').find('.approval-column').append('<p class="status_text disapprove">Disapproved</p>').fadeIn(2000);
          });

          $(this).parents('.approval_row').find('.approve_button').fadeOut(fadeOuttime);

        })

        $('.status_button').click(function(){
          
          $this = this;
          $this = this;
          setTimeout(function () {
              $($this).parents('.approval_row').find('.disapproval-column').append('<a href="javascript:void(0)" class="btn cancel_button" style="width: 100%;">Cancel</a>');
            }, 1000);
        })

        $(document).on('click', '.cancel_button', function(){
          $(this).fadeOut(fadeOuttime);
          $('.status_text').fadeOut(fadeOuttime);

          $this = this;
          setTimeout(function () {
              $($this).parents('.approval_row').find('.approve_button').fadeIn(fadeOuttime);
              $($this).parents('.approval_row').find('.disapprove_button').fadeIn(fadeOuttime);
            }, fadeOuttime);

          $( $(this).parents('.approval_row').find('.approval_status') ).val("");

        })

        $(document).on('change','.upload-file', function(){
            readURL(this);
        })
        
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {

                    var file = input.files[0];
                    var fileType = file["type"];
                    var validImageTypes = ["application/"];
                    if( fileType == "application/pdf"){
                      $(input).parents('.single_file_column').find('.img_preview').hide();
                      $('.excel_preview').hide();
                      $(input).parents('.single_file_column').find('.file_preview').show();
                      $(input).parents('.single_file_column').find('.file_preview').attr('src', e.target.result);
                  }
                  else if( fileType == "image/jpeg" || fileType == "image/jpg" || fileType == "image/png"){
                      $(input).parents('.single_file_column').find('.file_preview').hide();
                      $(input).parents('.single_file_column').find('.img_preview').show();
                      $(input).parents('.single_file_column').find('.img_preview').attr('src', e.target.result);
                      $(input).parents('.single_file_column').find('.img_preview').show();
                        console.log( $(input).parents('.single_file_column').find('.img_preview'));
                  }
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    });
  </script>
            
@endsection