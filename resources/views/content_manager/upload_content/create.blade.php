@extends('layouts.app', ['activePage' => 'upload-content', 'titlePage' => __('Upload Content')])

@section('title') Upload Content @endsection


@section('page-level-css')
    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datepicker-master/dist/datepicker.css')}}" rel="stylesheet" type="text/css" />
    <style type="text/css">
      input.content_type_radio {
          margin: 0 5px;
      }
      img.img_preview {
          object-fit: contain;
      }
      label.content_period_label {
          padding: 15px;
          border: 1px outset;
      }
    </style>
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Project : {{ $project->name }} </h4>
      </div>
      <div class="card-body">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <h6> Errors </h6>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="POST" action="{{ route('upload-content.store') }}" enctype="multipart/form-data">
          @csrf

          <input type="hidden" name="project_id" value="{{  $project->id }}">

          <div class="row">
            <div class="col-md-4">
              <label>Content Type :</label>
              <select class="selectpicker content_type_select form-control" name="content_type" required>
                <option value="">Select Content Type</option>
                <option value="photo">Photo</option>
                <option value="document">Document</option>
              </select>              
            </div>
            <div class="col-md-12">
              <label>Photo Period</label>
              <div class="form-group">
                <label class="content_period_label">
                  <input type="radio" class="content_period_radio" name="content_period" required value="before">
                  Before
                </label>
                <label class="content_period_label">
                  <input type="radio" class="content_period_radio" name="content_period" required value="after">
                  After
                </label>

                <label class="content_period_label">
                  <input type="radio" class="content_period_radio" name="content_period" required value="work_in_progress">Work In 
                  Progress
                </label>

                <label class="content_period_label">
                  <input type="radio" class="content_period_radio" name="content_period" required value="other">
                  Other
                </label>
              </div>
            </div>

            <div class="col-md-4">
              <label>Date</label>
              <input type="text" name="date" id="date" class="datepicker form-control" placeholder="Enter Date" required>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
                <label>Photo </label>
                <input type="file" class="upload-file form-control" name="content[]" accept=" .jpeg, .png, .jpg" multiple="multiple" disabled required>
            </div>
          </div>
          <div class="row content_row">
            
          </div>

        </div>


          <div class="row">
            <div class="col-md-3 offset-4">
              <input type="submit" name="" class="btn" value="Submit" style="width: 100%; background-color: #fa9f19; font-weight: 600; color: #fff;">
            </div>
          </div>
          
        </form>

      </div>
    </div>

  </div>
</div>
@endsection

@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datepicker-master/dist/datepicker.js')}}" type="text/javascript"></script>
    

@endsection

@section('page-level-scripts-js')

  <script type="text/javascript">
    

    $(document).ready(function(){

      $('.img_preview').hide();

        $("#date").datepicker({
            format      :   "dd-mm-yyyy",
            viewMode    :   "years", 
        });
        
        $(document).on('change','.upload-file', function(){
          console.log(1);

          var total_files = this.files.length;
          
          if( total_files > 0 ){

            existing_content_count = $('.content_row').children().length;

            for (var index = 0; index < total_files; index++) {

              if( index > existing_content_count )
                existing_content_count = index;

              var $div_container = $("<div>", {id: "photo_"+existing_content_count, "class": "col-md-4"});
              var $label = $('<label>').text('File Preview:');
              var $img = $('<img>', {width: "500", height: 500, class: "img_preview"});
              var $iframe = $('<iframe>', {width: "500", height: 500, class: "file_preview"});

              var $label_caption = $('<label>').text('Caption:');
              var $textarea = $('<textarea>', {placeholder: "Enter Photo Caption", name: "content_caption[]" ,  class: "content_caption form-control"});

              $($div_container).append($($label));
              $($div_container).append($($iframe));
              $($div_container).append($($img));

              $($div_container).append($($label_caption));
              $($div_container).append($($textarea));


              $('.content_row').append($div_container);

              readURL(this.files[index], existing_content_count);

            }
            if( $(this).val != "" ){

              $(this).addClass('old_input');
              $(this).hide();
              if( $('select.content_type_select').val() == "photo" ){
                $(this).parent().append('<input type="file" class="upload-file form-control" name="content[]" accept=" .jpeg, .png, .jpg" multiple="multiple">');
              }
              else if( $('select.content_type_select').val() == "document" ){
                $(this).parent().append('<input type="file" class="upload-file form-control" name="content[]" accept=".docx, .pdf" multiple="multiple">');
              }
            }
          }

          


        })
        
        function readURL(input, index) {
          console.log(index);
          if (input) {
              var reader = new FileReader();

              reader.onload = function(e) {
                  var file = input;
                  var fileType = file["type"];
                  var validImageTypes = ["application/"];
                  if( fileType == "application/pdf"){
                      $( '#photo_'+index+" .img_preview" ).hide();
                      $('.excel_preview').hide();
                      $('#photo_'+index+" .file_preview").show();
                      $('#photo_'+index+" .file_preview").attr('src', e.target.result);
                  }
                  else if( fileType == "image/jpeg" || fileType == "image/jpg" || fileType == "image/png"){
                      $('#photo_'+index+" .file_preview").hide();
                      $( '#photo_'+index+" .img_preview" ).show();
                      $( '#photo_'+index+" .img_preview" ).attr('src', e.target.result);
                      $( '#photo_'+index+" .img_preview" ).show();
                  }
              }
              reader.readAsDataURL(input);
          }
          

            
        }

        $('select.content_type_select').change(function(){
          var content_type = $(this).val();
          if( content_type != "" ){

            $('.content_row').html('');
            $('.old_input').remove();

            $('.content_period_radio').attr('required',true);
            $('.content_period_radio').attr('disabled',false);
            $('.upload-file').attr('disabled', false);

            if( content_type == "photo"){
              $('.upload-file').attr('accept', '.jpeg, .jpg, .png');
            }
            else if( content_type == "document" ){

              $('.upload-file').attr('accept', '.docx, .pdf');
              $('.content_period_radio').attr('disabled',true);
              $('.content_period_radio').attr('required',false);
            }
          }
          else{
            $('.upload-file').prop('disabled', true);
          }
        })
    });
  </script>
            
@endsection