@extends('layouts.app', ['activePage' => 'show-attendance', 'titlePage' => __('Show Attendance')])

@section('title')Show Attendance @endsection

@section('page-level-css')
    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datepicker-master/dist/datepicker.css')}}" rel="stylesheet" type="text/css" />
<script src="https://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
    
    <style type="text/css">
      #map-canvas {
        height: 400px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
      }
      .details_table{
        overflow: auto;
      }
    </style>
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Show Attendance </h4>
      </div>

      <div class="card-body">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <h6> Errors </h6>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="POST" action="{{ route('findAttendance') }}">
          @csrf
          <div class="row">
            @if( \Auth::user()->hasRole('Super Admin') )
              <div class="col-md-4">
                <label> Select Employee</label>
                <select class="selectpicker form-control employee_select" name="employee">
                  <option value="">Select Employee</option>
                  @foreach($employees as $key => $employee)
                    <option value="{{ $key }}"> {{ $employee }} </option>
                    }
                  @endforeach

                </select>
              </div>
            @endif

            <div class="col-md-4">
              <label>Date</label>
              <input type="text" id="date" name="date" class="datepicker form-control" required>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 offset-md-3">
              <button type="submit" class="btn">Submit</button>
            </div>
          </div>
        </form>
      </div>

    </div>

    <div class="card">
      <div class="card-body details_table">

        <table class="table table-striped table-bordered table-hove">
          <tr>
            <th>Date</th>
            <th>Day</th>
            <th>Time In</th>
            <th>Time out</th>
            <th>Total Time</th>
            <th>Status</th>
            <th>Remark</th>
            <th>Location Centre</th>
            <th>Department</th>
          </tr>

          @if( $clock_in_details != null )
            <tr>
              
              <td> {{date_dmy($clock_in_details->date)}} </td>
              
              <td> {{ $clock_in_details->weekday }} </td>
              
              <td> {{ $clock_in_details->clock_in_time }} </td>
              
              <td> 
                @if( !is_null($clock_out_details) )
                  {{ $clock_out_details->clock_in_time }} 
                @else
                  <p style="color:red; font-weight:500;">Not Available</p>
                @endif
              </td>
              
              <td>
                @if( !is_null($clock_in_details->total_time) )
                  {{  $clock_in_details->total_time }} 
                @else
                  <p style="color:red; font-weight:500;">Not Available</p>
                @endif
              </td>
              
              <td>
                @if( !is_null($clock_in_details->status) )
                  {{ $clock_in_details->status }} 
                @else
                  <p style="color:red; font-weight:500;">Not Available</p>
                @endif
              </td>
              
              <td> 
                @if( !is_null($clock_in_details->attendance_remark_code_val) )
                  {{ $clock_in_details->attendance_remark_code_val }} 
                @else
                  <p style="color:red; font-weight:500;">Not Available</p>
                @endif
              </td>
              
              <td> 
                  @if( !is_null( $clock_in_details->employee ) )
                    @if( !is_null($clock_in_details->employee->location_centre)  )
                      {{ $clock_in_details->employee->location_centre->name }}
                    @else
                      <p style="color:red; font-weight:500;">Not Available</p> 
                    @endif
                  @else
                    <p style="color:red; font-weight:500;">Not Available</p>
                  @endif
              </td>
              
              <td>
                @if( !is_null( $clock_in_details->employee ) )
                  @if( !is_null($clock_in_details->employee->department_centre)  )
                    {{ $clock_in_details->employee->department_centre->name }}
                  @else
                    <p style="color:red; font-weight:500;">Not Available</p> 
                  @endif
                @else
                  <p style="color:red; font-weight:500;">Not Available</p>
                @endif
              </td>

            </tr>
          @endif
          
        </table>

        @if( $clock_in_details != null )
          <div class="row">
            <div class="col-md-6">
              <div id="map-canvas"></div>
            </div>
          </div>
        @endif
      </div>

    </div>

  </div>
</div>
@endsection

@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datepicker-master/dist/datepicker.js')}}" type="text/javascript"></script>
@endsection

@section('page-level-scripts-js')
  @if(Session::has('status'))
      <script>alert('{{Session::get('status')}}');</script>
  @endif

  <script type="text/javascript">
    

    $(document).ready(function(){

        $("#date").datepicker({
            format      :   "dd-mm-yyyy",
            viewMode    :   "years", 
        });

        @if( $clock_in_details != null )
          init_map();

          //Initialize the map with the location of Anarde Headquarters(HQ)
          function init_map(){

            var mapOptions = {

                @if( !is_null($clock_out_details) )
                  center: new google.maps.LatLng({{$clock_in_details->clock_in_latitude}},{{$clock_out_details->clock_in_longitude}} ), //specify the location of map's center
                @else
                  center: new google.maps.LatLng({{$clock_in_details->clock_in_latitude}},{{$clock_in_details->clock_in_longitude}} ), //specify the location of map's center

                @endif
                zoom: 15
            };

            var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions); //embed map to the div with specified ID
            HQ_position = new google.maps.LatLng(19.037833635324812,72.84596283068821 ); //Anarde Headquarter Position

            var clock_in_marker = new google.maps.Marker({
              @if( !is_null($clock_out_details) )
                position: new google.maps.LatLng({{$clock_in_details->clock_in_latitude}}, {{$clock_out_details->clock_in_longitude}} ),
              @else
                position: new google.maps.LatLng({{$clock_in_details->clock_in_latitude}}, {{$clock_in_details->clock_in_longitude}} ),
              @endif
                title: 'Clock In Marker',
                map: map
            });

            @if( !is_null($clock_out_details) )
              var clock_out_marker = new google.maps.Marker({
                  position: new google.maps.LatLng({{$clock_out_details->clock_in_latitude}},{{$clock_out_details->clock_in_longitude}} ),
                  title: 'Clock out Marker',
                  map: map
              });
            @endif
            
            var markers = [clock_in_marker, clock_out_marker];
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0; i < 2; i++) {
             bounds.extend(markers[i].position);
            }

            map.fitBounds(bounds);

            
          }
        @endif



    });
  </script>

@endsection