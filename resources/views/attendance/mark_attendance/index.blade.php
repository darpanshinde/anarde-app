@extends('layouts.app', ['activePage' => 'mark-attendance', 'titlePage' => __('Mark Attendance')])

@section('title')Mark Attendance @endsection

@section('page-level-css')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0IRF26FT810wv2xVe-vVwsOqayPGPQ5A" type="text/javascript"></script>
<script src="/assets/gmap3.js?body=1" type="text/javascript"></script>

    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <style type="text/css">
      .beneficiary-button {
          text-transform: none;
      }
      #map-canvas {
        height: 400px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
      }

      @media screen and (max-width: 420px){
        .camera_table td {
            width: 100%;
            display: block;
        }
      }
    </style>

@endsection

@section('content')
<div id="overlay">
  <img src="{{ URL('/uploads/loader.gif') }}">
</div>
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Mark Attendance </h4>
      </div>
      <!-- Create button only visible if there is no record of Office Hours in the table -->
        <div class="card-body">
          <div id="location_centre_Master">
            <div class="table-toolbar">
              <div class="row">
                <div class="col-md-12">
                  <lable class="">
                    <a class="btn fa fa-plus add-fields get_location" href="javascript:void(0)"> Get Location</a>
                  </lable>
                  
                </div>
              </div>
              <hr>

              @csrf
              <div class="row">
                <div class="col-md-6">
                  <div id="map-canvas"></div>
                  <input type="hidden" name="" class="current_location" data-latitude data-longitude>             
                </div>

                <div class="col-md-6">
                  
                  <div class="row">
                    <div class="col-md-6">
                      <div id="webcam"></div>
                    </div>

                    <div class="col-md-6">
                      <img id="imgCapture" src="{{ URL('/uploads/no_image_placeholder.png') }}" width="320" height="240" />
                      <input type="hidden" class="image_capture" data-captured>
                    </div>

                    <div class="col-md-12">
                      <input type="button" class="btn" id="btnCapture" value="Capture"/>
                    </div>
                  </div>
                  
                </div>
              </div>

              <div class="row">
                <div class="col-md-3 offset-4">
                    <button class="btn submit" data-attendance-details="clock_in">Clock In</button>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>

  </div>
</div>
@endsection

@section('page-level-plugins-js')
    

    <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
@endsection

@section('page-level-scripts-js')
  @if(Session::has('status'))
      <script>alert('{{Session::get('status')}}');</script>
  @endif

<script type="text/javascript">

  $(document).ready(function() {

      $('#overlay').hide();

      init_map();

      //Initialize the map with the location of Anarde Headquarters(HQ)
      function init_map(){

        var mapOptions = {
            center: new google.maps.LatLng(19.037833635324812,72.84596283068821 ), //specify the location of map's center
            zoom: 15
        };

        var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions); //embed map to the div with specified ID
        HQ_position = new google.maps.LatLng(19.037833635324812,72.84596283068821 ); //Anarde Headquarter Position

      }

    });

  $(document).ready( function(){

     
    //Get User's current location on click of the button.
    $('.get_location').on('click', function(){
        
        if ("geolocation" in navigator){ //check geolocation available 
            
            function error(err) {
              console.warn(`ERROR(${err.code}): ${err.message}`);
            };

            var options = {
              enableHighAccuracy: true,
            };

            //try to get user current location using getCurrentPosition() method
            navigator.geolocation.getCurrentPosition(function(position){ 
                console.log("Found your location <br />Lat : "+position.coords.latitude+" </br>Lang :"+ position.coords.longitude);
                
                clientPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                var mapOptions = {
                    center: new google.maps.LatLng(position.coords.latitude,position.coords.longitude),
                    zoom: 15
                };
                var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

                //Set the current location in the input object
                $('.current_location').attr('data-latitude', position.coords.latitude);
                $('.current_location').attr('data-longitude', position.coords.longitude);

                var marker = new google.maps.Marker({
                    position: clientPosition,
                    title: 'new marker',
                    draggable: true,
                    map: map
                });

                var infowindow = new google.maps.InfoWindow({
                    content: '<div id="infodiv2">infowindow!</div>'
                });
                //map.setZoom(15);
                map.setCenter(marker.getPosition())
                //infowindow.open(map, marker)

              }, error, options);
          }else{
            console.log("Browser doesn't support geolocation!");
          }
    });

    $('.submit').on('click', function(e){
      $('#overlay').show();
      e.preventDefault();

      //Validation to check if the current location of the user is available or not
      var current_latitude = $('.current_location').attr('data-latitude');
      var current_longitude = $('.current_location').attr('data-longitude');
      if( current_latitude == "" && current_longitude == "" ){
        alert('Please provide your location info');
              $('#overlay').hide();
        return false;
      }
      if( $('.image_capture').attr('data-captured') != "captured" ){
        alert('Please provide your photo');
              $('#overlay').hide();
        return false;
      }

      attendance_details = $(this).attr('data-attendance-details');

      blob = makeblob($("#imgCapture")[0].src);
      var formData = new FormData();
      formData.append('image', blob);
      formData.append('current_latitude', current_latitude);
      formData.append('current_longitude', current_longitude);
      formData.append('attendance_details', attendance_details);

      if( attendance_details == "clock_out" ){
        if( !confirm("Are you sure you want to clock out?") ){
          return false;
        }

      }

      console.log(formData);
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

      $.ajax({
          url: "{{ route('mark-attendance.store') }}",
          method: "POST",
          data: formData,
          processData: false,
          contentType: false,
          success: function (result) {
              $('#overlay').hide();
              if( result == "clocked_in" ){
                alert('Attendance already clocked in for the day');
              }
              if( result == "clocked_out"){
                alert('Attendance already clocked out for you');
              }
              if( result == "success" ){
                alert('Attendance Marked Successfully');
                location.reload();
              }
          }
      });
    });

    makeblob = function (dataURL) {
      var BASE64_MARKER = ';base64,';
      if (dataURL.indexOf(BASE64_MARKER) == -1) {
          var parts = dataURL.split(',');
          var contentType = parts[0].split(':')[1];
          var raw = decodeURIComponent(parts[1]);
          return new Blob([raw], { type: contentType });
      }
      var parts = dataURL.split(BASE64_MARKER);
      var contentType = parts[0].split(':')[1];
      var raw = window.atob(parts[1]);
      var rawLength = raw.length;

      var uInt8Array = new Uint8Array(rawLength);

      for (var i = 0; i < rawLength; ++i) {
          uInt8Array[i] = raw.charCodeAt(i);
      }

      return new Blob([uInt8Array], { type: contentType });
  }
  });
  
</script>
<script src="{{ URL::asset('template/assets/global/plugins/plugins/jquery-webcam/WebCam.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        Webcam.set({
            width: 320,
            height: 240,
            image_format: 'jpeg',
            jpeg_quality: 90
        });
        Webcam.attach('#webcam');
        $("#btnCapture").click(function () {
            Webcam.snap(function (data_uri) {
                $("#imgCapture")[0].src = data_uri;
                $('.image_capture').attr('data-captured', "captured");
            });
        });
        $("#btnUpload").click(function () {
            $.ajax({
                type: "POST",
                url: "CS.aspx/SaveCapturedImage",
                data: "{data: '" + $("#imgCapture")[0].src + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) { }
            });
        });
    });
</script>
@endsection