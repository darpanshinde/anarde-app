@extends('layouts.app', ['activePage' => 'email-list', 'titlePage' => __('Email List')])

@section('title')Email List @endsection

@section('page-level-css')
    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Email List </h4>
      </div>
      <div class="card-body">
        <div id="location_centre_Master">
          <div class="table-toolbar">
            <div class="row">
              <div class="col-md-12">
                <label for="" class="caption font-green">Search Email Lists</label>
              </div>
            </div>
              <hr>

              <form method="POST" action="{{route('email-list.store')}}">
                  @csrf
                  <div class="row">
                    <div class="col-md-12 search-fields">

                      <div class="row email_list_row">
                        <div class="col-md-4">
                          <label class="email_label">Add To Email List</label>
                          <input type="email" name="email[]" class="form-control" placeholder="Enter Email" required>
                        </div>
                        <div class=" col-md-4">
                          <div class="icon_container">
                            <i class="fa fa-plus add_field fa-lg btn "></i>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3 offset-md-4">
                      <button class="btn submit" >SUBMIT</button>
                    </div>
                  </div>
              </form>

          </div>
        </div>
      </div>
    </div>

    <div class="card">
      <div class="card-body">
        <form method="GET" action="{{route('email-list.index')}}">
          <div class="row">
            <div class="col-md-3 offset-md-4">
              <label>Search Email</label>
              <input type="email" class="form-control search_field" name="search" placeholder="Enter Email" required>
            </div>
            <div class="col-md-3 offset-md-4">
              <input type="submit" class="btn" style="width: 100%;">
            </div>
          </div>
        </form>
        <form method="POST" action="{{ route('delete_email') }}">
          @csrf
          @method('PUT')
          <div class="row">
            
            <div class="col-md-4 pull-right">
              <input type="submit" class="btn" value="BULK DELETE">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              
              <table class="table table-striped table-bordered table-hove">
                <tr>
                  <th>
                    <input type="checkbox" class="bulk_select">
                    Select
                  </th>
                  <th> Email </th>
                  <th> Created By</th>
                  <th> Delete </th>
                </tr>

                @foreach($email_list as $email)
                  <tr>
                    <td>
                      <input type="checkbox" class="email_select" name="email[]" value="{{$email->id}}">
                    </td>
                    <td> {{$email->email}} </td>
                    <td> {{ $email->createdBy->name }} </td>
                    <td>
                      <div class="icon_container">
                        <i class="fa fa-trash delete_email fa-lg btn " data-email-id = "{{ $email->id }}"></i>
                      </div>
                    </td>
                  </tr>
                @endforeach
              </table>

            </div>
          </div>
        </form>

        {{ $email_list->render() }}

      </div>
    </div>

  </div>
</div>
@endsection

@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"
            type="text/javascript"></script>
    
@endsection

@section('page-level-scripts-js')
  @if(Session::has('status'))
      <script>alert('{{Session::get('status')}}');</script>
  @endif

  <script type="text/javascript">
    

    $(document).ready(function(){

      $('.add_field').click(function(){
        var original_row = $(this).parents(".row.email_list_row");
       
        new_row = original_row.clone();
        $(new_row).removeClass('email_list_row').addClass('cloned_row');
        
        var num_of_cloned_rows = $('.cloned_row').length+1;
        $(new_row).find('.email_label').html("Add Email");

        $(new_row).find('input').val('');

        $(original_row).parent().append(new_row);
        add_field = $(new_row).find('.fa-plus.add_field');
        $(add_field).removeClass('fa-plus add_field').addClass('fa-trash remove_field');
        console.log( $(new_row).find('.fa-plus.add_field') );
      })

      $(document).on('click','.remove_field',function () {
        console.log(1);
        current_row = $(this).parents('.cloned_row');
        current_row.remove();
      })

      $('.delete_email').click(function(){
        var email_id = $(this).attr('data-email-id');
            console.log(email_id);
            var url = "{{ route('delete_email')}}/"+email_id;
        $.ajax({
          url:url,
          type:"PUT",
          data:{
            "_token": "{{ csrf_token() }}",
          },
          success:function(result){
            console.log(result);
            if( result.error != undefined )
              alert(result.error);

            if( result == "success" )
              location.href = "{{ route('email-list.index') }}";

          }
        })
      });

      $(document).on('click','input.bulk_select',function () {
        state = this.checked;
        $('.email_select').prop('checked', state);
        
      })
        
    });
  </script>


@endsection