@extends('layouts.app', ['activePage' => 'outdoor-duty', 'titlePage' => __('Outdoor Duty')])

@section('title') Create Outdoor Duty @endsection


@section('page-level-css')
    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datepicker-master/dist/datepicker.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-clockpicker/dist/bootstrap-clockpicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet" type="text/css" />

    <style type="text/css">
      td{
        padding: 10px;
      }

      input.form-control.upload-file {
          padding: 2px;
      }
    </style>
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Create Outdoor Duty </h4>
      </div>
      <div class="card-body">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <h6> Errors </h6>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="POST" action="{{ route('outdoor-duty.store') }}">
          @csrf
          <div class="row">
            @if( \Auth::user()->hasRole('Super Admin') )
              <div class="col-md-4">
                <label>Select Employee</label>
                <select class="selectpicker form-control employees_select" name="employees">
                  <option value="">Select Employee</option>
                  @foreach($employees as $key => $employee)
                    <option value="{{ $key }}">{{ $employee }}</option>
                  @endforeach
                </select>
              </div>
            @endif
          </div>

          <div class="row">
            <div class="col-md-4">
              <input type="hidden" id="employee_id" name="employee_id" value="{{ \Auth::user()->id }}">

              <label> Employee Name</label>
              <input type="text" name="name" id="name" readonly class="form-control" value="{{ old('name', $current_user->name) }}" placeholder="Enter Name of Outdoor Duty" required>
            </div>
            <div class="col-md-4">
              <label>Department Name</label>
              <input type="hidden" name="department_centre_id" id="department_id" value="{{ old('department_centre_id', $current_user->department_centre_id) }}">
              <input type="text" name="departments" id="departments" readonly class="form-control" value="{{ old('department', $current_user->department_centre->name) }}" placeholder="Enter department" readonly>
            </div>

            <div class="col-md-4">
              <label> Mobile Number</label>
              <input type="text" name="mobile number" id="mobile_number" class="form-control" value="{{ old('mobile_number', $current_user->mobile_number) }}" placeholder="Enter Mobile Number" required>
            </div>
          </div>

          <div class="row">

            <div class="col-md-4">
              <label>Outdoor Duty Date</label>
              <input type="text" name="date" id="date" class="form-control datepicker" value="{{ old('date', '') }}" placeholder="Enter Outdoor Duty Date" required>
            </div>

            <div class="col-md-4">
              <label>Outdoor Duty Place</label>
              <input type="text" name="place" id="place" class="form-control" value="{{ old('place', '') }}" placeholder="Enter Outdoor Duty place" required>
            </div>

            <div class="col-md-4">
              <label>Project</label>
              <select class="selectpicker form-control project_select" name="project_id[]" multiple="true" required>
                <option value="">Select Projects</option>
                @foreach($projects as $key => $value)
                  <option value="{{ $key }}"

                  @if( $key == old('project_id') )
                    selected
                  @endif 

                  > {{ $value }} </option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <label>Purpose of Visit</label>
              <textarea class="form-control" name="purpose_of_visit" placeholder="Enter the purpose of visit" required>{{ old('purpose_of_visit', '') }}</textarea>
            </div>
          </div>

          <div class="row">
            <div class="col-md-4" style="padding: 15px;">
              <label style="width: 100%;">OD Duration</label>
              <input type="radio" name="od_duration" class="od_duration" value="full_day"
                @if( old('od_duration', 'full_day') == "full_day" )
                  checked
                @endif

              style="margin:5px;">Full Day
              <input type="radio" name="od_duration" class="od_duration" value="custom_time"
              @if( old('od_duration') == "custom_time" )
                checked
              @endif
              style="margin:5px;">Custom Time
            </div>
          </div>

          <div class="row" id="custom_time_row" style="display: none;">
            <div class="col-md-3">
              <label>Start Time</label>
              <input type="text" name="start_time" id="start_time" class="form-control" placeholder="Enter Start Time" value="{{ old('start_time', '') }}">
            </div>
            <div class="col-md-3">
              <label>End Time</label>
              <input type="text" name="end_time" id="end_time" class="form-control" placeholder="Enter End Time" value="{{ old('end_time', '') }}">
            </div>
          </div>

        </div>


          <div class="row">
            <div class="col-md-3 offset-4">
              <input type="submit" name="" class="btn" value="Submit" style="width: 100%; background-color: #fa9f19; font-weight: 600; color: #fff;">
            </div>
          </div>
          <hr>

          
        </form>

      </div>
    </div>

  </div>
</div>
@endsection

@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datepicker-master/dist/datepicker.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-clockpicker/dist/bootstrap-clockpicker.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-clockpicker/dist/jquery-clockpicker.js')}}" type="text/javascript"></script>

@endsection

@section('page-level-scripts-js')

  <script type="text/javascript">
    

    $(document).ready(function(){

        $('#start_time').clockpicker();
        $('#end_time').clockpicker();


        $("#date").datepicker({
            format      :   "dd-mm-YYYY",
            viewMode    :   "years", 
            startDate: "today"
        });

        $('select.employees_select').change(function(){
          var employee_id = $(this).val();
          var url = "{{ URL('/master/get-employee-details') }}/"+employee_id;
          $.ajax({
              type: "GET",
              url: url,
              success: function (r) {
                  if( r.success != undefined || r.success != null ){
                    r = r.success;

                    $('#employee_id').val(r.id);
                    $('#name').val(r.name);
                    
                    $('#department_id').val(r.department_centre_id);
                    $('#departments').val(r.department_name);

                    $('#mobile_number').val(r.mobile_number);
                  }
                  else{
                    alert("Something Went Wrong");
                  }

              }
          });

        });

        $('.od_duration').change(function(){
          var value = $('.od_duration:checked').val();
          if( value == "full_day" ){
            $('#custom_time_row').hide();
          }
          else{
            $('#custom_time_row').show();
          }
        })

        $('.od_duration').trigger('change');

    });
  </script>
            
@endsection