@extends('layouts.app', ['activePage' => 'outdoor-duty', 'titlePage' => __('Outdoor Duty')])

@section('title') Outdoor Duty @endsection


@section('page-level-css')
    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datepicker-master/dist/datepicker.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-clockpicker/dist/bootstrap-clockpicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet" type="text/css" />

    <style type="text/css">
      td{
        padding: 10px;
      }

      input.form-control.upload-file {
          padding: 2px;
      }
      textarea.form-control[disabled] {
          background-color: #ccc;
          border-radius: 5px;
      }
    </style>
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Outdoor Duty </h4>
      </div>
      <div class="card-body">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <h6> Errors </h6>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
          <div class="row">
            <div class="col-md-4">

              <label> Employee Name</label>
              <p> {{$outdoor_duty->employee->name}} </p>
            </div>
            <div class="col-md-4">
              <label>Department Name</label>
              <p> {{ $outdoor_duty->department_centre->name }} </p>
            </div>

            <div class="col-md-4">
              <label> Mobile Number</label>
              <p> {{ $outdoor_duty->mobile_number }} </p>
            </div>
          </div>

          <div class="row">

            <div class="col-md-4">
              <label>Outdoor Duty Date</label>
              <p> {{ date_dmy($outdoor_duty->date) }} </p>
            </div>

            <div class="col-md-4">
              <label>Outdoor Duty Place</label>
              <p> {{ $outdoor_duty->place }} </p>
            </div>

            <div class="col-md-4">
              <label>Project</label>
              <table class="table table-striped table-bordered table-hove">
                @foreach( $outdoor_duty->od_projects as $key => $projects )
                  <tr>
                    <td> {{ $projects->project->name }} </td>
                  </tr>
                @endforeach
              </table>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <label>Purpose of Visit</label>
              <p> {{ $outdoor_duty->purpose_of_visit }} </p>
            </div>
          </div>

          <div class="row">
            <div class="col-md-4" style="padding: 15px;">
              <label style="width: 100%;">OD Duration</label>
              <input type="radio" name="od_duration" class="od_duration" value="full_day"
                @if( old('od_duration', $outdoor_duty->od_duration) == "full_day" )
                  checked
                @endif

              style="margin:5px;">Full Day
              <input type="radio" name="od_duration" class="od_duration" value="custom_time"
              @if( old('od_duration', $outdoor_duty->od_duration) == "custom_time" )
                checked
              @endif
              style="margin:5px;">Custom Time
            </div>
          </div>

          <div class="row" id="custom_time_row" style="display: none;">
            <div class="col-md-3">
              <label>Start Time</label>
              <input type="text" name="start_time" id="start_time" class="form-control" placeholder="Enter Start Time" value="{{ old('start_time', $outdoor_duty->start_time) }}">
            </div>
            <div class="col-md-3">
              <label>End Time</label>
              <input type="text" name="end_time" id="end_time" class="form-control" placeholder="Enter End Time" value="{{ old('end_time', $outdoor_duty->end_time) }}">
            </div>
          </div>
        </div>
      </div>
    </div>

      <div class="card">
        @php
          $approved = "";
          $disabled = "";
          if( $outdoor_duty->approval_level_1 == "1" ){
            $disabled = "disabled";
            $approved = ": APPROVED";
          }
          else if( $outdoor_duty->approval_level_1 == "0" ){
            $disabled = "disabled";
            $approved = ": REJECTED";
          }
        @endphp

        <div class="card-header card-header-primary" data-color="orange">
          <h4 class="card-title" style="color: #000; font-weight: 600;">Approval Level 1 {{$approved}}</h4>
        </div>
        <div class="card-body">
              

            <div class="row">
              <div class="col-md-12">
                <label> Approval Level 1 Feedback</label>
                <textarea class="form-control approved_level_1_feedback" name="approved_level_1_feedback" {{ $disabled }} placeholder="Provide feedback appropriate to the type of approval">{{ $outdoor_duty->approved_level_1_feedback }}</textarea>
              </div>
            </div>

            @if( $approved != "" )
              <div class="row">
                <div class="col-md-4">
                  <label>Approved By</label>
                  <p> {{ $outdoor_duty->approvedLevel1By->name }} </p>
                </div>
                <div class="col-md-4">
                  <label>Employee ID</label>
                  <p> {{ $outdoor_duty->approvedLevel1By->employee_id }} </p>
                </div>
              </div>
            @endif  

            @if( $disabled == "" )

              @can('review-outdoor-duty')
                <div class="row">
                  <div class="col-md-3 offset-3">
                    <input type="button" name="" class="btn approval_submit" data-level="1" value="approve" style="width: 100%; background-color: #fa9f19; font-weight: 600; color: #fff;">
                  </div>

                  <div class="col-md-3">
                    <input type="button" name="" class="btn btn-danger approval_submit" data-level="1" value="reject" style="width: 100%; font-weight: 600; color: #fff;">
                  </div>
                </div>
              @endcan
            @endif
        </div>
      </div>

      @if( $outdoor_duty->approval_level_1 == 1 )


        <div class="card">
          @php
            $approved = "";
            $disabled = "";
            if( $outdoor_duty->approval_level_2 == "1" ){
              $disabled = "disabled";
              $approved = ": APPROVED";
            }
            else if( $outdoor_duty->approval_level_2 == "0" ){
              $disabled = "disabled";
              $approved = ": REJECTED";
            }
          @endphp

          <div class="card-header card-header-primary" data-color="orange">
            <h4 class="card-title" style="color: #000; font-weight: 600;">Approval Level 2 {{$approved}}</h4>
          </div>
          <div class="card-body">
                

              <div class="row">
                <div class="col-md-12">
                  <label> Approval Level 2 Feedback</label>
                  <textarea class="form-control approved_level_2_feedback" name="approved_level_2_feedback" {{ $disabled }} placeholder="Provide feedback appropriate to the type of approval">{{ $outdoor_duty->approved_level_2_feedback }}</textarea>
                </div>
              </div>

              @if( $approved != "" )
                <div class="row">
                  <div class="col-md-4">
                    <label>Approved By</label>
                    <p> {{ $outdoor_duty->approvedLevel2By->name }} </p>
                  </div>
                  <div class="col-md-4">
                    <label>Employee ID</label>
                    <p> {{ $outdoor_duty->approvedLevel2By->employee_id }} </p>
                  </div>
                </div>
              @endif  

              @if( $disabled == "" )

                @can('review-outdoor-duty')
                  <div class="row">
                    <div class="col-md-3 offset-3">
                      <input type="button" name="" class="btn approval_submit" data-level="2" value="approve" style="width: 100%; background-color: #fa9f19; font-weight: 600; color: #fff;">
                    </div>

                    <div class="col-md-3">
                      <input type="button" name="" class="btn btn-danger approval_submit" data-level="2" value="reject" style="width: 100%; font-weight: 600; color: #fff;">
                    </div>
                  </div>
                @endcan
              @endif
          </div>
        </div>
      @endif

  </div>
</div>
@endsection

@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datepicker-master/dist/datepicker.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-clockpicker/dist/bootstrap-clockpicker.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-clockpicker/dist/jquery-clockpicker.js')}}" type="text/javascript"></script>

@endsection

@section('page-level-scripts-js')

  <script type="text/javascript">
    

    $(document).ready(function(){

        $('#start_time').clockpicker();
        $('#end_time').clockpicker();


        $("#date").datepicker({
            format      :   "dd-mm-yy",
            viewMode    :   "years", 
        });

        $('select.employees_select').change(function(){
          var employee_id = $(this).val();
          var url = "{{ URL('/master/get-employee-details') }}/"+employee_id;
          $.ajax({
              type: "GET",
              url: url,
              success: function (r) {
                  if( r.success != undefined || r.success != null ){
                    r = r.success;

                    $('#employee_id').val(r.id);
                    $('#name').val(r.name);
                    
                    $('#department_id').val(r.department_centre_id);
                    $('#departments').val(r.department_name);

                    $('#mobile_number').val(r.mobile_number);
                  }
                  else{
                    alert("Something Went Wrong");
                  }

                  console.log(r);
              }
          });

        });

        $('.od_duration').change(function(){
          var value = $('.od_duration:checked').val();

          if( value == "full_day" ){
            $('#custom_time_row').hide();
          }
          else{
            $('#custom_time_row').show();
          }
        })

        $('.od_duration').trigger('change');


        $('.approval_submit').click(function(){
          var approval_status = $(this).val();
          var data_level = $(this).attr('data-level');

          if(data_level == 1 ){
            var approval_feedback = $('.approved_level_1_feedback').val();
          }
          else if( data_level == 2 ){
            var approval_feedback = $('.approved_level_2_feedback').val();
          }
          
          if( approval_status == "reject" && approval_feedback == ""){
            alert('Approval Feedback is compulsory when rejected');
            return false;
          }
          url = "{{ URL('outdoor-duty/approval-process') }}/{{ $outdoor_duty->id }}"; 
          $.ajax({
            type: "PUT",
            url:url,
            data:{
              "_token": "{{ csrf_token() }}",
              "approval_status": approval_status,
              "approval_feedback": approval_feedback,
              "approval_level" : data_level,
            },
            success: function (result) {
              if( result.error != undefined ){
                alert(result.error);
              }
              if( result == "success" ){
                location.href = "{{ route('outdoor-duty.index') }}";
              }
            }
          });
        })

    });
  </script>
            
@endsection