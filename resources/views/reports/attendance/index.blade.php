@extends('layouts.app', ['activePage' => 'attendance-report', 'titlePage' => __('Attendance Report')])

@section('title')Attendance Report @endsection

@section('page-level-css')
    <link href="{{ asset('material') }}/plugins/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ URL::asset('template/assets/global/plugins/plugins/datepicker-master/dist/datepicker.css')}}" rel="stylesheet" type="text/css" />
<script src="https://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
    
    <style type="text/css">
      #map-canvas {
        height: 400px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
      }
    </style>
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary" data-color="orange">
        <h4 class="card-title">Attendance Report </h4>
      </div>

      <div class="card-body">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <h6> Errors </h6>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="GET" action="{{ route('export-attendance') }}" id="exportForm">
          <div class="row">

            @if( \Auth::user()->hasRole('Super Admin') )
              <div class="col-md-4">
                <label>Select Employee</label>

                <select class="selectpicker form-control" name="employee_id">
                  <option value="">Select Employee</option>
                  @foreach($employees as $key => $employee)
                    <option value="{{ $key }}">{{ $employee }}</option>
                  @endforeach
                </select>
              </div>
            @endif
            <div class="col-md-4">
              <label>From Date</label>
              <input type="text" id="from_date" name="from_date" class="datepicker form-control" placeholder="Enter date" required>
            </div>

            <div class="col-md-4">
              <label>To Date</label>
              <input type="text" id="to_date" name="to_date" class="datepicker form-control" placeholder="Enter date" required>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 offset-3">
              <button type="submit" class="btn submit">Submit</button>
            </div>
          </div>
        </form>
      </div>

    </div>

    <div class="card">
      <div class="card-body">

        <table class="table table-striped table-bordered table-hove">
          
        </table>

      </div>

    </div>

  </div>
</div>
@endsection

@section('page-level-plugins-js')
    <script src="{{ URL::asset('template/assets/global/plugins/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('template/assets/global/plugins/plugins/datepicker-master/dist/datepicker.js')}}" type="text/javascript"></script>
@endsection

@section('page-level-scripts-js')
  @if(Session::has('status'))
      <script>alert('{{Session::get('status')}}');</script>
  @endif

  <script type="text/javascript">
    

    $(document).ready(function(){

        $("#from_date").datepicker({
            format      :   "dd-mm-yyyy",
            viewMode    :   "years", 
        });
        $("#to_date").datepicker({
            format      :   "dd-mm-yyyy",
            viewMode    :   "years", 
        });

        $('.submit').click(function(e){
          e.preventDefault();

          var from_date = $('#from_date').val();
          var to_date = $('#to_date').val();

          if( from_date == "" ){
            alert("from date cannot be empty");
            return false;
          }
          if( to_date == "" ){
            alert("To date cannot be empty");
            return false;
          }

          if( from_date > to_date ){
            alert("From Date cannot be greater than To Date");
            return false;
          }

          $('#exportForm').submit();
        })

    });
  </script>

@endsection